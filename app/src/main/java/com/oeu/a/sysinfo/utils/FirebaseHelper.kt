package com.oeu.a.sysinfo.utils

import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent

class FirebaseHelper {
    companion object {
        private val TAG = FirebaseHelper::class.java.simpleName
        //  Initialize this from SystemInfoApplication
        lateinit var firebaseAnalytics: FirebaseAnalytics

        private const val EVENT_PROVIDER_SELECT = "Provider_Select"
        private const val EVENT_PROVIDER_EXCEPTION = "Provider_Exception"
        private const val PARAM_MESSAGE = "Throwable_Message"
        private const val PARAM_FILENAME = "Throwable_Filename"
        private const val PARAM_LINE_NUMBER = "Throwable_Linenumber"

        fun onProviderSelected(name: String) {
            firebaseAnalytics.logEvent(EVENT_PROVIDER_SELECT) {
                param(FirebaseAnalytics.Param.ITEM_NAME, name)
            }
        }

        fun onThrowableCaught(e: Throwable) {
            firebaseAnalytics.logEvent(EVENT_PROVIDER_EXCEPTION) {
                param(FirebaseAnalytics.Param.ITEM_NAME, e::class.java.simpleName)
                param(PARAM_MESSAGE, e.message ?: "")
                param(PARAM_FILENAME, e.stackTrace[0].fileName)
                param(PARAM_LINE_NUMBER, e.stackTrace[0].lineNumber.toLong())
            }
        }
    }
}
