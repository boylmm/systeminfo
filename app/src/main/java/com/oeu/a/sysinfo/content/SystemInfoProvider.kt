package com.oeu.a.sysinfo.content

import com.oeu.a.sysinfo.R

class SystemInfoProvider: InfoProvider() {
    companion object {
        private val TAG = SystemInfoProvider::class.java.simpleName
    }

    private fun formatSeparator(ch: Char): String {
        return if (Character.isWhitespace(ch)) {
            String.format("(0x%1$02X)", ch.code)
        } else {
            String.format("%1$1c (0x%2$02X)", ch, ch.code)
        }
    }

    private fun formatSeparators(str: String?): String {
        val sb = StringBuilder()
        if (null != str && str.isNotEmpty()) {
            sb.append(formatSeparator(str[0]))
            for (idx in 1 until str.length) {
                sb.append('\n').append(formatSeparator(str[0]))
            }
        } else {
            sb.append(getString(R.string.none))
        }
        return sb.toString()
    }

    private val systemContent: MutableList<InfoItem> = mutableListOf()

    override suspend fun getItems(): List<InfoItem> {
        if (systemContent.isEmpty()) {
            systemContent.add(InfoItem(R.string.system_processors, Runtime.getRuntime().availableProcessors().toString()))

            val propertyItems: MutableList<InfoItem> = mutableListOf()
            val properties = System.getProperties()
            for (name in properties.stringPropertyNames()) {
                if (name.contains("separator")) {
                    propertyItems.add(InfoItem(name, formatSeparators(properties.getProperty(name))))
                } else {
                    propertyItems.add(InfoItem(name, properties.getProperty(name)))
                }
            }
            systemContent.addAll(propertyItems.sortedBy { it.name })

            val environmentItems: MutableList<InfoItem> = mutableListOf()
            val environmentVariables = System.getenv()
            for (key in environmentVariables.keys) {
                environmentItems.add(InfoItem(key, environmentVariables[key] ?: getString(R.string.empty)))
            }
            systemContent.addAll(environmentItems.sortedBy { it.name })
        }
        return systemContent
    }
}
