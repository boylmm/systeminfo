package com.oeu.a.sysinfo.content

import android.annotation.SuppressLint
import android.os.Build
import android.util.Log
import com.oeu.a.sysinfo.R
import com.oeu.a.sysinfo.utils.FirebaseHelper

class AboutInfoProvider: InfoProvider() {
    companion object {
        private val TAG = AboutInfoProvider::class.java.simpleName
        fun formatAppVersion(): String {
            val packageInfo = app.packageManager.getPackageInfo(app.packageName, 0)
            return if (Build.VERSION.SDK_INT < Build.VERSION_CODES.P)
                "${packageInfo.versionName} (${packageInfo.versionCode})"
            else
                "${packageInfo.versionName} (${packageInfo.longVersionCode})"
        }
    }

    private val itemSpecs = listOf(
        InfoSpec(R.string.about_app_name),
        InfoSpec(R.string.about_purpose),
        InfoSpec(R.string.about_packagename),
        InfoSpec(R.string.about_version),
        InfoSpec(R.string.about_supported_latest_android),
        InfoSpec(R.string.about_supported_oldest_android, 24),
        InfoSpec(R.string.about_developer),
        InfoSpec(R.string.about_thanks),
        InfoSpec(R.string.about_source),
        InfoSpec(R.string.about_license)
    )

    @SuppressLint("NewApi")
    override fun getItem(infoId: Int, vararg args: Any?): InfoItem {
        val title = getString(infoId)
        val value: String = try {
            when (infoId) {
                R.string.about_app_name-> app.packageManager.getApplicationLabel(app.applicationInfo).toString()
                R.string.about_purpose-> getString(R.string.about_purpose_text)
                R.string.about_packagename-> app.packageName
                R.string.about_version-> formatAppVersion()
                R.string.about_supported_latest_android->AndroidInfoProvider.formatSdk(app.applicationInfo.targetSdkVersion)
                R.string.about_supported_oldest_android->AndroidInfoProvider.formatSdk(app.applicationInfo.minSdkVersion)
                R.string.about_developer-> "boylmm@gmail.com"
                R.string.about_thanks-> "Nemustech"
                R.string.about_source-> "https://bitbucket.org/boylmm/systeminfo.git"
                R.string.about_license-> getString(R.string.about_license_gpl2)
                else-> getString(R.string.invalid_item)
            }
        } catch (e: Error) {
            FirebaseHelper.onThrowableCaught(e)
            e.printStackTrace()
            getString(R.string.unsupported)
        }
        return InfoItem(title, value)
    }

    private val aboutContent: MutableList<InfoItem> = mutableListOf()
    override suspend fun getItems(): List<InfoItem> {
        if (aboutContent.isEmpty()) {
            aboutContent.addItems(itemSpecs)
        }
        return aboutContent
    }
}
