package com.oeu.a.sysinfo.content

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Configuration
import android.graphics.Insets
import android.graphics.PixelFormat
import android.graphics.Point
import android.graphics.Rect
import android.hardware.display.DisplayManager
import android.os.Build
import android.util.DisplayMetrics
import android.util.Log
import android.view.Display
import android.view.DisplayCutout
import android.view.View
import android.view.WindowManager
import androidx.annotation.RequiresApi
import com.oeu.a.sysinfo.R
import com.oeu.a.sysinfo.utils.FirebaseHelper
import com.oeu.a.sysinfo.utils.with
import com.oeu.a.sysinfo.utils.isFlagsSet
import com.oeu.a.sysinfo.utils.trimEnd
import kotlin.math.sqrt

class ScreenInfoProvider: InfoProvider() {
    companion object {
        private val TAG = ScreenInfoProvider::class.java.simpleName
    }

    private fun formatSize(w: Int, h: Int): String = String.format("%d x %d", w, h)

    private fun formatSize(w: Float, h: Float): String = String.format("%f x %f", w, h)

    private fun formatSize(p: Point): String = String.format("%d x %d", p.x, p.y)

    private fun formatDpi(dpi: Int): String {
        val description = when (dpi) {
            DisplayMetrics.DENSITY_LOW -> "ldpi"
            DisplayMetrics.DENSITY_MEDIUM -> "mdpi"
            DisplayMetrics.DENSITY_HIGH -> "hdpi"
            DisplayMetrics.DENSITY_XHIGH -> "xhdpi"
            DisplayMetrics.DENSITY_XXHIGH -> "xxhdpi"
            DisplayMetrics.DENSITY_XXXHIGH -> "xxxhdpi"
            DisplayMetrics.DENSITY_TV -> "tvdpi"
            else -> getString(R.string.unknown)
        }
        return String.format("%d dpi (%s)", dpi, description)
    }

    private fun formatPixelFormat(format: Int): String {
        val name = when (format) {
            PixelFormat.A_8 -> "A_8"
            PixelFormat.LA_88 -> "LA_88"
            PixelFormat.L_8 -> "L_8"
            PixelFormat.RGBA_4444 -> "RGBA_4444"
            PixelFormat.RGBA_5551 -> "RGBA_5551"
            PixelFormat.RGBA_8888 -> "RGBA_8888"
            PixelFormat.RGBX_8888 -> "RGBX_8888"
            PixelFormat.RGB_332 -> "RGB_332"
            PixelFormat.OPAQUE -> "OPAQUE"
            PixelFormat.RGB_565 -> "RGB_565"
            PixelFormat.RGB_888 -> "RGB_888"
            PixelFormat.TRANSLUCENT -> "TRANSLUCENT"
            PixelFormat.TRANSPARENT -> "TRANSPARENT"
            else -> getString(R.string.unknown)
        }
        return String.format("%s (%d)", name, format)
    }

    private fun formatSizeDp(disp: Display, dm: DisplayMetrics): String {
        val p = Point().also { getDisplaySize(it, disp) }
        val w = p.x / dm.density
        val h = p.y / dm.density
        return String.format("%.2f x %.2f dp", w, h)
    }

    private fun formatSizeInch(disp: Display, dm: DisplayMetrics): String {
        val p = Point().also { getDisplaySize(it, disp) }
        val w = p.x / dm.xdpi
        val h = p.y / dm.ydpi
        val d = sqrt((w * w + h * h).toDouble())
        return String.format("%.2f x %.2f (%.2f inch)", w, h, d)
    }

    @SuppressLint("NewApi")
    private fun getDisplaySize(outPoint: Point, disp: Display) {
        try {
            if (Build.VERSION_CODES.JELLY_BEAN_MR1 <= Build.VERSION.SDK_INT) {
                disp.getRealSize(outPoint)
            } else {
                disp.getSize(outPoint)
            }
        } catch (e: Error) {
            FirebaseHelper.onThrowableCaught(e)
            e.printStackTrace()
            outPoint.x = disp.width
            outPoint.y = disp.height
        }
    }

    private fun formatDisplayState(state: Int): String {
        return when (state) {
            Display.STATE_ON -> String.format("On ($state)")
            Display.STATE_ON_SUSPEND -> String.format("On suspend ($state)")
            Display.STATE_OFF -> String.format("Off ($state)")
            Display.STATE_DOZE -> String.format("Doze ($state)")
            Display.STATE_DOZE_SUSPEND -> String.format("Doze suspend ($state)")
            Display.STATE_UNKNOWN -> String.format("Unknown ($state)")
            Display.STATE_VR-> String.format("VR ($state)")
            else-> getString(R.string.unknown_int_value, state)
        }
    }

    @SuppressLint("NewApi")
    private fun getSizeRange(disp: Display): String {
        val largest = Point()
        val smallest = Point()
        disp.getCurrentSizeRange(smallest, largest)
        return String.format("%4d x%5d - %4d x%5d", smallest.x, smallest.y, largest.x, largest.y)
    }

    @SuppressLint("InlinedApi")
    private fun formatFlags(_flags: Int): String {
        var flags = _flags
        val sb = StringBuilder()
        sb.append(String.format("Feature flags: 0x%02X", flags))
        if (flags.isFlagsSet(Display.FLAG_SUPPORTS_PROTECTED_BUFFERS)) {
            sb.append("\n\t").append(String.format("0x%02X: Supports protected buffer", Display.FLAG_SUPPORTS_PROTECTED_BUFFERS))
            flags = flags and Display.FLAG_SUPPORTS_PROTECTED_BUFFERS.inv()
        }
        if (flags.isFlagsSet(Display.FLAG_SECURE)) {
            sb.append("\n\t").append(String.format("0x%02X: Secure", Display.FLAG_SECURE))
            flags = flags and Display.FLAG_SECURE.inv()
        }
        if (flags.isFlagsSet(Display.FLAG_PRIVATE)) {
            sb.append("\n\t").append(String.format("0x%02X: Private", Display.FLAG_PRIVATE))
            flags = flags and Display.FLAG_PRIVATE.inv()
        }
        if (flags.isFlagsSet(Display.FLAG_PRESENTATION)) {
            sb.append("\n\t").append(String.format("0x%02X: Presentation", Display.FLAG_PRESENTATION))
            flags = flags and Display.FLAG_PRESENTATION.inv()
        }
        if (flags.isFlagsSet(Display.FLAG_ROUND)) {
            sb.append("\n\t").append(String.format("0x%02X: Round shaped", Display.FLAG_ROUND))
            flags = flags and Display.FLAG_ROUND.inv()
        }
        if (0 != flags) {
            sb.append("\n\t").append(String.format("0x%02X: Unknown", flags))
        }
        return sb.toString()
    }

    private fun getDisplays(): Array<Display> {
        return if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            val wm = app.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            arrayOf(wm.defaultDisplay)
        } else {
            val dm = app.getSystemService(Context.DISPLAY_SERVICE) as DisplayManager
            dm.displays
        }
    }

    private fun formatLogicalSize(layout: Int): String {
        return when (val layoutSize = layout and Configuration.SCREENLAYOUT_SIZE_MASK) {
            Configuration.SCREENLAYOUT_SIZE_SMALL ->  getString(R.string.screen_size_small, layoutSize)
            Configuration.SCREENLAYOUT_SIZE_NORMAL ->  getString(R.string.screen_size_normal, layoutSize)
            Configuration.SCREENLAYOUT_SIZE_LARGE ->  getString(R.string.screen_size_large, layoutSize)
            Configuration.SCREENLAYOUT_SIZE_XLARGE ->  getString(R.string.screen_size_xlarge, layoutSize)
            Configuration.SCREENLAYOUT_SIZE_UNDEFINED ->  getString(R.string.screen_size_undefined, layoutSize)
            else -> getString(R.string.unknown_int_value, layoutSize)
        }
    }

    private fun formatUiMode(uiMode: Int): String {
        val sb = StringBuilder()
        val modeType = uiMode and Configuration.UI_MODE_TYPE_MASK
        when (modeType) {
            Configuration.UI_MODE_TYPE_APPLIANCE -> sb.append(getString(R.string.screen_mode_type_appliance, modeType))
            Configuration.UI_MODE_TYPE_CAR -> sb.append(getString(R.string.screen_mode_type_car, modeType))
            Configuration.UI_MODE_TYPE_DESK -> sb.append(getString(R.string.screen_mode_type_desk, modeType))
            Configuration.UI_MODE_TYPE_NORMAL -> sb.append(getString(R.string.screen_mode_type_normal, modeType))
            Configuration.UI_MODE_TYPE_TELEVISION -> sb.append(getString(R.string.screen_mode_type_television, modeType))
            Configuration.UI_MODE_TYPE_WATCH -> sb.append(getString(R.string.screen_mode_type_watch, modeType))
            Configuration.UI_MODE_TYPE_UNDEFINED -> sb.append(getString(R.string.screen_mode_type_undefined, modeType))
            else -> sb.append(getString(R.string.unknown_int_value, modeType))
        }
        sb.append('\n')

        when (val night = uiMode and Configuration.UI_MODE_NIGHT_MASK) {
            Configuration.UI_MODE_NIGHT_NO -> sb.append(getString(R.string.screen_night_no, night))
            Configuration.UI_MODE_NIGHT_YES -> sb.append(getString(R.string.screen_night_yes, night))
            Configuration.UI_MODE_NIGHT_UNDEFINED -> sb.append(getString(R.string.screen_night_undefined, night))
            else -> sb.append(getString(R.string.unknown_int_value, modeType))
        }
        return sb.toString()
    }

    private fun formatLayoutDirection(direction: Int): String {
        return when (direction) {
            View.LAYOUT_DIRECTION_LTR-> getString(R.string.screen_layout_ltr, direction)
            View.LAYOUT_DIRECTION_RTL-> getString(R.string.screen_layout_rtl, direction)
            else-> getString(R.string.unknown_int_value, direction)
        }
    }

    private val Rect.infoDescription: String get() = "@($left, $top) ${width()} x ${height()}"

    @SuppressLint("NewApi")
    private fun formatCutout(cutout: DisplayCutout?): String {
        return cutout?.let {
            val builder = StringBuilder()
            val rects = it.boundingRects
            if (rects.isNotEmpty()) {
                builder.append(getString(R.string.screen_non_functional_area)).append('\n')
                for (rect in rects) {
                    builder.append('\t').append(rect.infoDescription).append('\n')
                }
            }
            builder.append("Safe inset left ${it.safeInsetLeft} top ${it.safeInsetTop} bottom ${it.safeInsetBottom} right ${it.safeInsetRight}\n")
            if (Build.VERSION_CODES.R <= Build.VERSION.SDK_INT) {
                val insets = it.waterfallInsets
                if (insets == Insets.NONE) {
                    builder.append(getString(R.string.screen_no_waterfall_insets)).append('\n')
                } else {
                    builder.append("Waterfall insets left ${insets.left} top ${insets.top} bottom ${insets.bottom} right ${insets.right}\n")
                }
            }
            builder.trimEnd().toString()
        } ?: getString(R.string.none)
    }

    @SuppressLint("NewApi")
    private fun Display.getNewMetrics(): DisplayMetrics {
        val metrics = DisplayMetrics()
        try {
            getRealMetrics(metrics)
        } catch (e: Error) {
            FirebaseHelper.onThrowableCaught(e)
            e.printStackTrace()
            getMetrics(metrics)
        }
        return metrics
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun formatOtherConfigurations(config: Configuration): String {
        val builder = StringBuilder()
        if (config.isScreenRound) {
            builder.append("Round screen\n")
        }
        if (Build.VERSION_CODES.O <= Build.VERSION.SDK_INT) {
            if (config.isScreenHdr) {
                builder.append("HDR\n")
            }
            if (config.isScreenWideColorGamut) {
                builder.append("Wide color gamut\n")
            }
        }
        if (Build.VERSION_CODES.R <= Build.VERSION.SDK_INT) {
            if (config.isNightModeActive) {
                builder.append("Night mode active\n")
            }
        }
        if (builder.isEmpty()) {
            builder.append(getString(R.string.none))
        }
        return builder.trimEnd().toString()
    }

    private val configSpecs = listOf(
        InfoSpec(R.string.screen_logical_size, 4),
        InfoSpec(R.string.screen_smallest_width_dp, 13),
        InfoSpec(R.string.screen_ui_mode, 8),
        InfoSpec(R.string.screen_layout_direction, 17),
        InfoSpec(R.string.screen_stable_dpi, 24),
        InfoSpec(R.string.screen_other_configurations, 23),
    )
    private val configIds = configSpecs.map { it.titleId }.toSet()


    private val Display.infoPrefix: String get() = "Display $displayId: "

    private val displaySpecs = listOf(
        InfoSpec(R.string.screen_id, 1),
        InfoSpec(R.string.screen_name, 17),
        InfoSpec(R.string.screen_dpi, 4),
        InfoSpec(R.string.screen_available_size, 13),
        InfoSpec(R.string.screen_real_size, 17),
        InfoSpec(R.string.screen_size_range, 16),
        InfoSpec(R.string.screen_logical_size_in_dp, 1),
        InfoSpec(R.string.screen_physical_size_in_inch, 1),
        InfoSpec(R.string.screen_pixel_format, 1),
        InfoSpec(R.string.screen_physical_dpi, 1),
        InfoSpec(R.string.screen_logical_density, 1),
        InfoSpec(R.string.screen_scaled_density, 1),
        InfoSpec(R.string.screen_rotation, 8),
        InfoSpec(R.string.screen_refresh_rate, 1),
        InfoSpec(R.string.screen_supported_refresh_rates, 21),
        InfoSpec(R.string.screen_app_vsync_offset, 21),
        InfoSpec(R.string.screen_presentation_deadline, 17),
        InfoSpec(R.string.screen_state, 20),
        InfoSpec(R.string.screen_cutout, 29),
        InfoSpec(R.string.screen_other_features, 17)
    )

    @SuppressLint("NewApi")
    override fun getItem(infoId: Int, vararg args: Any?): InfoItem? {
        val name = getString(infoId)
        if (infoId in configIds) {
            val config = app.resources.configuration
            val value = when (infoId) {
                R.string.screen_logical_size-> formatLogicalSize(config.screenLayout)
                R.string.screen_smallest_width_dp-> String.format("%d", config.smallestScreenWidthDp)
                R.string.screen_ui_mode-> formatUiMode(config.uiMode)
                R.string.screen_layout_direction-> formatLayoutDirection(config.layoutDirection)
                R.string.screen_stable_dpi -> formatDpi(DisplayMetrics.DENSITY_DEVICE_STABLE)
                R.string.screen_other_configurations -> formatOtherConfigurations(config)
                else -> getString(R.string.invalid_item)
            }
            return InfoItem(name, value)
        } else {
            with(args[0] as? Display, args[1] as? DisplayMetrics) { display, metrics ->
                val value = try {
                    when (infoId) {
                        R.string.screen_dpi -> formatDpi(metrics.densityDpi)
                        R.string.screen_available_size -> {
                            val p = Point().also { display.getSize(it) }
                            formatSize(p)
                        }
                        R.string.screen_real_size -> {
                            val p = Point().also { getDisplaySize(it, display) }
                            formatSize(p)
                        }
                        R.string.screen_logical_size_in_dp -> formatSizeDp(display, metrics)
                        R.string.screen_physical_size_in_inch -> formatSizeInch(display, metrics)
                        R.string.screen_pixel_format -> formatPixelFormat(display.pixelFormat)
                        R.string.screen_physical_dpi -> formatSize(metrics.xdpi, metrics.ydpi)
                        R.string.screen_logical_density -> metrics.density.toString()
                        R.string.screen_scaled_density -> metrics.scaledDensity.toString()
                        R.string.screen_id -> display.displayId.toString()
                        R.string.screen_name -> display.name
                        R.string.screen_rotation -> display.rotation.toString()
                        R.string.screen_refresh_rate -> display.refreshRate.toString()
                        R.string.screen_supported_refresh_rates -> formatFloatArray(display.supportedRefreshRates)
                        R.string.screen_app_vsync_offset -> display.appVsyncOffsetNanos.toString()
                        R.string.screen_presentation_deadline -> String.format("%,d", display.presentationDeadlineNanos)
                        R.string.screen_state -> formatDisplayState(display.state)
                        R.string.screen_size_range -> getSizeRange(display)
                        R.string.screen_other_features -> formatFlags(display.flags)
                        R.string.screen_cutout -> formatCutout(display.cutout)
                        else -> getString(R.string.invalid_item)
                    }
                } catch (e: Error) {
                    FirebaseHelper.onThrowableCaught(e)
                    e.printStackTrace()
                    getString(R.string.unsupported)
                }
                return InfoItem(display.infoPrefix + name, value)
            }
        }
        return null
    }

    private val screenItems: MutableList<InfoItem> = mutableListOf()

    override suspend fun getItems(): List<InfoItem> {
        //  Intentionally rebuild all the items whenever called
        //  because display orientation may have ben changed
        screenItems.clear()

        screenItems.addItems(configSpecs)
        val displays = getDisplays()
        screenItems.add(InfoItem(R.string.screen_count, "${displays.size}"))
        for (display in displays) {
            screenItems.addItems(displaySpecs, display, display.getNewMetrics())
        }

        return screenItems
    }
}
