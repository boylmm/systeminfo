package com.oeu.a.sysinfo.content

import android.annotation.SuppressLint
import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorDirectChannel
import android.hardware.SensorManager
import android.os.Build
import android.util.Log
import com.oeu.a.sysinfo.R
import com.oeu.a.sysinfo.utils.FirebaseHelper

class SensorInfoProvider: InfoProvider() {
    companion object {
        private val TAG = SensorInfoProvider::class.java.simpleName
    }

    private fun formatSensorType(type: Int): String {
        val name = when (type) {
            Sensor.TYPE_ACCELEROMETER -> "Accelerometer"
            Sensor.TYPE_ACCELEROMETER_UNCALIBRATED -> "Accelerometer (uncalibrated)"
            Sensor.TYPE_ALL -> "All"
            Sensor.TYPE_AMBIENT_TEMPERATURE -> "Ambient temperature"
            Sensor.TYPE_GAME_ROTATION_VECTOR -> "Rotation vector"
            Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR -> "Geomagnetic rotation vector"
            Sensor.TYPE_GRAVITY -> "Gravity"
            Sensor.TYPE_GYROSCOPE -> "Gyroscope"
            Sensor.TYPE_GYROSCOPE_UNCALIBRATED -> "Gyroscope (uncalibrated)"
            Sensor.TYPE_HEART_BEAT -> "Heart beat"
            Sensor.TYPE_HEART_RATE -> "Heart rate"
            Sensor.TYPE_HINGE_ANGLE -> "Hinge angle"
            Sensor.TYPE_LIGHT -> "Light"
            Sensor.TYPE_LINEAR_ACCELERATION -> "Linear acceleration"
            Sensor.TYPE_LOW_LATENCY_OFFBODY_DETECT -> "Low latency off-body detect"
            Sensor.TYPE_MAGNETIC_FIELD -> "Magnetic field"
            Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED -> "Magnetic field (uncalibrated)"
            Sensor.TYPE_MOTION_DETECT -> "Motion detect"
            Sensor.TYPE_ORIENTATION -> "Orientation"
            Sensor.TYPE_POSE_6DOF -> "Pose with 6 degrees of freedom"
            Sensor.TYPE_PRESSURE -> "Pressure"
            Sensor.TYPE_PROXIMITY -> "Proximity"
            Sensor.TYPE_RELATIVE_HUMIDITY -> "Relative humidity"
            Sensor.TYPE_ROTATION_VECTOR -> "Rotation vector"
            Sensor.TYPE_SIGNIFICANT_MOTION -> "Significant motion"
            Sensor.TYPE_STATIONARY_DETECT -> "Stationary detect"
            Sensor.TYPE_STEP_COUNTER -> "Step counter"
            Sensor.TYPE_STEP_DETECTOR -> "Step detector"
            Sensor.TYPE_TEMPERATURE -> "Temperature"
            else -> null
        }
        return name?.let { "$name ($type)"} ?: getString(R.string.unknown_int_value, type)
    }

    private fun formatReportingMode(mode: Int): String {
        return when (mode) {
            Sensor.REPORTING_MODE_CONTINUOUS -> "Continuous ($mode)"
            Sensor.REPORTING_MODE_ON_CHANGE -> "On change ($mode)"
            Sensor.REPORTING_MODE_ONE_SHOT -> "One shot ($mode)"
            Sensor.REPORTING_MODE_SPECIAL_TRIGGER -> "Special trigger ($mode)"
            else -> getString(R.string.unknown_int_value, mode)
        }
    }

    private fun formatHighestDirectReportRate(level: Int): String {
        return when (level) {
            SensorDirectChannel.RATE_STOP -> "Stop ($level)"
            SensorDirectChannel.RATE_NORMAL -> "Normal ($level)"
            SensorDirectChannel.RATE_FAST -> "Fast ($level)"
            SensorDirectChannel.RATE_VERY_FAST -> "Very fast ($level)"
            else -> getString(R.string.unknown_int_value, level)
        }
    }

    private fun formatSensorId(id: Int): String {
        return when (id) {
            0 -> "Unsupported ($id)"
            -1 -> "Unique ($id)"
            else -> id.toString()
        }
    }

    @SuppressLint("newApi")
    private fun formatDirectChannelSupport(sensor: Sensor): String {
        val builder = StringBuilder()
        if (sensor.isDirectChannelTypeSupported(SensorDirectChannel.TYPE_HARDWARE_BUFFER)) {
            builder.append("\n\tHardware buffer")
        }
        if (sensor.isDirectChannelTypeSupported(SensorDirectChannel.TYPE_MEMORY_FILE)) {
            builder.append("\n\tMemory file")
        }
        if (builder.isEmpty()) {
            builder.append("No support")
        }
        return builder.toString()
    }

    @SuppressLint("newApi")
    private fun appendSensorProperty(builder: StringBuilder, id: Int, sensor: Sensor) {
        val title = getString(id)
        val value = try {
            when (id) {
                R.string.sensor_id -> formatSensorId(sensor.id)
                R.string.sensor_type -> formatSensorType(sensor.type) +
                    if (Build.VERSION_CODES.KITKAT_WATCH <= Build.VERSION.SDK_INT) "\n\t${sensor.stringType}" else ""
                R.string.sensor_vendor -> sensor.vendor
                R.string.sensor_version -> sensor.version.toString()
                R.string.sensor_power -> sensor.power.toString()
                R.string.sensor_resolution -> sensor.resolution.toString()
                R.string.sensor_max_range -> sensor.maximumRange.toString()
                R.string.sensor_delay -> {
                    val max = if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) -1 else sensor.maxDelay
                    if (max <= 0) {
                        String.format("minimum %,d", sensor.minDelay)
                    } else {
                        String.format("%,d ~ %,d", sensor.minDelay, max)
                    }
                }
                R.string.sensor_batch_fifo_size -> {
                    val max = sensor.fifoMaxEventCount
                    if (0 == max) {
                        getString(R.string.sensor_batch_not_supported)
                    } else {
                        String.format("%,d ~ %,d", sensor.fifoReservedEventCount, max)
                    }
                }
                R.string.sensor_reporting_mode -> formatReportingMode(sensor.reportingMode)
                R.string.sensor_highest_direct_report_rate -> formatHighestDirectReportRate(sensor.highestDirectReportRateLevel)
                R.string.sensor_direct_channel_support -> formatDirectChannelSupport(sensor)
                else -> getString(R.string.invalid_item)
            }
        } catch (e: Error) {
            FirebaseHelper.onThrowableCaught(e)
            e.printStackTrace()
            getString(R.string.unsupported)
        }
        builder.append(title).append(": ").append(value).append('\n')
    }

    private val itemSpecs = listOf(
        InfoSpec(R.string.sensor_id, 24),
        InfoSpec(R.string.sensor_type, 3),
        InfoSpec(R.string.sensor_vendor, 3),
        InfoSpec(R.string.sensor_version, 3),
        InfoSpec(R.string.sensor_power, 3),
        InfoSpec(R.string.sensor_resolution, 3),
        InfoSpec(R.string.sensor_max_range, 3),
        InfoSpec(R.string.sensor_delay, 9),
        InfoSpec(R.string.sensor_batch_fifo_size, 19),
        InfoSpec(R.string.sensor_reporting_mode, 21),
        InfoSpec(R.string.sensor_highest_direct_report_rate, 26),
        InfoSpec(R.string.sensor_direct_channel_support, 26),
    )

    private fun getSensorItem(sensor: Sensor, suffix: String = ""): InfoItem {
        val builder = StringBuilder()
        for (spec in itemSpecs) {
            if (spec.isAvailable) {
                appendSensorProperty(builder, spec.titleId, sensor)
            }
        }
        builder.deleteCharAt(builder.lastIndex)
        return InfoItem(sensor.name + suffix, builder.toString())
    }

    private val sensorContent = mutableListOf<InfoItem>()
    override suspend fun getItems(): List<InfoItem> {
        if (sensorContent.isEmpty()) {
            val manager = app.getSystemService(Context.SENSOR_SERVICE) as SensorManager
            val sensors = manager.getSensorList(Sensor.TYPE_ALL)
            if (sensors.isEmpty()) {
                sensorContent.add(InfoItem(R.string.item_sensor, R.string.sensor_none))
            } else {
                for (sensor in sensors.sortedBy { it.type }) {
                    sensorContent.add(getSensorItem(sensor))
                }
                if (Build.VERSION_CODES.N <= Build.VERSION.SDK_INT) {
                    sensorContent.add(InfoItem(R.string.sensor_dynamic_discovery, if (manager.isDynamicSensorDiscoverySupported) "supported" else "not supported"))
                    val dynamicSensors = manager.getDynamicSensorList(Sensor.TYPE_ALL)
                    if (dynamicSensors.isNotEmpty()) {
                        for (sensor in dynamicSensors.sortedBy { it.type }) {
                            sensorContent.add(getSensorItem(sensor, " (Dynamic)"))
                        }
                    }
                }
            }
        }
        return sensorContent
    }
}
