package com.oeu.a.sysinfo.content

import android.annotation.SuppressLint
import android.os.Build
import android.util.Log
import com.oeu.a.sysinfo.R
import com.oeu.a.sysinfo.utils.FirebaseHelper
import java.util.Locale

class LocaleInfoProvider: InfoProvider() {
    companion object {
        private val TAG = LocaleInfoProvider::class.java.simpleName
    }

    private fun formatLocaleName(locale: Locale): String {
        val name = locale.displayName
        val dname = locale.getDisplayName(locale)
        return if (name.isNotEmpty()) {
            "$name: $dname"
        } else getString(R.string.no_name)
    }

    @SuppressLint("NewApi")
    private fun appendUnicodeLocaleInfo(sb: StringBuilder, locale: Locale) {
        if (Build.VERSION_CODES.LOLLIPOP <= Build.VERSION.SDK_INT) {
            val attribs = locale.unicodeLocaleAttributes
            if (0 < attribs.size) {
                sb.append("\nUnicode attributes")
                for (attr in attribs) {
                    sb.append("\n\t").append(attr)
                }
            }
            val keys = locale.unicodeLocaleKeys
            if (0 < keys.size) {
                sb.append("\nUnicode extension")
                for (key in keys) {
                    sb.append("\n\t").append(key).append(": ")
                    sb.append(locale.getUnicodeLocaleType(key))
                }
            }
        }
    }

    @SuppressLint("NewApi")
    private fun appendLocaleScript(sb: StringBuilder, locale: Locale) {
        if (Build.VERSION_CODES.LOLLIPOP <= Build.VERSION.SDK_INT) {
            val scr = locale.script
            if (scr.isNotEmpty()) {
                sb.append(" - ")
                val ds = locale.displayScript
                val lds = locale.getDisplayScript(locale)
                sb.append(ds).append(" (").append(lds).append(')')
            }
        }
    }

    @SuppressLint("NewApi")
    private fun formatLocaleCodes(locale: Locale): String {
        val lang = locale.language
        val cc = locale.country
        val variant = locale.variant
        var scr: String? = null
        var lang3: String? = null
        var cc3: String? = null
        try {
            lang3 = locale.isO3Language
            cc3 = locale.isO3Country
        } catch (e: Exception) {
            FirebaseHelper.onThrowableCaught(e)
            Log.e(TAG, "$e\tlang: $lang")
        }
        val sb = StringBuilder()
        sb.append(lang)
        if (Build.VERSION_CODES.LOLLIPOP <= Build.VERSION.SDK_INT) {
            scr = locale.script
            if (scr.isNotEmpty()) {
                sb.append('-').append(scr)
            }
        }
        if (cc.isNotEmpty()) {
            sb.append('-').append(cc)
        }
        if (variant.isNotEmpty()) {
            sb.append(", ").append(variant)
        }
        val tag = sb.toString()
        sb.append(" (")
        if (lang3.isNullOrEmpty()) {
            sb.append(getString(R.string.locale_no_iso3))
        } else {
            sb.append(lang3)
            if (!scr.isNullOrEmpty()) {
                sb.append('-').append(scr)
            }
            if (!cc3.isNullOrEmpty()) {
                sb.append('-').append(cc3)
            }
        }
        sb.append(')')
        if (Build.VERSION_CODES.LOLLIPOP <= Build.VERSION.SDK_INT) {
            val tag21 = locale.toLanguageTag()
            if (tag21.isNotEmpty() && tag != tag21) {
                sb.append(" tag ").append(tag21)
            }
        }
        return sb.toString()
    }

    private fun getLocaleItem(locale: Locale): InfoItem {
        val sb = StringBuilder()
        //        appendLocaleScript(sb, locale);  //  this information is redundant
        appendUnicodeLocaleInfo(sb, locale)
        return InfoItem(formatLocaleName(locale), formatLocaleCodes(locale) + sb.toString())
    }

    @SuppressLint("NewApi")
    private fun getConfigurationLocaleItems(): List<InfoItem> {
        val items: MutableList<InfoItem> = mutableListOf()
        val localeList = app.resources.configuration.locales
        Log.i(TAG, "Locales in config: ${localeList.size()}")
        if (localeList.isEmpty) {
            items.add(InfoItem(R.string.locale_config, R.string.empty))
        } else {
            items.add(getSpecificLocale(getString(R.string.locale_current), localeList[0]))
            for (idx in 1 until localeList.size()) {
                items.add(getSpecificLocale("* config $idx", localeList[idx]))
            }
        }
        return items
    }

    private fun getSpecificLocale(name: String, locale: Locale): InfoItem =
        InfoItem(name, formatLocaleCodes(locale) + " - " + formatLocaleName(locale))

    private val localeContent: MutableList<InfoItem> = mutableListOf()

    override suspend fun getItems(): List<InfoItem> {
        if (localeContent.isEmpty()) {
            localeContent.add(getSpecificLocale(getString(R.string.locale_default), Locale.getDefault()))
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                localeContent.add(getSpecificLocale(getString(R.string.locale_current), app.resources.configuration.locale))
            } else {
                localeContent.addAll(getConfigurationLocaleItems())
            }

            val locales = Locale.getAvailableLocales()
            for (locale in locales) {
                localeContent.add(getLocaleItem(locale))
            }
        }
        return localeContent
    }
}
