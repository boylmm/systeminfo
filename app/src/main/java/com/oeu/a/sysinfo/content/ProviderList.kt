package com.oeu.a.sysinfo.content

import com.oeu.a.sysinfo.R

//  TODO: Biometric authentication: API 29
//  TODO: Carrier config: API 23
//  TODO: EUICC (Embedded SIM): API 28
//  TODO: Subscription: API 22

//  TODO: Display Hash: API 31

val providerList: List<ProviderItem> = listOf(
    ProviderItem(R.string.item_android, lazy { AndroidInfoProvider() }),
    ProviderItem(R.string.item_screen, lazy { ScreenInfoProvider() }),
    ProviderItem(R.string.item_memory, lazy { MemoryInfoProvider() }),
    ProviderItem(R.string.item_storage, lazy { StorageInfoProvider() }),
    ProviderItem(R.string.item_telephone, lazy { TelephoneInfoProvider() }),
    ProviderItem(R.string.item_camera, lazy { CameraInfoProvider() }),
    ProviderItem(R.string.item_sensor, lazy { SensorInfoProvider() }),
    ProviderItem(R.string.item_input, lazy { InputInfoProvider() }),
    ProviderItem(R.string.item_connectivity, lazy { ConnectivityInfoProvider() }),
    ProviderItem(R.string.item_network, lazy { NetworkInfoProvider() }),
    ProviderItem(R.string.item_location, lazy { LocationInfoProvider() }),
    ProviderItem(R.string.item_account, lazy { AccountInfoProvider() }),
    ProviderItem(R.string.item_codec, lazy { CodecInfoProvider() }),
    ProviderItem(R.string.item_opengl, lazy { OpenGlInfoProvider() }),
    ProviderItem(R.string.item_security, lazy { SecurityInfoProvider() }),
    ProviderItem(R.string.item_infrared, lazy { InfraredInfoProvider() }),
    ProviderItem(R.string.item_biometric, lazy { BiometricInfoProvider() }),
    ProviderItem(R.string.item_drm, lazy { DrmInfoProvider() }),
    ProviderItem(R.string.item_policy, lazy { PolicyInfoProvider() }),
    ProviderItem(R.string.item_system, lazy { SystemInfoProvider() }),
    ProviderItem(R.string.item_locale, lazy { LocaleInfoProvider() }),
    ProviderItem(R.string.item_about, lazy { AboutInfoProvider() })
)
