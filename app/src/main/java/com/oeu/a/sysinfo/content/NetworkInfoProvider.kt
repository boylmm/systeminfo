package com.oeu.a.sysinfo.content

import android.os.Build
import com.oeu.a.sysinfo.R
import com.oeu.a.sysinfo.utils.FirebaseHelper
import com.oeu.a.sysinfo.utils.isNullOrEmpty
import java.net.NetworkInterface
import java.net.SocketException
import java.util.*

class NetworkInfoProvider: InfoProvider() {
    companion object {
        private val TAG = NetworkInfoProvider::class.java.simpleName
    }

    private fun formatHardwareAddress(address: ByteArray?): String {
        if (address.isNullOrEmpty()) {
            return getString(R.string.unknown)
        }
        val builder = StringBuilder()
        builder.append(String.format("%02x", address[0]))
        for (idx in 1 until address.size) {
            builder.append('-').append(String.format("%02x", address[idx]))
        }
        return builder.toString()
    }

    private fun getNetworkItems(network: NetworkInterface): List<InfoItem> {
        val list = mutableListOf<InfoItem>()
        val name = network.displayName ?: network.name
        val builder = StringBuilder()
        builder.append(getString(R.string.network_name)).append(network.name)
        builder.append('\n').append(getString(R.string.network_hardware_address))
        try {
            builder.append(formatHardwareAddress(network.hardwareAddress))
        } catch (e1: SocketException) {
            FirebaseHelper.onThrowableCaught(e1)
            e1.printStackTrace()
            builder.append(formatHardwareAddress(ByteArray(0)))
        }
        try {
            builder.append('\n').append(getString(R.string.network_mtu)).append(network.mtu.toString())
        } catch (e1: SocketException) {
            FirebaseHelper.onThrowableCaught(e1)
            e1.printStackTrace()
        }
        if (Build.VERSION_CODES.KITKAT <= Build.VERSION.SDK_INT) {
            val index = network.index
            builder.append(String.format("\nNetwork index: %d", index))
            if (index < 0) {
                builder.append(getString(R.string.unknown))
            }
        }
        try {
            builder.append('\n').append(getString(if (network.isUp) R.string.network_up else R.string.network_down))
        } catch (e: SocketException) {
            FirebaseHelper.onThrowableCaught(e)
            e.printStackTrace()
        }
        val aList = Collections.list(network.inetAddresses)
        if (0 < aList.size) {
            builder.append('\n').append("iNetAddresses")
            for (ia in aList) {
                val hostName = ia.canonicalHostName
                val hostAddress = ia.hostAddress
                builder.append("\n\t").append(hostName)
                if (hostName != hostAddress) {
                    builder.append('(').append(hostAddress).append(')')
                }
            }
        }
        val subnets = Collections.list(network.subInterfaces)
        if (0 < subnets.size) {
            builder.append('\n').append(getString(R.string.network_subinterface)).append(subnets.size.toString())
        }
        list.add(InfoItem(name, builder.toString()))
        for (subnet in subnets) {
            list.addAll(getNetworkItems(subnet))
        }
        return list
    }

    private val networkContent = mutableListOf<InfoItem>()

    @Suppress("BlockingMethodInNonBlockingContext")
    //  Already calling this with Dispatchers.IO
    override suspend fun getItems(): List<InfoItem> {
        if (networkContent.isEmpty()) {
            val networks = try {
                NetworkInterface.getNetworkInterfaces().toList()
            } catch (se: SocketException) {
                FirebaseHelper.onThrowableCaught(se)
                se.printStackTrace()
                null
            }
            if (networks.isNullOrEmpty()) {
                networkContent.add(InfoItem(R.string.item_network, R.string.network_none))
            } else {
                for (network in networks) {
                    networkContent.addAll(getNetworkItems(network))
                }
            }
        }
        return networkContent
    }
}
