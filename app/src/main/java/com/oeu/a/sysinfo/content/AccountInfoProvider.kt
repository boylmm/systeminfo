package com.oeu.a.sysinfo.content

import android.Manifest
import android.accounts.AccountManager
import android.accounts.AuthenticatorDescription
import android.content.pm.PackageManager
import android.content.res.Resources
import android.util.Log
import com.oeu.a.sysinfo.R
import com.oeu.a.sysinfo.utils.FirebaseHelper

class AccountInfoProvider: InfoProvider() {
    companion object {
        private val TAG = AccountInfoProvider::class.java.simpleName
    }

    override val requiredPermissions = arrayOf(
        Manifest.permission.GET_ACCOUNTS,
        Manifest.permission.READ_CONTACTS
    )

    private fun getAuthenticatorItem(ad: AuthenticatorDescription, pm: PackageManager): InfoItem {
        val name = try {
            pm.getResourcesForApplication(ad.packageName).getString(ad.labelId)
        } catch (e: PackageManager.NameNotFoundException) {
            FirebaseHelper.onThrowableCaught(e)
            e.printStackTrace()
            getString(R.string.unknown)
        } catch (e: Resources.NotFoundException) {
            FirebaseHelper.onThrowableCaught(e)
            e.printStackTrace()
            getString(if (ad.labelId == 0) R.string.no_name else R.string.unsupported)
        }

        val sb = StringBuilder()
        sb.append("Type: ").append(ad.type)
        sb.append("\nPackage: ").append(ad.packageName)
        return InfoItem("Authenticator: $name", sb.toString())
    }

    private val accountContent: MutableList<InfoItem> = mutableListOf()
    override suspend fun getItems(): List<InfoItem> {
        if (accountContent.isEmpty()) {
            val manager = AccountManager.get(app)
            val accounts = manager.accounts
            if (accounts.isEmpty()) {
                Log.i(TAG, "no accounts: size ${accounts.size}")
                accountContent.add(InfoItem(R.string.item_account, R.string.account_none))
            } else {
                for (account in accounts) {
                    accountContent.add(InfoItem("Account: ${account.name}", "type: ${account.type}"))
                }
            }
            val authenticators = manager.authenticatorTypes
            if (authenticators.isEmpty()) {
                accountContent.add(InfoItem(R.string.account_auth, R.string.account_auth_none))
            } else {
                val pm = app.packageManager
                for (authenticator in authenticators) {
                    accountContent.add(getAuthenticatorItem(authenticator, pm))
                }
            }

        }
        return accountContent
    }
}
