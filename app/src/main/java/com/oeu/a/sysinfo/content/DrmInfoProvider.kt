package com.oeu.a.sysinfo.content

import android.annotation.SuppressLint
import android.drm.DrmManagerClient
import android.media.MediaDrm
import android.os.Build
import com.oeu.a.sysinfo.R

class DrmInfoProvider: InfoProvider() {
    companion object {
        private val TAG = DrmInfoProvider::class.java.simpleName
    }

    private val drmContent = mutableListOf<InfoItem>()

    @SuppressLint("newApi")
    override suspend fun getItems(): List<InfoItem> {
        if (drmContent.isEmpty()) {
            when {
                Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB ->
                    drmContent.add(InfoItem(R.string.item_drm, getString(R.string.sdk_version_required, Build.VERSION_CODES.HONEYCOMB)))
                Build.VERSION.SDK_INT < Build.VERSION_CODES.R -> {
                    val client = DrmManagerClient(app)
                    val engines = client.availableDrmEngines
                    if (engines.isEmpty()) {
                        drmContent.add(InfoItem(R.string.item_drm, R.string.drm_none))
                    } else {
                        val builder = StringBuilder()
                        builder.append("- ").append(engines[0])
                        for (idx in 1 until engines.size) {
                            builder.append("\n- ").append(engines[idx])
                        }
                        drmContent.add(InfoItem(R.string.item_drm, builder.toString()))
                    }
                    if (Build.VERSION_CODES.JELLY_BEAN <= Build.VERSION.SDK_INT) {
                        client.release()
                    }
                }
                else -> {
                    val schemes = MediaDrm.getSupportedCryptoSchemes()
                    if (schemes.isEmpty()) {
                        drmContent.add(InfoItem(R.string.item_drm, R.string.drm_none))
                    } else {
                        for (uuid in schemes) {
                            val builder = StringBuilder()
                            val drm = MediaDrm(uuid)
                            builder.append("description: ").append(drm.getPropertyString(MediaDrm.PROPERTY_DESCRIPTION)).append('\n')
                            builder.append("algorithms: ").append(drm.getPropertyString(MediaDrm.PROPERTY_ALGORITHMS)).append('\n')
                            builder.append("vendor: ").append(drm.getPropertyString(MediaDrm.PROPERTY_VENDOR)).append('\n')
                            builder.append("version: ").append(drm.getPropertyString(MediaDrm.PROPERTY_VERSION)).append('\n')
                            drm.metrics?.also {
                                for (key in it.keySet()) {
                                    builder.append("\t$key: ${it[key]}\n")
                                }
                            }
                            builder.deleteCharAt(builder.lastIndex)
                            drmContent.add(InfoItem(uuid.toString(), builder.toString()))
                        }
                    }
                }
            }
        }
        return drmContent
    }
}
