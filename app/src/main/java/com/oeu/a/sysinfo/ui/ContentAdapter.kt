package com.oeu.a.sysinfo.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.oeu.a.sysinfo.R
import com.oeu.a.sysinfo.content.InfoItem
import com.oeu.a.sysinfo.content.LongClickHandler

class ContentAdapter (var items: List<InfoItem>, var longClickHandler: LongClickHandler? = null)
    : RecyclerView.Adapter<ContentAdapter.ContentItemViewHolder>() {
    companion object {
        private val TAG = ContentAdapter::class.java.simpleName
    }
    inner class ContentItemViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val name: TextView = view.findViewById(R.id.content_name)
        val value: TextView = view.findViewById(R.id.content_value)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContentItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.content_item, parent, false)
        return ContentItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ContentItemViewHolder, position: Int) {
        val item = items[position]
        holder.name.text = item.name
        holder.value.text = item.value
        holder.itemView.setOnLongClickListener {
            longClickHandler?.handleLongClick(it, item) ?: false
        }
    }

    override fun getItemCount(): Int = items.size
}
