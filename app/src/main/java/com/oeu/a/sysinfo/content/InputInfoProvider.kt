package com.oeu.a.sysinfo.content

import android.annotation.SuppressLint
import android.os.Build
import android.util.Log
import android.view.InputDevice
import android.view.InputDevice.MotionRange
import android.view.MotionEvent
import com.oeu.a.sysinfo.R
import com.oeu.a.sysinfo.utils.FirebaseHelper
import com.oeu.a.sysinfo.utils.isFlagsSet

class InputInfoProvider: InfoProvider() {
    companion object {
        private val TAG = InputInfoProvider::class.java.simpleName
    }

    private fun formatSource(source: Int): String {
        val sb = StringBuilder()
        if (source.isFlagsSet(InputDevice.SOURCE_BLUETOOTH_STYLUS)) {
            sb.append("\n\tBluetooth Stylus")
        }
        if (source.isFlagsSet(InputDevice.SOURCE_DPAD)) {
            sb.append("\n\tDpad")
        }
        if (source.isFlagsSet(InputDevice.SOURCE_GAMEPAD)) {
            sb.append("\n\tGame pad")
        }
        if (source.isFlagsSet(InputDevice.SOURCE_HDMI)) {
            sb.append("\n\tHDMI")
        }
        if (source.isFlagsSet(InputDevice.SOURCE_JOYSTICK)) {
            sb.append("\n\tJoystick")
        }
        if (source.isFlagsSet(InputDevice.SOURCE_KEYBOARD)) {
            sb.append("\n\tKeyboard")
        }
        if (source.isFlagsSet(InputDevice.SOURCE_MOUSE)) {
            sb.append("\n\tMouse")
        }
        if (source.isFlagsSet(InputDevice.SOURCE_MOUSE_RELATIVE)) {
            sb.append("\n\tMouse (navigation)")
        }
        if (source.isFlagsSet(InputDevice.SOURCE_ROTARY_ENCODER)) {
            sb.append("\n\tRotating encoder (wheel)")
        }
        if (source.isFlagsSet(InputDevice.SOURCE_STYLUS)) {
            sb.append("\n\tStylus")
        }
        if (source.isFlagsSet(InputDevice.SOURCE_TOUCH_NAVIGATION)) {
            sb.append("\n\tTouch navigation")
        }
        if (source.isFlagsSet(InputDevice.SOURCE_TOUCHPAD)) {
            sb.append("\n\tTouchpad")
        }
        if (source.isFlagsSet(InputDevice.SOURCE_TOUCHSCREEN)) {
            sb.append("\n\tTouch screen")
        }
        if (source.isFlagsSet(InputDevice.SOURCE_TRACKBALL)) {
            sb.append("\n\tTrackball")
        }
        if (sb.isEmpty()) {
            sb.append("\n\t").append(getString(R.string.unknown))
        }
        return sb.toString()
    }

    private fun appendSourceClass(sb: StringBuilder, source: Int) {
        val sourceClass = source and InputDevice.SOURCE_CLASS_MASK
        if (sourceClass.isFlagsSet(InputDevice.SOURCE_CLASS_BUTTON)) {
            sb.append("\n\t- Button class")
        }
        if (sourceClass.isFlagsSet(InputDevice.SOURCE_CLASS_JOYSTICK)) {
            sb.append("\n\t- Joystick class")
        }
        if (sourceClass.isFlagsSet(InputDevice.SOURCE_CLASS_POINTER)) {
            sb.append("\n\t- Pointer class")
        }
        if (sourceClass.isFlagsSet(InputDevice.SOURCE_CLASS_POSITION)) {
            sb.append("\n\t- Position class")
        }
        if (sourceClass.isFlagsSet(InputDevice.SOURCE_CLASS_TRACKBALL)) {
            sb.append("\n\t- Trackball class")
        }
        if (sourceClass == InputDevice.SOURCE_CLASS_NONE) {
            sb.append("\n\t- No class")
        }
    }

    private fun formatSources(sources: Int): String {
        val sb = StringBuilder()
        sb.append(String.format("x%08x", sources))
        sb.append(String.format(" (class x%02x)", sources and InputDevice.SOURCE_CLASS_MASK))
        appendSourceClass(sb, sources)
        sb.append(formatSource(sources))
        return sb.toString()
    }

    private fun formatAxis(axis: Int): String {
        val name = when (axis) {
            MotionEvent.AXIS_BRAKE -> "brake"
            MotionEvent.AXIS_DISTANCE -> "distance"
            MotionEvent.AXIS_GAS -> "gas"
            MotionEvent.AXIS_GENERIC_1 -> "generic 1"
            MotionEvent.AXIS_HAT_X -> "hat X"
            MotionEvent.AXIS_HAT_Y -> "hat Y"
            MotionEvent.AXIS_HSCROLL -> "horizontal scroll"
            MotionEvent.AXIS_LTRIGGER -> "left trigger"
            MotionEvent.AXIS_ORIENTATION -> "orientation"
            MotionEvent.AXIS_PRESSURE -> "pressure"
            MotionEvent.AXIS_RTRIGGER -> "right trigger"
            MotionEvent.AXIS_RUDDER -> "rudder"
            MotionEvent.AXIS_RX -> "rotation X"
            MotionEvent.AXIS_RY -> "rotation Y"
            MotionEvent.AXIS_RZ -> "rotation Z"
            MotionEvent.AXIS_SIZE -> "size"
            MotionEvent.AXIS_THROTTLE -> "throttle"
            MotionEvent.AXIS_TILT -> "tilt"
            MotionEvent.AXIS_TOOL_MAJOR -> "major tool"
            MotionEvent.AXIS_TOOL_MINOR -> "minor tool"
            MotionEvent.AXIS_TOUCH_MAJOR -> "major touch"
            MotionEvent.AXIS_TOUCH_MINOR -> "minor touch"
            MotionEvent.AXIS_VSCROLL -> "vertical scroll"
            MotionEvent.AXIS_WHEEL -> "wheel"
            MotionEvent.AXIS_X -> "X"
            MotionEvent.AXIS_Y -> "Y"
            MotionEvent.AXIS_Z -> "Z"
            else -> getString(R.string.unknown)
        }
        return String.format("%s (%d)", name, axis)
    }

    private fun appendMotionRange(sb: StringBuilder, range: MotionRange) {
        if (Build.VERSION_CODES.HONEYCOMB_MR1 <= Build.VERSION.SDK_INT) {
            val axis = range.axis
            sb.append("\t\tAxis: ").append(formatAxis(axis)).append('\n')
        }
        val flat = range.flat
        val fuzz = range.fuzz
        val min = range.min
        val max = range.max
        sb.append(String.format("\t\tvalue %f ~ %f\n\t\tflat %f fuzz %f\n", min, max, flat, fuzz))
        if (Build.VERSION_CODES.JELLY_BEAN_MR2 <= Build.VERSION.SDK_INT) {
            val resolution = range.resolution
            sb.append(String.format("\t\tResolution %f\n", resolution))
        }
    }

    private fun formatMotionRanges(ranges: List<MotionRange>): String {
        val sb = StringBuilder()
        sb.append(String.format("total %d\n", ranges.size))
        for (idx in ranges.indices) {
            sb.append(String.format("\tMotion range %d\n", idx))
            val range = ranges[idx]
            appendMotionRange(sb, range)
        }
        sb.deleteCharAt(sb.lastIndex)
        return sb.toString()
    }

    private fun hasMicrophone(device: InputDevice): Boolean {
        return if (Build.VERSION_CODES.M <= Build.VERSION.SDK_INT) {
            device.hasMicrophone()
        } else false
    }

    private fun hasVibrator(device: InputDevice): Boolean {
        return if (Build.VERSION_CODES.JELLY_BEAN <= Build.VERSION.SDK_INT) {
            device.vibrator.hasVibrator()
        } else false
    }

    private fun isVirtual(device: InputDevice): Boolean {
        return if (Build.VERSION_CODES.JELLY_BEAN <= Build.VERSION.SDK_INT) {
            device.isVirtual
        } else false
    }

    private fun getFeaturesString(device: InputDevice): String {
        val sb = StringBuilder()
        if (hasVibrator(device)) {
            sb.append("\n\tVibrator")
        }
        if (hasMicrophone(device)) {
            sb.append("\n\tMicrophone")
        }
        if (isVirtual(device)) {
            sb.append("\n\t(virtual device)")
        }
        if (Build.VERSION_CODES.O_MR1 <= Build.VERSION.SDK_INT) {
            if (!device.isEnabled) {
                sb.append("\n\tDisabled")
            }
        }
        if (Build.VERSION_CODES.Q <= Build.VERSION.SDK_INT) {
            if (device.isExternal) {
                sb.append("\n\tExternal")
            }
        }
        return if (sb.isNotEmpty()) {
            sb.toString()
        } else ""
    }

    @SuppressLint("NewApi")
    private fun appendInputProperty(sb: StringBuilder, id: Int, device: InputDevice) {
        val title = getString(id)
        val value: String = try {
            when (id) {
                R.string.input_id -> device.id.toString()
                R.string.input_source -> formatSources(device.sources)
                R.string.input_descriptor -> device.descriptor
                R.string.input_product_id -> device.productId.toString()
                R.string.input_vendor_id -> device.vendorId.toString()
                R.string.input_controller_number -> {
                    val number = device.controllerNumber
                    if (0 != number) {
                        number.toString()
                    } else {
                        ""
                    }
                }
                R.string.input_motion_ranges -> {
                    val ranges = device.motionRanges
                    if (null != ranges && 0 < ranges.size) {
                        formatMotionRanges(device.motionRanges)
                    } else {
                        ""
                    }
                }
                R.string.input_other_features -> getFeaturesString(device)
                else -> getString(R.string.invalid_item)
            }
        } catch (e: Error) {
            FirebaseHelper.onThrowableCaught(e)
            e.printStackTrace()
            getString(R.string.unsupported)
        }
        if (value.isNotEmpty()) {
            sb.append(title).append(": ").append(value).append('\n')
        }
    }

    private val itemSpecs = listOf(
        InfoSpec(R.string.input_id, 9),
        InfoSpec(R.string.input_descriptor, 16),
        InfoSpec(R.string.input_vendor_id, 19),
        InfoSpec(R.string.input_product_id, 19),
        InfoSpec(R.string.input_source, 9),
        InfoSpec(R.string.input_controller_number, 19),
        InfoSpec(R.string.input_motion_ranges, 12),
        InfoSpec(R.string.input_other_features, 9),
    )

    private fun getInputItem(deviceId: Int): InfoItem {
        val device = InputDevice.getDevice(deviceId)
        val name: String
        val sb = java.lang.StringBuilder()
        if (null == device) {
            name = String.format("ID: %d", deviceId)
            sb.append(getString(R.string.unknown))
        } else {
            name = device.name
            for (spec in itemSpecs) {
                if (spec.minSdk <= Build.VERSION.SDK_INT) {
                    appendInputProperty(sb, spec.titleId, device)
                }
            }
            sb.deleteCharAt(sb.lastIndex)
        }
        return InfoItem(name, sb.toString())
    }

    private val inputContent = mutableListOf<InfoItem>()

    override suspend fun getItems(): List<InfoItem> {
        if (inputContent.isEmpty()) {
            val ids = InputDevice.getDeviceIds()
            if (ids.isEmpty()) {
                inputContent.add(InfoItem(R.string.item_input, R.string.input_none))
            } else {
                for (id in ids) {
                    inputContent.add(getInputItem(id))
                }
            }
        }
        return inputContent
    }
}
