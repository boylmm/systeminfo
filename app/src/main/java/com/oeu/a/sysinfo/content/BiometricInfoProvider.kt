package com.oeu.a.sysinfo.content

import android.Manifest
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.biometric.BiometricManager
import com.oeu.a.sysinfo.R

class BiometricInfoProvider: InfoProvider() {
    companion object {
        private val TAG = BiometricInfoProvider::class.java.simpleName
    }

    private val biometricContent = mutableListOf<InfoItem>()

    @RequiresApi(Build.VERSION_CODES.P)
    override val requiredPermissions = arrayOf(
        Manifest.permission.USE_BIOMETRIC
    )

    private fun formatAuthenticatorClass(authClass: Int): String {
        return when (authClass) {
            BiometricManager.Authenticators.BIOMETRIC_STRONG -> "Class 3 (strong)"
            BiometricManager.Authenticators.BIOMETRIC_WEAK -> "Class 2 (weak)"
            else -> "Authenticators: " + getString(R.string.unknown_int_value, authClass)
        }
    }

    private fun formatBiometricCapability(authClass: Int, result: Int): String {
        return when (result) {
            BiometricManager.BIOMETRIC_SUCCESS -> formatAuthenticatorClass(authClass)
            BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> "* Hardware unavailable"
            BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> "* None enrolled"
            BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE -> "* No hardware"
            BiometricManager.BIOMETRIC_ERROR_SECURITY_UPDATE_REQUIRED -> "* Security update required"
            BiometricManager.BIOMETRIC_ERROR_UNSUPPORTED-> "* Unsupported"
            BiometricManager.BIOMETRIC_STATUS_UNKNOWN -> "* Unknown status"
            else -> getString(R.string.unknown_int_value, result)
        }
    }

    private fun getBiometricItems(): List<InfoItem> {
        val items = mutableListOf<InfoItem>()
        val manager = BiometricManager.from(app)
        val value = if (manager.canAuthenticate(BiometricManager.Authenticators.BIOMETRIC_STRONG) == BiometricManager.BIOMETRIC_SUCCESS) {
            formatBiometricCapability(BiometricManager.Authenticators.BIOMETRIC_STRONG, BiometricManager.BIOMETRIC_SUCCESS)
        } else {
            formatBiometricCapability(BiometricManager.Authenticators.BIOMETRIC_WEAK, manager.canAuthenticate(BiometricManager.Authenticators.BIOMETRIC_WEAK))
        }
        items.add(InfoItem(R.string.biometric_authentication, value))
        return items
    }

    override suspend fun getItems(): List<InfoItem> {
        if (biometricContent.isEmpty()) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                biometricContent.add(InfoItem(R.string.item_biometric, getString(R.string.sdk_version_required, Build.VERSION_CODES.M)))
            } else {
                biometricContent.addAll(getBiometricItems())
            }
        }
        return biometricContent
    }
}
