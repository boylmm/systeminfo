package com.oeu.a.sysinfo.utils

import android.os.BaseBundle
import android.os.Build
import androidx.annotation.RequiresApi
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.contract

inline fun <T1: Any, T2: Any, R: Any> with(p1: T1?, p2: T2?, block: (T1, T2)->R?): R? {
    return if (p1 != null && p2 != null) block(p1, p2) else null
}

fun Int.isFlagsSet(flags: Int): Boolean {
    return ((this and flags) == flags)
}

@OptIn(ExperimentalContracts::class)
inline fun ByteArray?.isNullOrEmpty(): Boolean {
    contract {
        returns(false) implies (this@isNullOrEmpty != null)
    }
    return this == null || this.size == 0
}

@OptIn(ExperimentalContracts::class)
@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
inline fun BaseBundle?.isNullOrEmpty(): Boolean {
    contract {
        returns(false) implies (this@isNullOrEmpty != null)
    }
    return this == null || this.isEmpty
}

fun StringBuilder.trimEnd(): StringBuilder {
    while (this[lastIndex].isWhitespace()) deleteCharAt(lastIndex)
    return this
}

fun Any?.className(): String = if (null == this) "null" else this::class.java.simpleName
