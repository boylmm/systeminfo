package com.oeu.a.sysinfo.content

import android.annotation.SuppressLint
import android.opengl.*
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import com.oeu.a.sysinfo.R
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.withContext
import javax.microedition.khronos.egl.*
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.egl.EGLContext
import javax.microedition.khronos.egl.EGLDisplay
import javax.microedition.khronos.egl.EGLSurface

class OpenGlInfoProvider: InfoProvider() {
    companion object {
        private val TAG = OpenGlInfoProvider::class.java.simpleName
        private const val MAGIC_INT = -7151
        private const val MAGIC_FLOAT = -7151.1617F
    }

    interface GlHelper {
        fun prepare()
        fun destroy()
    }

    private class GlHelperV10 : GlHelper {
        var display: EGLDisplay? = null
        var surface: EGLSurface? = null
        var context: EGLContext? = null
        override fun prepare() {
            val egl = EGLContext.getEGL() as EGL10
            display = egl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY)
            val vers = IntArray(2)
            egl.eglInitialize(display, vers)
            val configAttr = intArrayOf(
                EGL10.EGL_COLOR_BUFFER_TYPE, EGL10.EGL_RGB_BUFFER,
                EGL10.EGL_LEVEL, 0,
                EGL10.EGL_SURFACE_TYPE, EGL10.EGL_PBUFFER_BIT,
                EGL10.EGL_NONE
            )
            val configs = arrayOfNulls<EGLConfig>(1)
            val numConfig = IntArray(1)
            egl.eglChooseConfig(display, configAttr, configs, 1, numConfig)
            if (numConfig[0] == 0) {
                // TROUBLE! No config found.
                Log.e(TAG, "No configuration with ${this::class.java.simpleName}")
            }
            val config = configs[0]
            val surfAttr = intArrayOf(
                EGL10.EGL_WIDTH, 64,
                EGL10.EGL_HEIGHT, 64,
                EGL10.EGL_NONE
            )
            surface = egl.eglCreatePbufferSurface(display, config, surfAttr)
            val EGL_CONTEXT_CLIENT_VERSION = 0x3098 // missing in EGL10
            val ctxAttrib = intArrayOf(
                EGL_CONTEXT_CLIENT_VERSION, 1,
                EGL10.EGL_NONE
            )
            context = egl.eglCreateContext(display, config, EGL10.EGL_NO_CONTEXT, ctxAttrib)
            egl.eglMakeCurrent(display, surface, surface, context)
        }

        override fun destroy() {
            val egl = EGLContext.getEGL() as EGL10
            egl.eglMakeCurrent(display, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT)
            egl.eglDestroySurface(display, surface)
            egl.eglDestroyContext(display, context)
            egl.eglTerminate(display)
        }
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private class GlHelperV14 : GlHelper {
        private var display: android.opengl.EGLDisplay? = null
        private var surface: android.opengl.EGLSurface? = null
        private var context: android.opengl.EGLContext? = null
        override fun prepare() {
            //  Display
            display = EGL14.eglGetDisplay(EGL14.EGL_DEFAULT_DISPLAY)
            val ver = IntArray(2)
            EGL14.eglInitialize(display, ver, 0, ver, 1)

            // Config
            val configAttr = intArrayOf(
                EGL14.EGL_COLOR_BUFFER_TYPE, EGL14.EGL_RGB_BUFFER,
                EGL14.EGL_LEVEL, 0,
                EGL14.EGL_RENDERABLE_TYPE, EGL14.EGL_OPENGL_ES2_BIT,
                EGL14.EGL_SURFACE_TYPE, EGL14.EGL_PBUFFER_BIT,
                EGL14.EGL_NONE
            )
            val configs = arrayOfNulls<android.opengl.EGLConfig>(1)
            val numConfig = IntArray(1)
            EGL14.eglChooseConfig(display, configAttr, 0, configs, 0, 1, numConfig, 0)
            if (numConfig[0] == 0) {
                // TROUBLE! No config found.
                Log.e(TAG, "No configuration with ${this::class.java.simpleName}")
            }
            val config = configs[0]

            //  Surface
            val surfAttr = intArrayOf(
                EGL14.EGL_WIDTH, 64,
                EGL14.EGL_HEIGHT, 64,
                EGL14.EGL_NONE
            )
            surface = EGL14.eglCreatePbufferSurface(display, config, surfAttr, 0)

            //  Context
            val ctxAttrib = intArrayOf(
                EGL14.EGL_CONTEXT_CLIENT_VERSION, 2,
                EGL14.EGL_NONE
            )
            context = EGL14.eglCreateContext(display, config, EGL14.EGL_NO_CONTEXT, ctxAttrib, 0)
            EGL14.eglMakeCurrent(display, surface, surface, context)
        }

        override fun destroy() {
            EGL14.eglMakeCurrent(display, EGL14.EGL_NO_SURFACE, EGL14.EGL_NO_SURFACE, EGL14.EGL_NO_CONTEXT)
            EGL14.eglDestroySurface(display, surface)
            EGL14.eglDestroyContext(display, context)
            EGL14.eglTerminate(display)
        }
    }

    fun getGlHelper(): GlHelper {
        return if (Build.VERSION_CODES.JELLY_BEAN_MR1 <= Build.VERSION.SDK_INT) {
            GlHelperV14()
        } else {
            GlHelperV10()
        }
    }

    private fun formatOpenGLExtensions(src: String?): String {
        return if (null == src) {
            getString(R.string.invalid_item)
        } else {
            formatStringArray(*src.trim().split(Regex("\\s+")).sorted().toTypedArray())
        }
    }

    private fun formatSize(size: IntArray): String {
        return String.format("%d x %d", size[0], size[1])
    }

    private val glOutInt = IntArray(2)
    private fun invokeGlGetIntegerv(element: Int, params: IntArray = glOutInt, offset: Int = 0) {
        @SuppressLint("newApi")
        when {
            Build.VERSION.SDK_INT < Build.VERSION_CODES.FROYO -> GLES11.glGetIntegerv(element, params, offset)
            Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2 -> GLES20.glGetIntegerv(element, params, offset)
            Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP -> GLES30.glGetIntegerv(element, params, offset)
            Build.VERSION.SDK_INT < Build.VERSION_CODES.N -> GLES31.glGetIntegerv(element, params, offset)
            else -> GLES32.glGetIntegerv(element, params, offset)
        }
    }

    private fun getGlInteger(element: Int): String? {
        glOutInt[0] = MAGIC_INT
        invokeGlGetIntegerv(element, glOutInt, 0)
        return if (glOutInt[0] != MAGIC_INT) String.format("%,d", glOutInt[0]) else null
    }

    private fun getGlSize(element: Int): String? {
        glOutInt[0] = MAGIC_INT
        invokeGlGetIntegerv(element, glOutInt, 0)
        return if (glOutInt[0] != MAGIC_INT) formatSize(glOutInt) else null
    }

    private val glOutFloat = FloatArray(2)
    private fun invokeGlGetFloatv(element: Int, params: FloatArray = glOutFloat, offset: Int = 0) {
        @SuppressLint("newApi")
        when {
            Build.VERSION.SDK_INT < Build.VERSION_CODES.FROYO -> GLES11.glGetFloatv(element, params, offset)
            Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2 -> GLES20.glGetFloatv(element, params, offset)
            Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP -> GLES30.glGetFloatv(element, params, offset)
            Build.VERSION.SDK_INT < Build.VERSION_CODES.N -> GLES31.glGetFloatv(element, params, offset)
            else -> GLES32.glGetFloatv(element, params, offset)
        }
    }

    private fun getGlFloat(element: Int): String? {
        glOutFloat[0] = MAGIC_FLOAT
        invokeGlGetFloatv(element, glOutFloat, 0)
        return if (glOutFloat[0] != MAGIC_FLOAT) glOutFloat[0].toString() else null
    }

    private fun getOpenGLItem(id: Int): InfoItem {
        val value = when (id) {
            R.string.opengl_version -> GLES10.glGetString(GLES10.GL_VERSION)
            R.string.opengl_vendor -> GLES10.glGetString(GLES10.GL_VENDOR)
            R.string.opengl_renderer -> GLES10.glGetString(GLES10.GL_RENDERER)
            R.string.opengl_shading_language_version -> getGlFloat(GLES20.GL_SHADING_LANGUAGE_VERSION)
            R.string.opengl_max_elements_indices -> getGlInteger(GLES10.GL_MAX_ELEMENTS_INDICES)
            R.string.opengl_max_elements_vertices -> getGlInteger(GLES10.GL_MAX_ELEMENTS_VERTICES)
            R.string.opengl_max_lights -> getGlInteger(GLES10.GL_MAX_LIGHTS)
            R.string.opengl_max_modelview_stack_depth -> getGlInteger(GLES10.GL_MAX_MODELVIEW_STACK_DEPTH)
            R.string.opengl_max_projection_stack_depth -> getGlInteger(GLES10.GL_MAX_PROJECTION_STACK_DEPTH)
            R.string.opengl_max_texture_size -> getGlInteger(GLES10.GL_MAX_TEXTURE_SIZE)
            R.string.opengl_max_texture_stack_depth -> getGlInteger(GLES10.GL_MAX_TEXTURE_STACK_DEPTH)
            R.string.opengl_max_texture_units -> getGlInteger(GLES10.GL_MAX_TEXTURE_UNITS)
            R.string.opengl_max_viewport_dims -> getGlSize(GLES10.GL_MAX_VIEWPORT_DIMS)
            R.string.opengl_extensions -> formatOpenGLExtensions(GLES10.glGetString(GLES10.GL_EXTENSIONS))
            else -> getString(R.string.invalid_item)
        }
        return value?.let { InfoItem(id, it) } ?: InfoItem(id, R.string.unsupported)
    }

    private val itemSpecs = listOf(
        InfoSpec(R.string.opengl_version),
        InfoSpec(R.string.opengl_vendor),
        InfoSpec(R.string.opengl_renderer),
//        InfoSpec(R.string.opengl_shading_language_version),
        InfoSpec(R.string.opengl_max_texture_size),
        InfoSpec(R.string.opengl_max_viewport_dims),
        InfoSpec(R.string.opengl_max_elements_indices),
        InfoSpec(R.string.opengl_max_elements_vertices),
//        InfoSpec(R.string.opengl_max_lights),
//        InfoSpec(R.string.opengl_max_modelview_stack_depth),
//        InfoSpec(R.string.opengl_max_projection_stack_depth),
//        InfoSpec(R.string.opengl_max_texture_stack_depth),
//        InfoSpec(R.string.opengl_max_texture_units),
        InfoSpec(R.string.opengl_extensions),
    )

    private val openGlContent = mutableListOf<InfoItem>()

    override suspend fun getItems(): List<InfoItem> {
        if (openGlContent.isEmpty()) {
            for (spec in itemSpecs) {
                val item = withContext(MainScope().coroutineContext) {
                    getOpenGLItem(spec.titleId)
                }
                openGlContent.add(item)
            }
        }
        return openGlContent
    }
}
