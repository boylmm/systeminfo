package com.oeu.a.sysinfo.content

import android.annotation.SuppressLint
import android.content.Context
import android.net.*
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import com.oeu.a.sysinfo.R
import com.oeu.a.sysinfo.utils.with

class ConnectivityInfoProvider: InfoProvider() {
    companion object {
        private val TAG = ConnectivityInfoProvider::class.java.simpleName
    }

    @SuppressLint("newAPI")
    private fun formatDefaultProxy(proxyInfo: ProxyInfo?): String {
        val builder = StringBuilder()
        proxyInfo?.also { info ->
            if (info.host.isNotEmpty()) {
                builder.append("${info.host}:${info.port}\n")
            }
            info.pacFileUrl?.also {
                builder.append(it.toString()).append('\n')
            }
            if (info.exclusionList.isNotEmpty()) {
                builder.append("exclusion list\n")
                for (host in info.exclusionList) {
                    builder.append('\t').append(host).append('\n')
                }
            }
            builder.deleteCharAt(builder.lastIndex)
        }
        return if (builder.isNotEmpty()) builder.toString()
               else getString(R.string.none)
    }

    private fun formatName(ni: NetworkInfo): String {
        val subtype = ni.subtypeName
        return if (subtype.isNullOrEmpty()) {
            ni.typeName
        } else String.format("%s (%s)", ni.typeName, subtype)
    }

    private fun formatNetworkState(ni: NetworkInfo): String {
        val sb = java.lang.StringBuilder()
        sb.append(getString(R.string.connectivity_state))
            .append(": ").append(ni.state)
            .append(" (").append(ni.detailedState).append(')')
        if (!ni.isAvailable) {
            sb.append("\n\tUnavailable")
        }
        if (ni.isRoaming) {
            sb.append("\n\tRoaming")
        }
        val ei = ni.extraInfo
        if (null != ei && ei.isNotEmpty()) {
            sb.append("\n\t\textra: ").append(ei)
        }
        return sb.toString()
    }

    private val globalSpecs = listOf(
        InfoSpec(R.string.connectivity_default_proxy, 23)
    )
    private val globalIds = globalSpecs.map { it.titleId }.toSet()

    private val networkSpecs = listOf(
        InfoSpec(R.string.connectivity_network_state),
        InfoSpec(R.string.connectivity_network_capability, 21),
        InfoSpec(R.string.connectivity_network_bandwidth, 21),
    )
    private val networkIds = networkSpecs.map { it.titleId }.toSet()

    private fun getNetworkInfoItem(networkInfo: NetworkInfo): InfoItem {
        val name = formatName(networkInfo)
        val value = formatNetworkState(networkInfo)
        return InfoItem(name, value)
    }

    @SuppressLint("newApi")
    private fun getGlobalItem(infoId: Int, manager: ConnectivityManager): InfoItem {
        val value = when (infoId) {
            R.string.connectivity_default_proxy -> formatDefaultProxy(manager.defaultProxy)
            else -> getString(R.string.invalid_item)
        }
        return InfoItem(infoId, value)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun determineNetworkName(manager: ConnectivityManager, network: Network): String {
        manager.getNetworkInfo(network)?.also {
            return formatName(it)
        }
        manager.getLinkProperties(network)?.also {
            if (!it.interfaceName.isNullOrEmpty()) {
                return it.interfaceName.toString()
            }
        }
        return getString(R.string.unknown)
    }

    private data class CapabilityItem(val value: Int, val description: String)

    @SuppressLint("newApi")
    private val capabilityItems = listOf(
        CapabilityItem(NetworkCapabilities.NET_CAPABILITY_CAPTIVE_PORTAL, "Captive portal"),
        CapabilityItem(NetworkCapabilities.NET_CAPABILITY_CBS, "CBS"),
        CapabilityItem(NetworkCapabilities.NET_CAPABILITY_DUN, "DUN (Dial-up networking)"),
        CapabilityItem(NetworkCapabilities.NET_CAPABILITY_EIMS, "EIMS (Emergency IMS)"),
        CapabilityItem(NetworkCapabilities.NET_CAPABILITY_FOREGROUND, "Foreground (available to apps)"),
        CapabilityItem(NetworkCapabilities.NET_CAPABILITY_FOTA, "FOTA (Firmware over the air update)"),
        CapabilityItem(NetworkCapabilities.NET_CAPABILITY_IA, "IA (Initial Attach)"),
        CapabilityItem(NetworkCapabilities.NET_CAPABILITY_IMS, "IMS (IP multimedia subsystem)"),
        CapabilityItem(NetworkCapabilities.NET_CAPABILITY_INTERNET, "Internet"),
        CapabilityItem(NetworkCapabilities.NET_CAPABILITY_MCX, "MCX (Mission critical servers)"),
        CapabilityItem(NetworkCapabilities.NET_CAPABILITY_MMS, "MMS (Multimedia messaging service)"),
        CapabilityItem(NetworkCapabilities.NET_CAPABILITY_NOT_CONGESTED, "Not congested"),
        CapabilityItem(NetworkCapabilities.NET_CAPABILITY_NOT_METERED, "Not metered"),
        CapabilityItem(NetworkCapabilities.NET_CAPABILITY_NOT_RESTRICTED, "Not restricted (General use)"),
        CapabilityItem(NetworkCapabilities.NET_CAPABILITY_NOT_ROAMING, "Not roaming"),
        CapabilityItem(NetworkCapabilities.NET_CAPABILITY_NOT_SUSPENDED, "Not suspended"),
        CapabilityItem(NetworkCapabilities.NET_CAPABILITY_NOT_VPN, "Not VPN (Virtual private network)"),
        CapabilityItem(NetworkCapabilities.NET_CAPABILITY_RCS, "RCS (Rich communication service)"),
        CapabilityItem(NetworkCapabilities.NET_CAPABILITY_SUPL, "SUPL (Secure user plane location)"),
        CapabilityItem(NetworkCapabilities.NET_CAPABILITY_TEMPORARILY_NOT_METERED, "Not metered temporarily"),
        CapabilityItem(NetworkCapabilities.NET_CAPABILITY_TRUSTED, "Trusted"),
        CapabilityItem(NetworkCapabilities.NET_CAPABILITY_VALIDATED, "Validated"),
        CapabilityItem(NetworkCapabilities.NET_CAPABILITY_WIFI_P2P, "Wifi P2P"),
        CapabilityItem(NetworkCapabilities.NET_CAPABILITY_XCAP, "XCAP (XML configuration access protocol)"),
    )

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun formatCapabilities(capabilities: NetworkCapabilities): String {
        val builder = StringBuilder()
        for (item in capabilityItems) {
            if (capabilities.hasCapability(item.value)) {
                builder.append("${item.description} (${item.value})\n")
            }
        }
        builder.deleteCharAt(builder.lastIndex)
        return if (builder.isNotEmpty()) builder.toString() else getString(R.string.no_capability)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun formatBandwidth(nc: NetworkCapabilities): String {
        return String.format("Down: %,d kbps\nUp: %,d kbps", nc.linkDownstreamBandwidthKbps, nc.linkUpstreamBandwidthKbps)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun getNetworkItem(infoId: Int, manager: ConnectivityManager, network: Network, prefix: String): InfoItem? {
        val value: String? = when (infoId) {
            R.string.connectivity_network_state -> manager.getNetworkInfo(network)?.let { formatNetworkState(it) }
            R.string.connectivity_network_capability -> manager.getNetworkCapabilities(network)?.let { formatCapabilities(it) }
            R.string.connectivity_network_bandwidth -> manager.getNetworkCapabilities(network)?.let { formatBandwidth(it) }
            else -> getString(R.string.invalid_item)
        }

        return value?.let {
            InfoItem("$prefix: ${getString(infoId)}", it)
        }
    }

    @SuppressLint("newApi")
    override fun getItem(infoId: Int, vararg args: Any?): InfoItem? {
        if (globalIds.contains(infoId)) {
            return (args[0] as? ConnectivityManager)?.let {
                getGlobalItem(infoId, it)
            }
        }
        if (networkIds.contains(infoId)) {
            return with(args[0] as? ConnectivityManager, args[1] as? Network) { manager, network ->
                val prefix = determineNetworkName(manager, network)
                getNetworkItem(infoId, manager, network, prefix)
            }
        }

        return InfoItem(infoId, R.string.invalid_item)
    }

    private val connectivityContent = mutableListOf<InfoItem>()
    override suspend fun getItems(): List<InfoItem> {
        if (connectivityContent.isEmpty()) {
            val manager = app.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                val infos = manager.allNetworkInfo
                for (info in infos) {
                    connectivityContent.add(getNetworkInfoItem(info))
                }
            } else {
                val networks = manager.allNetworks
                for (network in networks) {
                    connectivityContent.addItems(networkSpecs, manager, network)
                }
            }
            connectivityContent.addItems(globalSpecs, manager)
        }
        return connectivityContent
    }
}