package com.oeu.a.sysinfo.content

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import android.os.Build
import android.os.PersistableBundle
import android.telecom.PhoneAccount
import android.telecom.PhoneAccountHandle
import android.telecom.TelecomManager
import android.telephony.SubscriptionManager
import android.telephony.TelephonyManager
import android.telephony.emergency.EmergencyNumber
import android.util.Log
import androidx.annotation.RequiresApi
import com.oeu.a.sysinfo.R
import com.oeu.a.sysinfo.utils.*
import java.net.URLDecoder
import kotlin.text.trimEnd

class TelephoneInfoProvider: InfoProvider() {
    companion object {
        private val TAG = TelephoneInfoProvider::class.java.simpleName
    }

    override val requiredPermissions: Array<String> =
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) arrayOf(
            Manifest.permission.READ_PHONE_STATE,
        ) else arrayOf(
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.READ_PHONE_NUMBERS,
        )

    private fun formatPhoneType(phoneType: Int): String {
        val name = when (phoneType) {
            TelephonyManager.PHONE_TYPE_CDMA -> "CDMA"
            TelephonyManager.PHONE_TYPE_GSM -> "GSM"
            TelephonyManager.PHONE_TYPE_SIP -> "SIP"
            TelephonyManager.PHONE_TYPE_NONE -> getString(R.string.unknown)
            else -> getString(R.string.unknown)
        }
        return String.format("%s (%d)", name, phoneType)
    }

    private fun networkTypeName(type: Int): String {
        return  when (type) {
            TelephonyManager.NETWORK_TYPE_1xRTT -> "1xRTT ($type)"
            TelephonyManager.NETWORK_TYPE_CDMA -> "CDMA ($type)"
            TelephonyManager.NETWORK_TYPE_EDGE -> "EDGE ($type)"
            TelephonyManager.NETWORK_TYPE_EHRPD -> "EHRPD ($type)"
            TelephonyManager.NETWORK_TYPE_EVDO_0 -> "EVDO_0 ($type)"
            TelephonyManager.NETWORK_TYPE_EVDO_A -> "EVDO_A ($type)"
            TelephonyManager.NETWORK_TYPE_EVDO_B -> "EVDO_B ($type)"
            TelephonyManager.NETWORK_TYPE_GSM -> "GSM ($type)"
            TelephonyManager.NETWORK_TYPE_GPRS -> "GPRS ($type)"
            TelephonyManager.NETWORK_TYPE_HSDPA -> "HSDPA ($type)"
            TelephonyManager.NETWORK_TYPE_HSPA -> "HSPA ($type)"
            TelephonyManager.NETWORK_TYPE_HSPAP -> "HSPAP ($type)"
            TelephonyManager.NETWORK_TYPE_HSUPA -> "HSUPA ($type)"
            TelephonyManager.NETWORK_TYPE_IDEN -> "IDEN ($type)"
            TelephonyManager.NETWORK_TYPE_IWLAN -> "IWLAN ($type)"
            TelephonyManager.NETWORK_TYPE_LTE -> "LTE ($type)"
            TelephonyManager.NETWORK_TYPE_NR -> "NR 5G ($type)"
            TelephonyManager.NETWORK_TYPE_TD_SCDMA -> "TD_SCDMA ($type)"
            TelephonyManager.NETWORK_TYPE_UMTS -> "UMTS ($type)"
            TelephonyManager.NETWORK_TYPE_UNKNOWN -> "Unknown  ($type)"
            else -> getString(R.string.unknown_int_value, type)
        }
    }

    @SuppressLint("MissingPermission")
    private fun formatNetworkType(manager: TelephonyManager): String {
        val networkType = if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) manager.networkType else manager.dataNetworkType
        return networkTypeName(networkType)
    }

    private fun formatSimState(simState: Int): String {
        val name = when (simState) {
            TelephonyManager.SIM_STATE_ABSENT -> "Absent"
            TelephonyManager.SIM_STATE_NETWORK_LOCKED -> "Network locked"
            TelephonyManager.SIM_STATE_PIN_REQUIRED -> "PIN required"
            TelephonyManager.SIM_STATE_PUK_REQUIRED -> "PUK required"
            TelephonyManager.SIM_STATE_READY -> "Ready"
            TelephonyManager.SIM_STATE_UNKNOWN -> getString(R.string.unknown)
            else -> getString(R.string.unknown)
        }
        return String.format("%s (%d)", name, simState)
    }

    @SuppressLint("MissingPermission")
    private fun getFeatures(tm: TelephonyManager): String {
        val builder = StringBuilder()
        if (tm.isNetworkRoaming) {
            builder.append("Roaming")
        } else {
            builder.append("Not Roaming")
        }
        if (tm.hasIccCard()) {
            builder.append("\nICC card")
        }
        if (Build.VERSION_CODES.LOLLIPOP <= Build.VERSION.SDK_INT) {
            if (tm.isSmsCapable) {
                builder.append("\nSMS")
            }
        }
        if (Build.VERSION_CODES.LOLLIPOP_MR1 <= Build.VERSION.SDK_INT) {
            if (tm.isVoiceCapable) {
                builder.append("\nVoice call")
            }
            if (tm.hasCarrierPrivileges()) {
                builder.append("\nCarrier Privileges")
            }
        }
        if (Build.VERSION_CODES.M <= Build.VERSION.SDK_INT) {
            if (tm.isHearingAidCompatibilitySupported) {
                builder.append("\nHearing Aid Compatibility support")
            }
            if (tm.isTtyModeSupported) {
                builder.append("\nTTY(Teletypewriter) support")
            }
            if (tm.isWorldPhone) {
                builder.append("\nWorld phone")
            }
            if (tm.canChangeDtmfToneLength()) {
                builder.append("\nCan change DTMF tone length")
            }
        }
        if (Build.VERSION_CODES.O <= Build.VERSION.SDK_INT) {
            if (tm.isDataEnabled) {
                builder.append("\nMobile data enabled")
            }
            if (tm.isConcurrentVoiceAndDataSupported) {
                builder.append("\nConcurrent voice and data support")
            }
        }
        if (Build.VERSION_CODES.Q <= Build.VERSION.SDK_INT) {
            if (tm.isDataRoamingEnabled) {
                builder.append("\nData roaming enabled")
            }
            if (tm.doesSwitchMultiSimConfigTriggerReboot()) {
                builder.append("\nSwitching SIM triggers reboot")
            }
            if (tm.isRttSupported) {
                builder.append("\nRTT(Real-time text) support")
            }
        }
        return builder.toString()
    }

    private fun formatEuiccId(id: Int): String {
        return when (id) {
            TelephonyManager.UNINITIALIZED_CARD_ID -> "Uninitialized ($id)"
            TelephonyManager.UNSUPPORTED_CARD_ID -> "Unsupported ($id)"
            else -> id.toString()
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun formatCarrierConfig(config: PersistableBundle?): String {
        return if (config.isNullOrEmpty()) {
            getString(R.string.none)
        } else {
            val builder = StringBuilder()
            for (key in config.keySet().toSortedSet()) {
                when (val value = config[key]) {
                    null -> getString(R.string.none)
                    is IntArray -> {
                        builder.append("$key:\n")
                        for (e in value) {
                            builder.append("\t$e\n")
                        }
                    }
                    is Array<*> -> {
                        builder.append("$key:\n")
                        for (e in value) {
                            builder.append("\t$e\n")
                        }
                    }
                    else -> builder.append("$key: $value\n")
                }
            }
            builder.trimEnd().toString()
        }
    }

    private fun formatEmergencyCallRouting(routing: Int): String {
        return when (routing) {
            EmergencyNumber.EMERGENCY_CALL_ROUTING_NORMAL -> "Normal (${routing})"
            EmergencyNumber.EMERGENCY_CALL_ROUTING_EMERGENCY -> "Emergency (${routing})"
            EmergencyNumber.EMERGENCY_CALL_ROUTING_UNKNOWN -> "Unknown (${routing})"
            else -> getString(R.string.unknown_int_value, routing)
        }
    }

    private fun formatEmergencyCategory(category: Int): String {
        return when (category) {
            EmergencyNumber.EMERGENCY_SERVICE_CATEGORY_AIEC -> "AIEC ($category)"
            EmergencyNumber.EMERGENCY_SERVICE_CATEGORY_AMBULANCE -> "Ambulance ($category)"
            EmergencyNumber.EMERGENCY_SERVICE_CATEGORY_FIRE_BRIGADE -> "Fire brigade ($category)"
            EmergencyNumber.EMERGENCY_SERVICE_CATEGORY_MARINE_GUARD -> "Marine guard ($category)"
            EmergencyNumber.EMERGENCY_SERVICE_CATEGORY_MIEC -> "MIEC ($category)"
            EmergencyNumber.EMERGENCY_SERVICE_CATEGORY_MOUNTAIN_RESCUE -> "Mountain rescue ($category)"
            EmergencyNumber.EMERGENCY_SERVICE_CATEGORY_POLICE -> "Police ($category)"
            EmergencyNumber.EMERGENCY_SERVICE_CATEGORY_UNSPECIFIED -> "Unspecified ($category) meaning all"
            else -> getString(R.string.unknown_int_value, category)
        }
    }

    private fun formatEmergencyNumberSource(source: Int): String {
        return when (source) {
            EmergencyNumber.EMERGENCY_NUMBER_SOURCE_DATABASE -> "Database ($source)"
            EmergencyNumber.EMERGENCY_NUMBER_SOURCE_DEFAULT -> "Default ($source)"
            EmergencyNumber.EMERGENCY_NUMBER_SOURCE_MODEM_CONFIG -> "Modem config ($source)"
            EmergencyNumber.EMERGENCY_NUMBER_SOURCE_NETWORK_SIGNALING -> "Network ($source)"
            EmergencyNumber.EMERGENCY_NUMBER_SOURCE_SIM -> "SIM ($source)"
            else -> getString(R.string.unknown_int_value, source)
        }
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    private fun formatEmergencyNumbers(numberMap: Map<Int, List<EmergencyNumber>>): String {
        val builder = StringBuilder()
        for (key in numberMap.keys) {
            builder.append(key.toString())
            if (key == SubscriptionManager.getDefaultSubscriptionId()) {
                builder.append(" (Default)")
            }
            builder.append('\n')
            numberMap[key]?.also {
                for (number in it) {
                    builder.append("\t${number.countryIso}: ${number.mnc}: ${number.number}\n")
                    builder.append("\t Routing: ").append(formatEmergencyCallRouting(number.emergencyCallRouting)).append('\n')
                    builder.append("\t- Categories\n")
                    for (category in number.emergencyServiceCategories) {
                        builder.append("\t\t").append(formatEmergencyCategory(category)).append('\n')
                    }
                    builder.append("\t- Sources\n")
                    for (source in number.emergencyNumberSources) {
                        builder.append("\t\t").append(formatEmergencyNumberSource(source)).append('\n')
                    }
                    if (number.emergencyUrns.isNotEmpty()) {
                        builder.append("\t- URN\n")
                        for (urn in number.emergencyUrns) {
                            builder.append("\t\t$urn\n")
                        }
                    }
                }
            }
        }
        return builder.toString()
    }

    private fun formatCarrier(id: Int, name: CharSequence?): String {
        val builder = StringBuilder()
        if (id == TelephonyManager.UNKNOWN_CARRIER_ID) {
            builder.append("Unknown ID ($id)")
        } else {
            builder.append(id.toString())
        }
        builder.append(": ").append(name ?: getString(R.string.not_available))
        return builder.toString()
    }

    @RequiresApi(Build.VERSION_CODES.P)
    private fun formatSimCarrier(manager: TelephonyManager): String {
        return formatCarrier(manager.simCarrierId, manager.simCarrierIdName)
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    private fun formatSimSpecificCarrier(manager: TelephonyManager): String {
        return formatCarrier(manager.simSpecificCarrierId, manager.simSpecificCarrierIdName)
    }

    private fun formatMultiSimSupport(support: Int): String {
        return when (support) {
            TelephonyManager.MULTISIM_NOT_SUPPORTED_BY_HARDWARE -> "Not supported by hardware ($support)"
            TelephonyManager.MULTISIM_NOT_SUPPORTED_BY_CARRIER -> "Not supported by carrier ($support)"
            TelephonyManager.MULTISIM_ALLOWED -> "Allowed ($support)"
            else -> getString(R.string.unknown_int_value, support)
        }
    }

    @SuppressLint("newApi")
    private fun formatAccountCapabilities(capabilities: Int, outBuilder: StringBuilder? = null): String {
        val builder = outBuilder?: java.lang.StringBuilder()
        val startIdx = builder.length

        builder.append(String.format("Capabilities: 0x%08X", capabilities))
        var value = capabilities
        if (0 == value) {
            builder.append(getString(R.string.none))
        }

        fun Int.parseFlag(flag: Int, description: String): Int {
            return if (this.isFlagsSet(flag)) {
                builder.append("\n\t").append(description).append(String.format(" 0x%08X", flag))
                this and flag.inv()
            } else {
                this
            }
        }

        value = value.parseFlag(PhoneAccount.CAPABILITY_ADHOC_CONFERENCE_CALLING, "AdHoc conference calling")
        value = value.parseFlag(PhoneAccount.CAPABILITY_CALL_PROVIDER, "Call provider")
        value = value.parseFlag(PhoneAccount.CAPABILITY_CALL_SUBJECT, "Call subject")
        value = value.parseFlag(PhoneAccount.CAPABILITY_CONNECTION_MANAGER, "Connection manager")
        value = value.parseFlag(PhoneAccount.CAPABILITY_PLACE_EMERGENCY_CALLS, "Place emergency calls")
        value = value.parseFlag(PhoneAccount.CAPABILITY_RTT, "RTT(Real-time text)")
        value = value.parseFlag(PhoneAccount.CAPABILITY_SELF_MANAGED, "Self managed")
        value = value.parseFlag(PhoneAccount.CAPABILITY_SIM_SUBSCRIPTION, "SIM subscription")
        value = value.parseFlag(PhoneAccount.CAPABILITY_SUPPORTS_VIDEO_CALLING, "Supports video calling")
        value = value.parseFlag(PhoneAccount.CAPABILITY_VIDEO_CALLING, "Currently able to place video calls")
        value = value.parseFlag(PhoneAccount.CAPABILITY_VIDEO_CALLING_RELIES_ON_PRESENCE, "Video call capability relies on presence")

        if (value != 0) {
            builder.append("\n\tUnidentified capabilities: ").append(String.format("0x%08X", value))
        }

        return builder.substring(startIdx)
    }

    private fun formatPhoneAddressUri(address: Uri?): String {
        val str = address.toString()
        return if (str.contains('%')) {
            URLDecoder.decode(str, "utf-8")
        } else {
            str
        }
    }

    @SuppressLint("MissingPermission", "newApi")
    private fun formatPhoneAccount(handle: PhoneAccountHandle, manager: TelecomManager, outBuilder: StringBuilder? = null): String {
        val builder = outBuilder?: StringBuilder()
        val startIdx = builder.length
        val account = manager.getPhoneAccount(handle)
        builder.append("Label: ").append(account.label)
        builder.append("\nDescription: ").append(account.shortDescription)
        builder.append("\nAddress: ").append(formatPhoneAddressUri(account.address))

        account.subscriptionAddress?.also {
            builder.append("\nSubscription address: ").append(formatPhoneAddressUri(it))
        }

        if (!account.supportedUriSchemes.isNullOrEmpty()) {
            builder.append("\nSupported URI schemes")
            for (scheme in account.supportedUriSchemes) {
                builder.append("\n\t").append(scheme)
            }
        }
        builder.append('\n')
        formatAccountCapabilities(account.capabilities, builder)
        if (Build.VERSION_CODES.N <= Build.VERSION.SDK_INT) {
            if (!account.extras.isNullOrEmpty()) {
                builder.append("\nExtras")
                for (key in account.extras.keySet()) {
                    builder.append("\n\t$key: ${account.extras[key]}")
                }
            }
        }

        builder.append("\nLine1 Number: ").append(manager.getLine1Number(handle))
        builder.append("\nVoicemail Number: ").append(manager.getVoiceMailNumber(handle))
        builder.append("\n\n")
        return builder.substring(startIdx)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun formatPhoneAccounts(handles: List<PhoneAccountHandle>, manager: TelecomManager): String {
        if (handles.isNullOrEmpty()) {
            return getString(R.string.none)
        }
        val builder = StringBuilder()
        for (handle in handles) {
            formatPhoneAccount(handle, manager, builder)
        }
        return builder.trimEnd().toString()
    }

    private val phoneItemSpecs = listOf(
        InfoSpec(R.string.phone_phone_count, 23),
        InfoSpec(R.string.phone_phone_type),
        InfoSpec(R.string.phone_device_id),
        InfoSpec(R.string.phone_subscriber_id),
        InfoSpec(R.string.phone_device_software_version),
        InfoSpec(R.string.phone_line1_number),
        InfoSpec(R.string.phone_mms_ua, 19),
        InfoSpec(R.string.phone_mms_ua_prof_url, 19),
        InfoSpec(R.string.phone_network_country_iso),
        InfoSpec(R.string.phone_network_operator),
        InfoSpec(R.string.phone_network_operator_name),
        InfoSpec(R.string.phone_network_type),
        InfoSpec(R.string.phone_voice_network_type, 24),
        InfoSpec(R.string.phone_network_specifier, 26),
        InfoSpec(R.string.phone_supported_modem_count, 30),
        InfoSpec(R.string.phone_active_modem_count, 30),
        InfoSpec(R.string.phone_multi_sim_support, 29),
        InfoSpec(R.string.phone_sim_state),
        InfoSpec(R.string.phone_voice_mail_tag),
        InfoSpec(R.string.phone_voice_mail_number),
        InfoSpec(R.string.phone_visual_voice_mail_package, 26),
        InfoSpec(R.string.phone_forbidden_plmn, 26),
        InfoSpec(R.string.phone_default_euicc_id, 29),
        InfoSpec(R.string.phone_emergency_numbers, 29),
        InfoSpec(R.string.phone_features),
        InfoSpec(R.string.phone_sim_carrier, 28),
        InfoSpec(R.string.phone_sim_specific_carrier, 29),
        InfoSpec(R.string.phone_carrier_config, 26),
    )

    private val gsmItemSpecs = listOf(
        InfoSpec(R.string.phone_group_id_l1, 10),
    )

    private val cdmaItemSpecs = listOf(
        InfoSpec(R.string.phone_manufacturer_code, 29),
        InfoSpec(R.string.phone_type_allocation_code, 29),
    )

    private val simItemSpecs = listOf(
        InfoSpec(R.string.phone_sim_country_iso),
        InfoSpec(R.string.phone_sim_operator),
        InfoSpec(R.string.phone_sim_operator_name),
        InfoSpec(R.string.phone_sim_serial),
    )

    @SuppressLint("MissingPermission", "newApi")
    private fun getTelephonyItem(infoId: Int, manager: TelephonyManager): InfoItem? {
        val title = getString(infoId)
        val value = try {
            when (infoId) {
                R.string.phone_phone_count -> manager.phoneCount.toString()
                R.string.phone_phone_type -> formatPhoneType(manager.phoneType)
                R.string.phone_device_id -> manager.deviceId ?: getString(R.string.not_available)
                R.string.phone_subscriber_id -> manager.subscriberId ?: getString(R.string.not_available)
                R.string.phone_device_software_version -> manager.deviceSoftwareVersion ?: getString(R.string.not_available)
                R.string.phone_group_id_l1 -> manager.groupIdLevel1 ?: getString(R.string.not_available)
                R.string.phone_manufacturer_code -> manager.manufacturerCode ?: getString(R.string.not_available)
                R.string.phone_type_allocation_code -> manager.typeAllocationCode ?: getString(R.string.not_available)
                R.string.phone_line1_number -> manager.line1Number ?: getString(R.string.not_available)
                R.string.phone_mms_ua -> manager.mmsUserAgent
                R.string.phone_mms_ua_prof_url -> manager.mmsUAProfUrl
                R.string.phone_network_country_iso -> manager.networkCountryIso
                R.string.phone_network_operator -> manager.networkOperator
                R.string.phone_network_operator_name -> manager.networkOperatorName
                R.string.phone_network_type -> formatNetworkType(manager)
                R.string.phone_voice_network_type -> networkTypeName(manager.voiceNetworkType)
                R.string.phone_network_specifier -> manager.networkSpecifier ?: getString(R.string.not_available)
                R.string.phone_sim_carrier -> formatSimCarrier(manager)
                R.string.phone_sim_specific_carrier -> formatSimSpecificCarrier(manager)
                R.string.phone_carrier_config -> formatCarrierConfig(manager.carrierConfig)
                R.string.phone_supported_modem_count -> manager.supportedModemCount.toString()
                R.string.phone_active_modem_count -> manager.activeModemCount.toString()
                R.string.phone_multi_sim_support -> formatMultiSimSupport(manager.isMultiSimSupported)
                R.string.phone_sim_state -> formatSimState(manager.simState)
                R.string.phone_sim_country_iso -> manager.simCountryIso
                R.string.phone_sim_operator -> manager.simOperator
                R.string.phone_sim_operator_name -> manager.simOperatorName
                R.string.phone_sim_serial -> manager.simSerialNumber
                R.string.phone_voice_mail_tag -> manager.voiceMailAlphaTag
                R.string.phone_voice_mail_number -> manager.voiceMailNumber ?: getString(R.string.not_available)
                R.string.phone_visual_voice_mail_package -> manager.visualVoicemailPackageName ?: getString(R.string.not_available)
                R.string.phone_forbidden_plmn -> manager.forbiddenPlmns?.let { formatStringArray(*it) } ?: getString(R.string.none)
                R.string.phone_default_euicc_id -> formatEuiccId(manager.cardIdForDefaultEuicc)
                R.string.phone_emergency_numbers -> formatEmergencyNumbers(manager.emergencyNumberList)
                R.string.phone_features -> getFeatures(manager)
                else -> getString(R.string.invalid_item)
            }
        } catch (e: Error) {
            FirebaseHelper.onThrowableCaught(e)
            e.printStackTrace()
            Log.e(TAG, "$title\n" + e.toString())
            getString(R.string.unsupported)
        } catch (se: SecurityException) {
            FirebaseHelper.onThrowableCaught(se)
            se.printStackTrace()
            Log.e(TAG, "$title\n" + se.toString())
            getString(R.string.not_available)
        } catch (e: Throwable) {
            e.printStackTrace()
            e.message.toString()
        }
        return InfoItem(title, value)
    }

    private val telecomItemSpecs = listOf(
        InfoSpec(R.string.phone_call_capable_accounts, 23),
        InfoSpec(R.string.phone_self_managed_accounts, 26),
    )
    private val telecomIds = telecomItemSpecs.map { it.titleId }.toSet()

    @SuppressLint("newApi")
    private fun getTelecomItem(infoId: Int, manager: TelecomManager): InfoItem? {
        val title = getString(infoId)
        val value = try {
            when (infoId) {
                R.string.phone_call_capable_accounts -> formatPhoneAccounts(manager.callCapablePhoneAccounts, manager)
                R.string.phone_self_managed_accounts -> formatPhoneAccounts(manager.selfManagedPhoneAccounts, manager)
                else -> getString(R.string.invalid_item)
            }
        } catch (e: Error) {
            FirebaseHelper.onThrowableCaught(e)
            e.printStackTrace()
            Log.e(TAG, "$title\n" + e.toString())
            getString(R.string.unsupported)
        } catch (se: SecurityException) {
            FirebaseHelper.onThrowableCaught(se)
            se.printStackTrace()
            Log.e(TAG, "$title\n" + se.toString())
            getString(R.string.not_available)
        } catch (e: Throwable) {
            e.printStackTrace()
            e.message.toString()
        }
        return InfoItem(title, value)
    }

    @SuppressLint("newApi")
    override fun getItem(infoId: Int, vararg args: Any?): InfoItem? {
        return if (telecomIds.contains(infoId)) {
            (args[0] as? TelecomManager)?.let { manager ->
                getTelecomItem(infoId, manager)
            }
        } else (args[0] as? TelephonyManager)?.let { manager ->
            getTelephonyItem(infoId, manager)
        }
    }

    private val telephoneContent = mutableListOf<InfoItem>()

    override suspend fun getItems(): List<InfoItem> {
        if (telephoneContent.isEmpty()) {
            val telephonyManager = app.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            if (telephonyManager.phoneType == TelephonyManager.PHONE_TYPE_NONE) {
                telephoneContent.add(InfoItem(R.string.phone_phone_type, R.string.phone_none))
            } else {
                telephoneContent.addItems(phoneItemSpecs, telephonyManager)
                if (telephonyManager.phoneType == TelephonyManager.PHONE_TYPE_GSM) {
                    telephoneContent.addItems(gsmItemSpecs, telephonyManager)
                }
                if (telephonyManager.phoneType == TelephonyManager.PHONE_TYPE_CDMA) {
                    telephoneContent.addItems(cdmaItemSpecs, telephonyManager)
                }
                if (telephonyManager.simState == TelephonyManager.SIM_STATE_READY) {
                    telephoneContent.addItems(simItemSpecs, telephonyManager)
                }
                if (Build.VERSION_CODES.LOLLIPOP <= Build.VERSION.SDK_INT) {
                    val telecomManager = app.getSystemService(Context.TELECOM_SERVICE) as TelecomManager
                    telephoneContent.addItems(telecomItemSpecs, telecomManager)
                }
            }
        }
        return telephoneContent
    }
}
