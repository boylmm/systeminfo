package com.oeu.a.sysinfo.content

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.hardware.ConsumerIrManager
import android.os.Build
import androidx.annotation.RequiresApi
import com.oeu.a.sysinfo.R
import com.oeu.a.sysinfo.utils.FirebaseHelper

class InfraredInfoProvider: InfoProvider() {
    companion object {
        private val TAG = InfraredInfoProvider::class.java.simpleName
    }

    private val infraredContent = mutableListOf<InfoItem>()

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override val requiredPermissions = arrayOf(
        Manifest.permission.TRANSMIT_IR
    )

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    private fun formatCarrierFrequencyRanges(ranges: Array<ConsumerIrManager.CarrierFrequencyRange>?, outBuilder: StringBuilder? = null): String {
        val builder = outBuilder ?: StringBuilder()
        val startIdx = builder.length
        builder.append("Carrier frequency ranges:\n")
        if (ranges.isNullOrEmpty()) {
            builder.append(getString(R.string.none))
        } else {
            for (range in ranges) {
                builder.append(String.format("%,d Hz ~ %,d Hz\n", range.minFrequency, range.maxFrequency))
            }
        }
        return builder.trimEnd().substring(startIdx)
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    private fun formatInfraredFeature(manager: ConsumerIrManager): String {
        val builder = StringBuilder()
        if (!manager.hasIrEmitter()) {
            builder.append("* no IR emitter\n")
        }
        formatCarrierFrequencyRanges(manager.carrierFrequencies, builder)
        return builder.toString()
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    private fun getInfraredItems(): List<InfoItem> {
        val items = mutableListOf<InfoItem>()
        val irm = app.getSystemService(Context.CONSUMER_IR_SERVICE) as ConsumerIrManager
        //  Currently only one attribute is available from ConsumerIrManager
        try {
            items.add(InfoItem(R.string.infrared_carrier_frequency_ranges, formatInfraredFeature(irm)))
        } catch (t: Throwable) {
            FirebaseHelper.onThrowableCaught(t)
            t.printStackTrace()
            items.add(InfoItem(R.string.infrared_carrier_frequency_ranges, R.string.unsupported))
        }
        return items
    }

    override suspend fun getItems(): List<InfoItem> {
        if (infraredContent.isEmpty()) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                infraredContent.add(InfoItem(R.string.item_infrared, getString(R.string.sdk_version_required, Build.VERSION_CODES.KITKAT)))
            } else if (app.packageManager.hasSystemFeature(PackageManager.FEATURE_CONSUMER_IR)) {
                infraredContent.addAll(getInfraredItems())
            } else {
                infraredContent.add(InfoItem(R.string.item_infrared, R.string.infrared_no_feature))
            }
        }
        return infraredContent
    }
}
