package com.oeu.a.sysinfo.content

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.location.Criteria
import android.location.GnssCapabilities
import android.location.LocationManager
import android.location.LocationProvider
import android.os.Build
import com.oeu.a.sysinfo.R

class LocationInfoProvider: InfoProvider() {
    companion object {
        private val TAG = LocationInfoProvider::class.java.simpleName
    }

    override val requiredPermissions = arrayOf(
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION
    )

    private fun formatLocationAccuracy(acc: Int): String {
        val value: String = if (Build.VERSION.SDK_INT < Build.VERSION_CODES.GINGERBREAD) {
            when (acc) {
                Criteria.ACCURACY_COARSE -> "Coarse"
                Criteria.ACCURACY_FINE -> "Fine"
                else -> getString(R.string.unknown)
            }
        } else {
            when (acc) {
                Criteria.ACCURACY_LOW -> "Low"
                Criteria.ACCURACY_MEDIUM -> "Medium"
                Criteria.ACCURACY_HIGH -> "high"
                else -> getString(R.string.unknown)
            }
        }
        return "$value ($acc)"
    }

    private fun formatLocationPowerReq(req: Int): String {
        val value = when (req) {
            Criteria.NO_REQUIREMENT -> "No requirement"
            Criteria.POWER_LOW -> "Low"
            Criteria.POWER_MEDIUM -> "Medium"
            Criteria.POWER_HIGH -> "High"
            else -> getString(R.string.unknown)
        }
        return "$value ($req)"
    }

    private fun formatGnssYear(year: Int): String = if (year == 0) "(Before 2016)" else "($year)"

    private fun formatGnssCapabilities(capabilities: GnssCapabilities): String {
        val builder = StringBuilder()
        if (capabilities.hasGnssAntennaInfo()) {
            builder.append(getString(R.string.location_gnss_capability_antenna_info)).append('\n')
        }
        if (builder.isEmpty()) {
            builder.append(getString(R.string.no_capability))
        }
        return builder.toString()
    }

    private fun getLocationProviderItem(lp: LocationProvider, enabled: Boolean): InfoItem {
        val name = lp.name
        val sb = StringBuilder()
        if (!enabled) {
            sb.append(getString(R.string.disabled)).append('\n')
        }
        sb.append(getString(R.string.location_accuracy)).append(formatLocationAccuracy(lp.accuracy))
        sb.append('\n').append(getString(R.string.location_power_req)).append(formatLocationPowerReq(lp.powerRequirement))
        sb.append('\n').append(getString(R.string.location_cost))
            .append(getString(if (lp.hasMonetaryCost()) R.string.location_may_charge else R.string.location_free))
        val rCel = lp.requiresCell()
        val rNet = lp.requiresNetwork()
        val rSat = lp.requiresSatellite()
        if (rCel or rNet or rSat) {
            sb.append("\n(").append(getString(R.string.location_requires))
            if (rCel) {
                sb.append(getString(R.string.location_req_cell))
            }
            if (rNet) {
                sb.append(getString(R.string.location_req_network))
            }
            if (rSat) {
                sb.append(getString(R.string.location_req_satellite))
            }
            sb.append(')')
        }
        val sAlt = lp.supportsAltitude()
        val sBng = lp.supportsBearing()
        val sSpd = lp.supportsSpeed()
        if (sAlt or sBng or sSpd) {
            sb.append("\nAdditional informations")
            if (sAlt) {
                sb.append("\n\taltitude")
            }
            if (sBng) {
                sb.append("\n\tbearing")
            }
            if (sSpd) {
                sb.append("\n\tspeed")
            }
        }
        return InfoItem(name, sb.toString())
    }

    private val itemSpecs = listOf(
        InfoSpec(R.string.location_gnss, 28),
        InfoSpec(R.string.location_gnss_capabilities, 30),
    )

    @SuppressLint("NewApi")
    override fun getItem(infoId: Int, vararg args: Any?): InfoItem? {
        val name = getString(infoId)
        (args[0] as? LocationManager)?.also { manager ->
            val value = when (infoId) {
                R.string.location_gnss -> manager.gnssHardwareModelName?.let {
                        it + '\t' + formatGnssYear(manager.gnssYearOfHardware)
                    } ?: run {
                        getString(R.string.location_no_gnss_hardware_name) + '\t' + formatGnssYear(manager.gnssYearOfHardware)
                    }
                R.string.location_gnss_capabilities -> formatGnssCapabilities(manager.gnssCapabilities)
                else -> getString(R.string.invalid_item)
            }
            return InfoItem(name, value)
        }
        return null
    }

    private val locationContent: MutableList<InfoItem> = mutableListOf()

    override suspend fun getItems(): List<InfoItem> {
        if (locationContent.isEmpty()) {
            val manager = app.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            for (spec in itemSpecs) {
                getItem(spec, manager)?.also {
                    locationContent.add(it)
                }
            }
            val names = manager.allProviders
            if (names.isEmpty()) {
                locationContent.add(InfoItem(R.string.item_location, R.string.location_none))
            } else {
                for (name in names) {
                    manager.getProvider(name)?.also {
                        locationContent.add(getLocationProviderItem(it, manager.isProviderEnabled(name)))
                    } ?: run {
                        locationContent.add(InfoItem(name, R.string.invalid_item))
                    }
                }
            }
        }
        return locationContent
    }
}
