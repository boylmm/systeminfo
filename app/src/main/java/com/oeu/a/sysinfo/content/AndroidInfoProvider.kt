package com.oeu.a.sysinfo.content

import android.annotation.SuppressLint
import android.os.Build
import android.provider.Settings
import android.util.Log
import com.oeu.a.sysinfo.R
import com.oeu.a.sysinfo.utils.FirebaseHelper

class AndroidInfoProvider: InfoProvider() {
    companion object {
        private val TAG = AndroidInfoProvider::class.java.simpleName
        private val SDK_NAMES: Array<String> = arrayOf(
            "* Unknown",                //  0
            "Base",                     //  1
            "Base_1_1",                 //  2
            "Cupcake",                  //  3
            "Donut",                    //  4
            "Eclair",                   //  5
            "Eclair_0_1",               //  6
            "Eclair MR1",               //  7
            "Froyo",                    //  8
            "Gingerbread",              //  9
            "Gingerbread MR1",          // 10
            "Honeycomb",                // 11
            "Honeycomb_MR1",            // 12
            "Honeycomb_MR2",            // 13
            "Ice Cream Sandwich",       // 14
            "Ice Cream Sandwich MR1",   // 15 Deprecated up to this
            "Jelly Bean",               // 16
            "Jelly Bean MR1",           // 17
            "Jelly Bean MR2",           // 18
            "KitKat",                   // 19
            "KitKat for Wearables",     // 20
            "Lollipop 5.0",             // 21
            "Lollipop MR1 5.1",         // 22
            "Marshmallow 6.0",          // 23
            "Nougat 7.0",               // 24
            "Nougat MR1 7.1",           // 25
            "Oreo 8.0",                 // 26
            "Oreo MR1 8.1",             // 27
            "Pie 9.0",                  // 28
            "Android 10",               // 29
            "Android 11"                // 30
        )
        fun getSdkName(sdkInt: Int): String {
            return if (sdkInt < 0 || SDK_NAMES.size <= sdkInt) SDK_NAMES[0]
            else SDK_NAMES[sdkInt]
        }
        fun formatSdk(sdkInt: Int): String {
            return "$sdkInt (${getSdkName(sdkInt)})"
        }
    }

    private fun formatPreviewSdk(sdkInt: Int): String {
        val previewName = if (0 == sdkInt) getString(R.string.android_tags) else getSdkName(sdkInt)
        return "$sdkInt ($previewName)"
    }

    private fun StringBuilder.append(idx: Int, partition: Build.Partition): StringBuilder {
        append("Partition $idx: ").append(partition.name).append("\n\t")
            .append("build time: ").append(formatTime(partition.buildTimeMillis)).append("\n\t")
            .append("fingerprint: ").append(partition.fingerprint)
        return this
    }

    private fun formatPartitions(partitions: List<Build.Partition>): String {
        Log.d(TAG, "partitions: ${partitions.size}")
        if (partitions.isEmpty())
            getString(R.string.none)
        val builder = StringBuilder()

        builder.append(0, partitions[0])
        for (idx in 1 until partitions.size) {
            builder.append("\n\n").append(idx, partitions[idx])
        }
        return builder.toString()
    }

    private val itemSpecs = listOf(
        InfoSpec(R.string.android_release),
        InfoSpec(R.string.android_sdk, 4),
        InfoSpec(R.string.android_manufacturer, 4),
        InfoSpec(R.string.android_brand),
        InfoSpec(R.string.android_model),
        InfoSpec(R.string.android_device),
        InfoSpec(R.string.android_product),
        InfoSpec(R.string.android_hardware, 8),
        InfoSpec(R.string.android_time),
        InfoSpec(R.string.android_tags),
        InfoSpec(R.string.android_type),
        InfoSpec(R.string.android_codename, 4),
        InfoSpec(R.string.android_display, 3),
        InfoSpec(R.string.android_id),
        InfoSpec(R.string.android_fingerprint),
        InfoSpec(R.string.android_serial, 9),
        InfoSpec(R.string.android_board),
        InfoSpec(R.string.android_bootloader, 8),
        InfoSpec(R.string.android_radio, 8),
        InfoSpec(R.string.android_incremental),
        InfoSpec(R.string.android_supported_abis, 4),
        InfoSpec(R.string.android_supported_32bit_abis, 21),
        InfoSpec(R.string.android_supported_64bit_abis, 21),
        InfoSpec(R.string.android_host),
        InfoSpec(R.string.android_secure_id, 3),
        InfoSpec(R.string.android_base_os, 23),
        InfoSpec(R.string.android_preview_sdk, 23),
        InfoSpec(R.string.android_partitions, 29)
    )

    @SuppressLint("NewApi")
    override fun getItem(infoId: Int, vararg args: Any?): InfoItem? {
        val title = getString(infoId)
        val value: String = try {
            when (infoId) {
                R.string.android_release-> Build.VERSION.RELEASE
                R.string.android_sdk-> formatSdk(Build.VERSION.SDK_INT)
                R.string.android_time-> formatTime(Build.TIME)
                R.string.android_tags-> Build.TAGS
                R.string.android_type-> Build.TYPE
                R.string.android_codename-> Build.VERSION.CODENAME
                R.string.android_display-> Build.DISPLAY
                R.string.android_fingerprint-> Build.FINGERPRINT
                R.string.android_manufacturer-> Build.MANUFACTURER
                R.string.android_brand-> Build.BRAND
                R.string.android_product-> Build.PRODUCT
                R.string.android_device-> Build.DEVICE
                R.string.android_model-> Build.MODEL
                R.string.android_hardware-> Build.HARDWARE
                R.string.android_serial-> if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
                    Build.SERIAL
                } else {
                    getString(R.string.privileged_permission_required)
                }
                R.string.android_board-> Build.BOARD
                R.string.android_bootloader-> Build.BOOTLOADER
                R.string.android_radio-> if (Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                        Build.RADIO
                    } else {
                        Build.getRadioVersion()
                    }
                R.string.android_incremental-> Build.VERSION.INCREMENTAL
                R.string.android_host-> Build.HOST
                R.string.android_id-> Build.ID
                R.string.android_secure_id-> Settings.Secure.getString(app.contentResolver, Settings.Secure.ANDROID_ID)
                R.string.android_supported_abis-> if (Build.VERSION.SDK_INT < Build.VERSION_CODES.FROYO) {
                        Build.CPU_ABI
                    } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                        formatStringArray(Build.CPU_ABI, Build.CPU_ABI2)
                    } else {
                        formatStringArray(*Build.SUPPORTED_ABIS)
                    }
                R.string.android_supported_32bit_abis-> formatStringArray(*Build.SUPPORTED_32_BIT_ABIS)
                R.string.android_supported_64bit_abis-> formatStringArray(*Build.SUPPORTED_64_BIT_ABIS)
                R.string.android_base_os-> Build.VERSION.BASE_OS
                R.string.android_preview_sdk-> formatPreviewSdk(Build.VERSION.PREVIEW_SDK_INT)
                R.string.android_partitions-> formatPartitions(Build.getFingerprintedPartitions())
                else-> getString(R.string.invalid_item)
            }
        } catch (e: Error) {
            FirebaseHelper.onThrowableCaught(e)
            e.printStackTrace()
            getString(R.string.unsupported)
        }
        return InfoItem(title, value)
    }

    private val androidContent: MutableList<InfoItem> = mutableListOf()
    override suspend fun getItems(): List<InfoItem> {
        if (androidContent.isEmpty()) {
            androidContent.addItems(itemSpecs)
        }
        return androidContent
    }
}
