package com.oeu.a.sysinfo

import androidx.multidex.MultiDexApplication
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.oeu.a.sysinfo.content.InfoProvider
import com.oeu.a.sysinfo.utils.FirebaseHelper

class SystemInfoApplication: MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()
        InfoProvider.app = this
        FirebaseHelper.firebaseAnalytics = Firebase.analytics
    }
}
