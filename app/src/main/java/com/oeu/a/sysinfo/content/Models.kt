package com.oeu.a.sysinfo.content

import android.view.View
import org.json.JSONArray
import org.json.JSONObject

interface SharableItem

interface LongClickHandler {
    fun handleLongClick(view: View, item: SharableItem): Boolean
}

data class ProviderItem(val nameId: Int, private val providerImpl: Lazy<InfoProvider>, var selected: Boolean = false): SharableItem {
    val provider: InfoProvider get() = providerImpl.value

    fun getShareTextAsync(onCompletion: (String)-> Unit) {
        provider.getItemsAsync { infoItems ->
            val builder = StringBuilder()
            builder.append(InfoProvider.app.getString(nameId)).append('\n')
            for (item in InfoProvider.shareInfoItems) {
                builder.append(item.toText("\t"))
            }
            for (item in infoItems) {
                builder.append(item.toText("\t"))
            }
            onCompletion(builder.toString())
        }
    }

    fun getShareJsonAsync(onCompletion: (JSONObject)-> Unit) {
        provider.getItemsAsync { infoItems ->
            val contents = JSONArray()
            for (item in InfoProvider.shareInfoItems) {
                contents.put(item.toShareJson())
            }
            for (item in infoItems) {
                contents.put(item.toShareJson())
            }
            val result = JSONObject()
            result.put("category", InfoProvider.app.getString(nameId))
            result.put("contents", contents)
            onCompletion(result)
        }
    }
}

data class InfoItem(val name: String, val value: String): SharableItem {
    constructor(nameId: Int, valueId: Int): this(InfoProvider.app.getString(nameId), InfoProvider.app.getString(valueId))
    constructor(name: String, valueId: Int): this(name, InfoProvider.app.getString(valueId))
    constructor(nameId: Int, value: String): this(InfoProvider.app.getString(nameId), value)

    fun toText(prefix: String, indent: String = "\t"): String {
        val valuePrefix = prefix + indent
        val builder = StringBuilder()
        builder.append(prefix).append(name).append('\n')
        for (line in value.split('\n')) {
            builder.append(valuePrefix).append(line).append('\n')
        }
        return builder.toString()
    }

    fun toShareText(): String {
        return toText("")
    }

    fun toShareJson(): JSONObject {
        return JSONObject().put("name", name).put("value", value)
    }
}
