package com.oeu.a.sysinfo.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.oeu.a.sysinfo.R
import com.oeu.a.sysinfo.content.LongClickHandler
import com.oeu.a.sysinfo.content.SharableItem
import com.oeu.a.sysinfo.content.providerList

class InfoContentFragment: Fragment(), LongClickHandler {
    companion object {
        private val TAG = InfoContentFragment::class.java.simpleName
        const val KEY_CONTENT_IDX: String = "infoContent.contentIdx"

        fun newInstance(contentIdx: Int): InfoContentFragment{
            return InfoContentFragment().apply {
                arguments = Bundle().apply {
                    putInt(KEY_CONTENT_IDX, contentIdx)
                }
            }
        }
    }

//    override fun onAttach(context: Context) {
//        Log.i(TAG, "onAttach()")
//        super.onAttach(context)
//    }

//    override fun onCreate(savedInstanceState: Bundle?) {
//        Log.i(TAG, "onCreate()")
//        super.onCreate(savedInstanceState)
//    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
//        Log.i(TAG, "onCreateView()")
//        arguments?.also {
//            for (key in it.keySet()) {
//                Log.i(TAG, "ARG: $key: ${it.get(key)}")
//            }
//        }
        val view = inflater.inflate(R.layout.fragment_content, container, false)
        val contentAdapter = ContentAdapter(emptyList(), this)
        view.findViewById<RecyclerView>(R.id.content_list)?.apply {
            adapter = contentAdapter
            layoutManager = LinearLayoutManager(inflater.context).apply {
                orientation = LinearLayoutManager.VERTICAL
            }
        }
        val contentIdx = arguments?.getInt(KEY_CONTENT_IDX) ?: 0
        providerList[contentIdx].provider.getItemsAsync { newItems ->
            contentAdapter.apply {
                items = newItems
                notifyDataSetChanged()
            }
        }
        return view
    }

    override fun handleLongClick(view: View, item: SharableItem): Boolean {
        return (activity as? LongClickHandler)?.handleLongClick(view, item) ?: false
    }
}
