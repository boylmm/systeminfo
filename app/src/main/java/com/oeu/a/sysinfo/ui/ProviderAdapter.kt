package com.oeu.a.sysinfo.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.oeu.a.sysinfo.R
import com.oeu.a.sysinfo.content.LongClickHandler
import com.oeu.a.sysinfo.content.ProviderItem

class ProviderAdapter(var providers: List<ProviderItem>, var providerHandler: ProviderItemHandler, var longClickHandler: LongClickHandler? = null)
    : RecyclerView.Adapter<ProviderAdapter.ProviderItemViewHolder>() {
    companion object {
        private val TAG = ProviderAdapter::class.java.simpleName
    }

    interface ProviderItemHandler {
        fun onItemSelected(selectedIdx: Int)
    }

    inner class ProviderItemViewHolder(view: View): RecyclerView.ViewHolder(view) {
        init {
            view.setOnClickListener {
                view.isSelected = true
                providerHandler.onItemSelected(bindingAdapterPosition)
            }
            view.setOnLongClickListener {
                longClickHandler?.handleLongClick(it, providers[bindingAdapterPosition]) ?: false
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProviderItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.provider_item, parent, false)
        return ProviderItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ProviderItemViewHolder, position: Int) {
        holder.itemView.findViewById<TextView>(R.id.provider_name)?.apply {
            text = context.getString(providers[position].nameId)
            isSelected = providers[position].selected
        }
    }

    override fun getItemCount(): Int = providers.size
}