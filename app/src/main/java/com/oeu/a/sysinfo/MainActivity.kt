package com.oeu.a.sysinfo

import android.content.pm.PackageManager
import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.PopupMenu
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.oeu.a.sysinfo.content.*
import com.oeu.a.sysinfo.ui.InfoContentFragment
import com.oeu.a.sysinfo.ui.ProviderAdapter
import com.oeu.a.sysinfo.utils.FirebaseHelper
import com.oeu.a.sysinfo.utils.className

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import androidx.core.content.FileProvider
import org.json.JSONObject
import java.io.File
import android.content.Intent
import android.os.Build

import androidx.core.app.ShareCompat
import java.io.IOException


class MainActivity: AppCompatActivity(), ProviderAdapter.ProviderItemHandler, LongClickHandler {
    companion object {
        private val TAG = MainActivity::class.java.simpleName
        private const val TAG_CONTENT: String = "Content"
        private const val KEY_LAST_SELECTED_IDX: String = "main.lastSelectedIdx"
    }

    private val separateFragment: Boolean get() {
        return resources.configuration.smallestScreenWidthDp < 480 &&
                resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT
    }

    private val itemListView: RecyclerView? get() = findViewById(R.id.provider_list)
    override fun onCreate(savedInstanceState: Bundle?) {
//        Log.i(TAG, "onCreate() ${savedInstanceState?.keySet() ?: "(no saved)"}")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        itemListView?.apply {
            layoutManager = LinearLayoutManager(this@MainActivity).apply {
                orientation = LinearLayoutManager.VERTICAL
            }
            adapter = ProviderAdapter(providerList, this@MainActivity, this@MainActivity)
        }
        supportActionBar?.apply {
            setHomeAsUpIndicator(R.drawable.back_icon)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
//        Log.i(TAG, "onSaveInstanceState()")
        super.onSaveInstanceState(outState)
        if (0 <= lastSelectedIdx && lastSelectedIdx < providerList.size) {
            outState.putInt(KEY_LAST_SELECTED_IDX, lastSelectedIdx)
        }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
//        Log.i(TAG, "onRestoreInstanceState()")
        super.onRestoreInstanceState(savedInstanceState)
        val selectedIdx = savedInstanceState.getInt(KEY_LAST_SELECTED_IDX, -1)
        if (0 <= selectedIdx && selectedIdx < providerList.size) {
            selectItem(selectedIdx)
            itemListView?.scrollToPosition(selectedIdx)
        }
    }

//    override fun onStart() {
//        Log.i(TAG, "onStart()")
//        super.onStart()
//    }

    override fun onResume() {
//        Log.i(TAG, "onResume()")
        super.onResume()
        if (separateFragment) {
            supportFragmentManager.findFragmentByTag(TAG_CONTENT)?.also {
                it.arguments?.getInt(InfoContentFragment.KEY_CONTENT_IDX)?.also { idx ->
                    supportActionBar?.apply {
                        title = getString(providerList[idx].nameId)
                        setDisplayHomeAsUpEnabled(true)
                    }
                    itemListView?.scrollToPosition(idx)
                }
            } ?: run {
                //  Somehow, selected value remains in memory after onDestroy()
                for (provider in providerList) provider.selected = false
            }
        } else if (null == supportFragmentManager.findFragmentByTag(TAG_CONTENT)) {
            var selected = if (lastSelectedIdx < 0) 0 else lastSelectedIdx
            //  Somehow, selected value remains in memory after onDestroy()
            for (idx in providerList.indices) {
                if (providerList[idx].selected) {
                    selected = idx
                    break
                }
            }
            itemListView?.scrollToPosition(selected)
            handleSelectedItem(selected)
        }
    }

//    override fun onPause() {
//        Log.i(TAG, "onPause() finising: $isFinishing")
//        super.onPause()
//    }
//
//    override fun onStop() {
//        Log.i(TAG, "onStop() finising: $isFinishing")
//        super.onStop()
//    }

    private var glHelper: OpenGlInfoProvider.GlHelper? = null

    override fun onDestroy() {
//        Log.i(TAG, "onDestroy()")
        glHelper?.destroy()
        super.onDestroy()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home-> {
                onBackPressed()
                true
            }
            else-> super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
//        Log.i(TAG, "onBackPressed()")
        if (separateFragment) {
            supportFragmentManager.findFragmentByTag(TAG_CONTENT)?.also {
                supportFragmentManager.beginTransaction().remove(it).commit()
                supportActionBar?.apply {
                    title = getString(R.string.app_name)
                    setDisplayHomeAsUpEnabled(false)
                }
                return
            }
        }
        super.onBackPressed()
    }

    private var lastSelectedIdx: Int = -1

    private fun selectItem(selectedIdx: Int) {
        if (lastSelectedIdx == selectedIdx) {
            return
        }
        providerList[selectedIdx].selected = true
//        val adapter = itemListView?.adapter
//        adapter?.notifyItemChanged(selectedIdx)
        if (0 <= lastSelectedIdx && lastSelectedIdx < providerList.size) {
            providerList[lastSelectedIdx].selected = false
//            adapter?.notifyItemChanged(lastSelectedIdx)
        }
        itemListView?.adapter?.notifyDataSetChanged()
        lastSelectedIdx = selectedIdx
    }

    private fun showContentFragment(contentIdx: Int) {
        val ft = supportFragmentManager.beginTransaction()
            .replace(R.id.content_frame, InfoContentFragment.newInstance(contentIdx), TAG_CONTENT)

        if (separateFragment) {
            supportActionBar?.apply {
                title = getString(providerList[contentIdx].nameId)
                setDisplayHomeAsUpEnabled(true)
            }
        }
        ft.commit()
    }

    private var afterCheckPermission: ((Int)->Unit)? = null

    private fun checkPermissionFor(providerIdx: Int, onCompletion: ((Int)->Unit)? = null) {
        val provider = providerList[providerIdx].provider
        val requiredPermissions = provider.requiredPermissions
        var granted = true
        for (permission in requiredPermissions) {
            if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                granted = false
                break
            }
        }
        (provider as? OpenGlInfoProvider)?.also {
            glHelper = glHelper ?: it.getGlHelper().apply { prepare() }
        }

        if (granted) {
            onCompletion?.invoke(providerIdx)
        }
        else {
            afterCheckPermission = onCompletion
            ActivityCompat.requestPermissions(this, requiredPermissions, providerIdx)
        }
    }

    private fun showSelectedItem(selectedIdx: Int) {
        checkPermissionFor(selectedIdx, this::showContentFragment)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        afterCheckPermission?.invoke(requestCode)
        afterCheckPermission = null
    }

    private fun handleSelectedItem(selectedIdx: Int) {
        selectItem(selectedIdx)
        showSelectedItem(selectedIdx)
    }

    override fun onItemSelected(selectedIdx: Int) {
        FirebaseHelper.onProviderSelected(getString(providerList[selectedIdx].nameId))
        handleSelectedItem(selectedIdx)
    }

    private fun copyToClipboard(text: String) {
        Log.i(TAG, "Copy to clipboard\n$text")
        val clipboard: ClipboardManager = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText(getString(R.string.app_name), text)
        clipboard.setPrimaryClip(clip)
    }

    private fun copyToClipboard(providerIdx: Int) {
        providerList[providerIdx].getShareTextAsync { copyToClipboard(it) }
    }

    private fun copyToClipboard(item: SharableItem): Boolean {
        return when (item) {
            is InfoItem -> {
                copyToClipboard(item.toShareText())
                true
            }
            is ProviderItem -> {
                val providerIdx = providerList.indexOf(item)
                if (0 <= providerIdx) {
                    checkPermissionFor(providerIdx, this::copyToClipboard)
                    true
                }
                else {
                    false
                }
            }
            else -> {
                Log.w(TAG, "Unexpected copyToClipboard: ${item.className()}")
                false
            }
        }
    }

    private fun shareTmpFile(text: String, fileExt: String, mimeType: String) {
        val tmpPath = File(cacheDir, "tmp")
        val tmpFile: File
        try {
            tmpPath.mkdirs()
            tmpFile = File.createTempFile("SysInfo_share", fileExt, tmpPath)
//            Log.i(TAG, "shareFile: ${tmpFile.name}\n$text")
            tmpFile.deleteOnExit()
            tmpFile.writeText(text)
        } catch (ioe: IOException) {
            FirebaseHelper.onThrowableCaught(ioe)
            ioe.printStackTrace()
            Log.w(TAG, "Failed to share")
            return
        }

        val uri = FileProvider.getUriForFile(this, application.packageName + ".fileprovider", tmpFile)
        val shareIntent = ShareCompat.IntentBuilder(this)
            .setStream(uri)
            .setType(mimeType)
            .intent

        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1) {
            intent.clipData = ClipData.newRawUri("", uri)
        }
        startActivity(Intent.createChooser(shareIntent, "SysInfo: share"))
    }

    private fun shareText(text: String) {
        shareTmpFile(text, ".txt", "text/plain")
    }

    private fun shareAsText(providerIdx: Int) {
        providerList[providerIdx].getShareTextAsync { shareText(it) }
    }

    private fun shareText(item: SharableItem): Boolean {
        return when (item) {
            is InfoItem -> {
                shareText(item.toShareText())
                true
            }
            is ProviderItem -> {
                val providerIdx = providerList.indexOf(item)
                if (0 <= providerIdx) {
                    checkPermissionFor(providerIdx, this::shareAsText)
                    true
                }
                else {
                    false
                }
            }
            else -> {
                Log.i(TAG, "Unexpected shareAsText: ${item.className()}")
                false
            }
        }
    }

    private fun shareJson(obj: JSONObject) {
        shareTmpFile(obj.toString(), ".json", "text/json")
    }

    private fun shareAsJson(providerIdx: Int) {
        providerList[providerIdx].getShareJsonAsync { shareJson(it) }
    }

    private fun shareJson(item: SharableItem): Boolean {
        return when (item) {
            is InfoItem -> {
                shareJson(item.toShareJson())
                true
            }
            is ProviderItem -> {
                val providerIdx = providerList.indexOf(item)
                if (0 <= providerIdx) {
                    checkPermissionFor(providerIdx, this::shareAsJson)
                    true
                }
                else {
                    false
                }
            }
            else -> {
                Log.i(TAG, "Unexpected shareAsJson: ${item.className()}")
                false
            }
        }
    }

    override fun handleLongClick(view: View, item: SharableItem): Boolean {
        val menu = PopupMenu(this@MainActivity, view)
        menuInflater.inflate(R.menu.share_popup_menu, menu.menu)
        menu.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.share_copy_to_clipboard -> copyToClipboard(item)
                R.id.share_as_text -> shareText(item)
                R.id.share_as_json -> shareJson(item)
                else -> false
            }
        }
        menu.show()
        return true
    }
}
