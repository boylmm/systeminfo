package com.oeu.a.sysinfo.content

import com.oeu.a.sysinfo.R
import com.oeu.a.sysinfo.utils.FirebaseHelper
import java.security.Provider
import java.security.Security

class SecurityInfoProvider: InfoProvider() {
    companion object {
        private val TAG = SecurityInfoProvider::class.java.simpleName
    }

    private val serviceTypes = mutableSetOf<String>()

    private fun getSecurityContent(provider: Provider): List<InfoItem> {
        val list = mutableListOf<InfoItem>()
        val name = "${provider.name}: ${provider.info}"
        val builder = StringBuilder()
        builder.append("version: ${provider.version}\n")

        val services = provider.services
        if (services.isEmpty()) {
            builder.append(getString(R.string.security_no_service))
        } else {
            var prevPkg: Package? = null
            for (service in services) {
                var pkg: Package? = null
                try {
                    pkg = Class.forName(service.className).getPackage()
                } catch (e: ClassNotFoundException) {
                    FirebaseHelper.onThrowableCaught(e)
                    e.printStackTrace()
                }
                if (null != pkg && pkg != prevPkg) {
                    builder.append("Package ").append(pkg.name).append('\n')
                    prevPkg = pkg
                }
                val type = service.type
                builder.append(service.algorithm)
                builder.append(" (").append(type).append(")\n")
                serviceTypes.add(type)
            }
            builder.deleteCharAt(builder.lastIndex)
        }
        list.add(InfoItem(name, builder.toString()))
        return list
    }

    private fun getServiceItem(serviceType: String): List<InfoItem> {
        val list = mutableListOf<InfoItem>()
        val algorithms = Security.getAlgorithms(serviceType)
        if (algorithms.isNotEmpty()) {
            val sb = java.lang.StringBuilder()
            for (alg in algorithms) {
                sb.append(alg).append('\n')
            }
            sb.deleteCharAt(sb.lastIndex)
            list.add(InfoItem("$serviceType algorithms", sb.toString()))
        }
        return list
    }

    private val securityContent = mutableListOf<InfoItem>()

    override suspend fun getItems(): List<InfoItem> {
        if (securityContent.isEmpty()) {
            val providers = Security.getProviders()
            if (providers.isEmpty()) {
                securityContent.add(InfoItem(R.string.item_security, R.string.security_none))
            } else {
                for (provider in providers) {
                    securityContent.addAll(getSecurityContent(provider))
                }
                for (serviceType in serviceTypes.toSortedSet()) {
                    securityContent.addAll(getServiceItem(serviceType))
                }
            }
        }
        return securityContent
    }
}
