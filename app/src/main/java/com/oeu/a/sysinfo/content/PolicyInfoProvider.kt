package com.oeu.a.sysinfo.content

import android.annotation.SuppressLint
import android.app.admin.DevicePolicyManager
import android.app.admin.SystemUpdatePolicy
import android.content.Context
import android.os.Build
import android.util.Log
import com.oeu.a.sysinfo.R
import com.oeu.a.sysinfo.utils.FirebaseHelper
import com.oeu.a.sysinfo.utils.isFlagsSet
import java.lang.IllegalArgumentException

class PolicyInfoProvider: InfoProvider() {
    companion object {
        private val TAG = PolicyInfoProvider::class.java.simpleName
    }

    @SuppressLint("newApi")
    private fun formatAdministrators(dpm: DevicePolicyManager): String {
        val admins = dpm.activeAdmins
        if (null == admins || 0 == admins.size) {
            return getString(R.string.policy_admin_none)
        }
        val sb = StringBuilder()
        for (admin in admins) {
            sb.append(admin.toShortString())
            if (Build.VERSION_CODES.JELLY_BEAN_MR2 <= Build.VERSION.SDK_INT) {
                if (dpm.isDeviceOwnerApp(admin.packageName)) {
                    sb.append("\n\tDevice owner")
                }
                if (dpm.isProfileOwnerApp(admin.packageName)) {
                    sb.append("\n\tProfile owner")
                }
            }
            sb.append('\n')
        }
        sb.deleteCharAt(sb.lastIndex)
        return sb.toString()
    }

    private fun formatRestrictions(dpm: DevicePolicyManager): String {
        val sb = java.lang.StringBuilder()
        try {
            if (!dpm.isActivePasswordSufficient) {
                sb.append("Insufficient password quality\n")
            }
        } catch (se: SecurityException) {
            FirebaseHelper.onThrowableCaught(se)
            se.printStackTrace()
        }
        if (Build.VERSION_CODES.HONEYCOMB <= Build.VERSION.SDK_INT) {
            if (dpm.getStorageEncryption(null)) {
                sb.append("Storage encryption required\n")
            }
        }
        if (Build.VERSION_CODES.ICE_CREAM_SANDWICH <= Build.VERSION.SDK_INT) {
            if (dpm.getCameraDisabled(null)) {
                sb.append("Camera disabled\n")
            }
        }
        if (Build.VERSION_CODES.LOLLIPOP <= Build.VERSION.SDK_INT) {
            if (dpm.autoTimeRequired) {
                sb.append("Auto time required\n")
            }
            if (dpm.getScreenCaptureDisabled(null)) {
                sb.append("Screen capture disabled\n")
            }
        }
        if (sb.isEmpty()) {
            sb.append("No specific restrictions")
        }
        return sb.toString()
    }

    @SuppressLint("newApi")
    private fun formatKeyguardDisabledFeatures(dpm: DevicePolicyManager): String {
        val sb = StringBuilder()
        var kdf = dpm.getKeyguardDisabledFeatures(null)
        sb.append(String.format("0x%08X", kdf))
        if (0 == kdf) {
            sb.append(" (none)")
        }
        if (0x7FFFFFFF == kdf) {
            sb.append(" (all)")
        }

        fun Int.parseFlag(flag: Int, description: String): Int {
            return if (this.isFlagsSet(flag)) {
                sb.append("\n\t").append(description).append(String.format(" 0x%08X", flag))
                this and flag.inv()
            } else {
                this
            }
        }

        kdf = kdf.parseFlag(DevicePolicyManager.KEYGUARD_DISABLE_FINGERPRINT, "Fingerprint")
        kdf = kdf.parseFlag(DevicePolicyManager.KEYGUARD_DISABLE_SECURE_CAMERA, "Camera")
        kdf = kdf.parseFlag(DevicePolicyManager.KEYGUARD_DISABLE_SECURE_NOTIFICATIONS, "Notifications")
        kdf = kdf.parseFlag(DevicePolicyManager.KEYGUARD_DISABLE_TRUST_AGENTS, "Trust agent")
        kdf = kdf.parseFlag(DevicePolicyManager.KEYGUARD_DISABLE_UNREDACTED_NOTIFICATIONS, "Unredacted notifications")
        kdf = kdf.parseFlag(DevicePolicyManager.KEYGUARD_DISABLE_WIDGETS_ALL, "All widgets")

        if (0 != kdf) {
            sb.append("\n\tUnrecognized flags: ").append(String.format("0x%08X", kdf))
        }
        return sb.toString()
    }

    private fun formatPasswordQuality(dpm: DevicePolicyManager): String {
        val sb = java.lang.StringBuilder()
        val pq = dpm.getPasswordQuality(null)
        when (pq) {
            DevicePolicyManager.PASSWORD_QUALITY_UNSPECIFIED -> sb.append("Unspecified")
            DevicePolicyManager.PASSWORD_QUALITY_SOMETHING -> sb.append("Any password")
            DevicePolicyManager.PASSWORD_QUALITY_NUMERIC -> sb.append("At least numeric")
            DevicePolicyManager.PASSWORD_QUALITY_NUMERIC_COMPLEX -> sb.append("At least complex numeric")
            DevicePolicyManager.PASSWORD_QUALITY_COMPLEX -> sb.append("Complex")
            DevicePolicyManager.PASSWORD_QUALITY_BIOMETRIC_WEAK -> sb.append("Weak biometric")
            DevicePolicyManager.PASSWORD_QUALITY_ALPHABETIC -> sb.append("Alphabetic")
            DevicePolicyManager.PASSWORD_QUALITY_ALPHANUMERIC -> sb.append("Alphanumeric")
            else -> sb.append(getString(R.string.unknown))
        }
        sb.append(String.format(" (0x%08X)", pq))
        sb.append(" Length ").append(dpm.getPasswordMinimumLength(null))
        sb.append(" ~ ").append(dpm.getPasswordMaximumLength(pq))
        return sb.toString()
    }

    private fun formatPasswordQualityDetail(dpm: DevicePolicyManager): String {
        val sb = java.lang.StringBuilder()
        val min = dpm.getPasswordMinimumLength(null)
        sb.append("Minimum length ").append(min)
        val minT = dpm.getPasswordMinimumLetters(null)
        val minL = dpm.getPasswordMinimumLowerCase(null)
        val minU = dpm.getPasswordMinimumUpperCase(null)
        val minN = dpm.getPasswordMinimumNumeric(null)
        val minX = dpm.getPasswordMinimumNonLetter(null)
        val minS = dpm.getPasswordMinimumSymbols(null)
        if (0 < minT) {
            sb.append("\n\tminimum letters: ").append(minT)
        }
        if (0 < minL) {
            sb.append("\n\tminimum lowercases: ").append(minL)
        }
        if (0 < minU) {
            sb.append("\n\tminimum uppercases: ").append(minU)
        }
        if (0 < minN) {
            sb.append("\n\tminimum numerics: ").append(minN)
        }
        if (0 < minX) {
            sb.append("\n\tminimum non-letters: ").append(minX)
        }
        if (0 < minS) {
            sb.append("\n\tminimum symbols: ").append(minS)
        }
        return sb.toString()
    }

    private fun formatStorageEncryptionStatus(dpm: DevicePolicyManager): String {
        val sb = java.lang.StringBuilder()
        val status = dpm.storageEncryptionStatus
        when (status) {
            DevicePolicyManager.ENCRYPTION_STATUS_UNSUPPORTED -> sb.append("Storage encryption not supported")
            DevicePolicyManager.ENCRYPTION_STATUS_INACTIVE -> sb.append("Inactive")
            DevicePolicyManager.ENCRYPTION_STATUS_ACTIVE_DEFAULT_KEY -> sb.append("Encrypted with default key")
            DevicePolicyManager.ENCRYPTION_STATUS_ACTIVATING -> sb.append("Encryption in progress")
            DevicePolicyManager.ENCRYPTION_STATUS_ACTIVE -> sb.append("Encrypted")
            DevicePolicyManager.ENCRYPTION_STATUS_ACTIVE_PER_USER -> sb.append("Active per user")
            else -> sb.append(getString(R.string.unknown))
        }
        sb.append(" (").append(status).append(')')
        if (dpm.getStorageEncryption(null)) {
            sb.append("\nEncryption required")
        }
        return sb.toString()
    }

    private fun formatInstallWindow(windowStart: Int, windowEnd: Int): String {
        val start = if (windowStart < 0) "??" else String.format("%02d:%02d", windowStart / 60, windowStart % 60)
        val end = if (windowEnd < 0) "??" else String.format("%02d:%02d", windowEnd / 60, windowEnd % 60)
        return "$start ~ $end"
    }

    @SuppressLint("newApi")
    private fun formatSystemUpdatePolicy(policy: SystemUpdatePolicy?): String {
        return policy?.let { policy ->
            val builder = StringBuilder()
            when (val type = policy.policyType) {
                SystemUpdatePolicy.TYPE_INSTALL_AUTOMATIC -> builder.append("Automatic ($type)")
                SystemUpdatePolicy.TYPE_INSTALL_WINDOWED -> builder.append("Windowed ($type)")
                    .append(' ').append(formatInstallWindow(policy.installWindowStart, policy.installWindowEnd))
                SystemUpdatePolicy.TYPE_POSTPONE -> builder.append("Postpone ($type)")
                else -> getString(R.string.unknown_int_value, type)
            }
            if (Build.VERSION_CODES.P <= Build.VERSION.SDK_INT) {
                val periods = policy.freezePeriods
                if (periods.isNotEmpty()) {
                    val description = periods.map { it.toString() }.toTypedArray()
                    builder.append("\nFreeze periods\n").append(formatStringArray(*description))
                }
            }
            builder.toString()
        } ?: getString(R.string.none)
    }

    private fun formatPasswordComplexity(complexity: Int): String {
        return when (complexity) {
            DevicePolicyManager.PASSWORD_COMPLEXITY_NONE -> getString(R.string.policy_password_complexity_none, complexity)
            DevicePolicyManager.PASSWORD_COMPLEXITY_LOW -> getString(R.string.policy_password_complexity_low, complexity)
            DevicePolicyManager.PASSWORD_COMPLEXITY_MEDIUM -> getString(R.string.policy_password_complexity_medium, complexity)
            DevicePolicyManager.PASSWORD_COMPLEXITY_HIGH -> getString(R.string.policy_password_complexity_high, complexity)
            else -> getString(R.string.unknown_int_value, complexity)
        }
    }

    private fun formatDurationMs(durationMs: Long): String {
        val sec = durationMs / 1000
        val min = sec / 60
        val hour = min / 60
        val day = hour / 24
        val year = day / 365
        return when {
            sec == 0L -> "$durationMs ms"
            min == 0L -> "$sec.${durationMs % 1000L} sec ($durationMs ms)"
            hour == 0L -> "$min min ${sec % 60} sec ($durationMs ms)"
            day == 0L -> "$hour hour ${min % 60} min ($durationMs ms)"
            year == 0L -> "$day day ${hour % 24} hour ($durationMs ms)"
            else -> "$year year ${day % 365} day ($durationMs ms)"
        }
    }

    @SuppressLint("newApi")
    private fun formatOtherFeatures(manager: DevicePolicyManager): String {
        val builder = StringBuilder()
        try {
            if (manager.isProvisioningAllowed(DevicePolicyManager.ACTION_PROVISION_MANAGED_PROFILE)) {
                builder.append("Profile provision allowed\n")
            }
            if (manager.isProvisioningAllowed(DevicePolicyManager.ACTION_PROVISION_MANAGED_DEVICE)) {
                builder.append("Device provision allowed\n")
            }
        } catch (e: IllegalArgumentException) {
            FirebaseHelper.onThrowableCaught(e)
            e.printStackTrace()
        }
        if (Build.VERSION_CODES.P <= Build.VERSION.SDK_INT) {
            if (manager.isAffiliatedUser) {
                builder.append("Affiliated user\n")
            }
            if (manager.isDeviceIdAttestationSupported) {
                builder.append("Device ID attestation supported\n")
            }
            if (manager.isLogoutEnabled) {
                builder.append("Logout enabled\n")
            }
        }
        if (Build.VERSION_CODES.R <= Build.VERSION.SDK_INT) {
            if (manager.isOrganizationOwnedDeviceWithManagedProfile) {
                builder.append("Device owned by an organization\n")
            }
            if (manager.isUniqueDeviceAttestationSupported) {
                builder.append("Unique device attestation supported\n")
            }
        }

        if (builder.isEmpty()) {
            builder.append(getString(R.string.none))
        }
        return builder.toString()
    }
    private val itemSpecs = listOf(
        InfoSpec(R.string.policy_admin, 8),
        InfoSpec(R.string.policy_management_disabled_types, 21),
        InfoSpec(R.string.policy_restrictions, 8),
        InfoSpec(R.string.policy_keyguard_disabled_features, 17),
        InfoSpec(R.string.policy_failed_password_attempt, 8),
        InfoSpec(R.string.policy_max_failed_password_for_wipe, 8),
        InfoSpec(R.string.policy_max_time_to_lock, 8),
        InfoSpec(R.string.policy_password_expiration, 11),
        InfoSpec(R.string.policy_password_expiration_timeout, 11),
        InfoSpec(R.string.policy_password_history_len, 11),
        InfoSpec(R.string.policy_password_quality, 8),
        InfoSpec(R.string.policy_password_quality_detail, 11),
        InfoSpec(R.string.policy_storage_encryption_status, 11),
        InfoSpec(R.string.policy_system_update_policy, 23),
        InfoSpec(R.string.policy_device_owner_lock_screen_info, 24),
        InfoSpec(R.string.policy_password_complexity, 29),
        InfoSpec(R.string.policy_strong_auth_timeout, 26),
        InfoSpec(R.string.policy_other_features, 24),
    )

    @SuppressLint("newApi")
    override fun getItem(infoId: Int, vararg args: Any?): InfoItem? {
        return (args[0] as? DevicePolicyManager)?.let { manager ->
            val value: String = try {
                when (infoId) {
                    R.string.policy_admin -> formatAdministrators(manager)
                    R.string.policy_management_disabled_types -> formatStringArray(*manager.accountTypesWithManagementDisabled!!)
                    R.string.policy_restrictions -> formatRestrictions(manager)
                    R.string.policy_keyguard_disabled_features -> formatKeyguardDisabledFeatures(manager)
                    R.string.policy_failed_password_attempt -> {
                        try {
                            manager.currentFailedPasswordAttempts.toString()
                        } catch (se: SecurityException) {
                            FirebaseHelper.onThrowableCaught(se)
                            se.printStackTrace()
                            getString(R.string.policy_not_watching_login)
                        }
                    }
                    R.string.policy_max_failed_password_for_wipe -> manager.getMaximumFailedPasswordsForWipe(null).toString()
                    R.string.policy_max_time_to_lock -> manager.getMaximumTimeToLock(null).toString()
                    R.string.policy_password_expiration -> manager.getPasswordExpiration(null).toString()
                    R.string.policy_password_expiration_timeout -> manager.getPasswordExpirationTimeout(null).toString()
                    R.string.policy_password_history_len -> manager.getPasswordHistoryLength(null).toString()
                    R.string.policy_password_quality -> formatPasswordQuality(manager)
                    R.string.policy_password_quality_detail -> formatPasswordQualityDetail(manager)
                    R.string.policy_storage_encryption_status -> formatStorageEncryptionStatus(manager)
                    R.string.policy_system_update_policy -> formatSystemUpdatePolicy(manager.systemUpdatePolicy)
                    R.string.policy_device_owner_lock_screen_info -> (manager.deviceOwnerLockScreenInfo)?.toString() ?: getString(R.string.none)
                    R.string.policy_password_complexity -> formatPasswordComplexity(manager.passwordComplexity)
                    R.string.policy_strong_auth_timeout -> formatDurationMs(manager.getRequiredStrongAuthTimeout(null))
                    R.string.policy_other_features -> formatOtherFeatures(manager)
                    else -> getString(R.string.invalid_item)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                FirebaseHelper.onThrowableCaught(e)
                getString(R.string.unsupported)
            }
            InfoItem(infoId, value)
        }
    }

    private val policyContent = mutableListOf<InfoItem>()
    override suspend fun getItems(): List<InfoItem> {
        if (policyContent.isEmpty()) {
            val manager = app.getSystemService(Context.DEVICE_POLICY_SERVICE) as DevicePolicyManager
            policyContent.addItems(itemSpecs, manager)
        }
        return policyContent
    }
}
