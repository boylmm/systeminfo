package com.oeu.a.sysinfo.content

import android.annotation.SuppressLint
import android.app.ActivityManager
import android.content.Context
import android.util.Log
import com.oeu.a.sysinfo.R
import com.oeu.a.sysinfo.utils.FirebaseHelper

class MemoryInfoProvider: InfoProvider() {
    companion object {
        private val TAG = MemoryInfoProvider::class.java.simpleName
    }

    private val itemSpecs = listOf(
        InfoSpec(R.string.memory_total, 16),
        InfoSpec(R.string.memory_available),
        InfoSpec(R.string.memory_threshold),
        InfoSpec(R.string.memory_runtime_free),
        InfoSpec(R.string.memory_runtime_max),
        InfoSpec(R.string.memory_runtime_total),
    )

    private val memoryInfo: ActivityManager.MemoryInfo by lazy {
        ActivityManager.MemoryInfo().also {
            (app.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager).getMemoryInfo(it)
        }
    }

    private val runtime: Runtime by lazy {
        Runtime.getRuntime()
    }

    @SuppressLint("newApi")
    override fun getItem(infoId: Int, vararg args: Any?): InfoItem {
        val title = getString(infoId)
        val value: String = try {
            when (infoId) {
                R.string.memory_total -> formatStorageSize(memoryInfo.totalMem)
                R.string.memory_available -> formatStorageSize(memoryInfo.availMem)
                R.string.memory_threshold -> formatStorageSize(memoryInfo.threshold)
                R.string.memory_runtime_free -> formatStorageSize(runtime.freeMemory())
                R.string.memory_runtime_max -> formatStorageSize(runtime.maxMemory())
                R.string.memory_runtime_total -> formatStorageSize(runtime.totalMemory())
                else -> getString(R.string.invalid_item)
            }
        } catch (e: Error) {
            FirebaseHelper.onThrowableCaught(e)
            e.printStackTrace()
            getString(R.string.unsupported)
        }
        return InfoItem(title, value)
    }

    private val memoryContent: MutableList<InfoItem> = mutableListOf()
    override suspend fun getItems(): List<InfoItem> {
        if (memoryContent.isEmpty()) {
            memoryContent.addItems(itemSpecs)
        }
        return memoryContent
    }
}
