package com.oeu.a.sysinfo.content

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.graphics.ImageFormat
import android.graphics.PixelFormat
import android.graphics.Rect
import android.hardware.Camera
import android.hardware.camera2.CameraCharacteristics
import android.hardware.camera2.CameraManager
import android.hardware.camera2.CameraMetadata
import android.hardware.camera2.params.Capability
import android.hardware.camera2.params.MandatoryStreamCombination
import android.hardware.camera2.params.StreamConfigurationMap
import android.os.Build
import android.util.*
import androidx.annotation.RequiresApi
import com.oeu.a.sysinfo.R
import com.oeu.a.sysinfo.utils.FirebaseHelper
import com.oeu.a.sysinfo.utils.className
import com.oeu.a.sysinfo.utils.trimEnd
import java.lang.Exception
import kotlin.Pair

private class CameraInfoLegacy: InfoProvider() {
    //  Support pre-Lollipop devices which do not have CameraManager
    companion object {
        private val TAG = CameraInfoLegacy::class.java.simpleName
        private const val MAGIC_INT = -7151
    }

    private fun formatZoomRatios(ratios: List<Int>?): String {
        val sb = StringBuilder()
        if (null != ratios) {
            for (ratio in ratios) {
                val i = ratio / 100
                val f = ratio % 100
                sb.append(i).append('.').append(f).append('\n')
            }
        }
        if (sb.isNotEmpty()) {
            sb.deleteCharAt(sb.lastIndex)
        } else {
            sb.append(getString(R.string.unsupported))
        }
        return sb.toString()
    }

    private fun formatStringList(strings: List<String>?): String {
        val sb = StringBuilder()
        if (null != strings) {
            for (str in strings) {
                sb.append(str).append('\n')
            }
        }
        if (sb.isNotEmpty()) {
            sb.deleteCharAt(sb.lastIndex)
        } else {
            sb.append(getString(R.string.unsupported))
        }
        return sb.toString()
    }

    private fun formatImageFormats(formats: List<Int>?): String {
        val sb = StringBuilder()
        if (null != formats) {
            for (format in formats) {
                val name = when (format) {
                    ImageFormat.JPEG -> "JPEG"
                    ImageFormat.NV16 -> "NV16"
                    ImageFormat.NV21 -> "NV21"
                    ImageFormat.RGB_565 -> "RGB 565"
                    ImageFormat.YUV_420_888 -> "Generic YCbCr"
                    ImageFormat.YUY2 -> "YUY2"
                    ImageFormat.YV12 -> "YUV"
                    ImageFormat.UNKNOWN -> getString(R.string.unknown)
                    else -> getString(R.string.unknown)
                }
                sb.append(name).append(" (").append(format).append(")\n")
            }
        }
        if (sb.isNotEmpty()) {
            sb.deleteCharAt(sb.lastIndex)
        } else {
            sb.append(getString(R.string.unsupported))
        }
        return sb.toString()
    }

    private fun formatPixelSize(size: Long): String {
        var kidx = 0
        var tmp = size
        var div: Long = 1
        while (tmp > 1024) {
            ++kidx
            tmp /= 1024
            div *= 1024
        }
        val v = size.toFloat() / div.toFloat()
        if (InfoProvider.siPrefixes.size <= kidx) {
            kidx = InfoProvider.siPrefixes.size - 1
        }
        return String.format("%.1f %s pixels", v, InfoProvider.siPrefixes[kidx])
    }

    private fun formatSizes(sizes: List<Camera.Size>?, showPixels: Boolean): String {
        val sb = StringBuilder()
        if (null != sizes) {
            for (size in sizes) {
                if (0 < size.width && 0 < size.height) {  //  ignore zero-size for thumbnail sizes
                    sb.append(size.width).append(" x ").append(size.height)
                    if (showPixels) {
                        sb.append(" (").append(formatPixelSize(size.width.toLong() * size.height.toLong())).append(')')
                    }
                    sb.append('\n')
                }
            }
        }
        if (sb.isNotEmpty()) {
            sb.deleteCharAt(sb.lastIndex)
        } else {
            sb.append(getString(R.string.unsupported))
        }
        return sb.toString()
    }

    private fun formatFpsRangeList(ranges: List<IntArray>?): String {
        val sb = StringBuilder()
        if (null != ranges) {
            for (range in ranges) {
                val min = range[Camera.Parameters.PREVIEW_FPS_MIN_INDEX] / 1000f
                val max = range[Camera.Parameters.PREVIEW_FPS_MAX_INDEX] / 1000f
                if (min == max) {
                    sb.append(min)
                } else {
                    sb.append(min).append(" - ").append(max)
                }
                sb.append(" fps\n")
            }
        }
        if (sb.isNotEmpty()) {
            sb.deleteCharAt(sb.lastIndex)
        } else {
            sb.append(getString(R.string.unsupported))
        }
        return sb.toString()
    }

    private fun getCameraInfoItem(info: Camera.CameraInfo, prefix: String): List<InfoItem> {
        val list = mutableListOf<InfoItem>()
        list.add(InfoItem(prefix + getString(R.string.camera_facing), if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) R.string.camera_facing_back else R.string.camera_facing_front))
        list.add(InfoItem(prefix + getString(R.string.camera_orientation), info.orientation.toString()))
        if (Build.VERSION_CODES.JELLY_BEAN_MR1 <= Build.VERSION.SDK_INT) {
            list.add(InfoItem(prefix + getString(R.string.camera_mute_shutter_sound), if (info.canDisableShutterSound) R.string.camera_allowed else R.string.camera_not_allowed))
        }
        return list
    }

    private fun getCameraFeatureItem(parameters: Camera.Parameters, prefix: String): InfoItem? {
        val sb = StringBuilder()
        if (parameters.isAutoExposureLockSupported()) {
            sb.append(getString(R.string.camera_auto_exposure_lock)).append('\n')
        }
        if (parameters.isAutoWhiteBalanceLockSupported()) {
            sb.append(getString(R.string.camera_auto_white_balance_lock)).append('\n')
        }
        if (parameters.isZoomSupported()) {
            sb.append(getString(R.string.camera_zoom))
            if (parameters.isSmoothZoomSupported()) {
                sb.append(" (").append(getString(R.string.camera_smooth_zoom)).append(')')
            }
            sb.append('\n')
        }
        if (parameters.isVideoSnapshotSupported()) {
            sb.append(getString(R.string.camera_video_snapshot)).append('\n')
        }
        if (Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1 <= Build.VERSION.SDK_INT) {
            if (parameters.isVideoStabilizationSupported()) {
                sb.append(getString(R.string.camera_video_stabilization)).append('\n')
            }
        }
        return if (sb.isNotEmpty()) {
            sb.deleteCharAt(sb.lastIndex)
            InfoItem(prefix + getString(R.string.camera_features), sb.toString())
        } else {
            null
        }
    }

    private fun getCameraParameterItems(parameters: Camera.Parameters, prefix: String): List<InfoItem> {
        val list = mutableListOf<InfoItem>()
        list.add(InfoItem(prefix + getString(R.string.camera_supported_antibanding), formatStringList(parameters.getSupportedAntibanding())))
        list.add(InfoItem(prefix + getString(R.string.camera_supported_color_effects), formatStringList(parameters.getSupportedColorEffects())))
        list.add(InfoItem(prefix + getString(R.string.camera_supported_flash_modes), formatStringList(parameters.getSupportedFlashModes())))
        list.add(InfoItem(prefix + getString(R.string.camera_supported_focus_modes), formatStringList(parameters.getSupportedFocusModes())))
        list.add(InfoItem(prefix + getString(R.string.camera_supported_scene_modes), formatStringList(parameters.getSupportedSceneModes())))
        list.add(InfoItem(prefix + getString(R.string.camera_supported_white_balance), formatStringList(parameters.getSupportedWhiteBalance())))
        list.add(InfoItem(prefix + getString(R.string.camera_supported_picture_formats), formatImageFormats(parameters.getSupportedPictureFormats())))
        list.add(InfoItem(prefix + getString(R.string.camera_supported_picture_sizes), formatSizes(parameters.getSupportedPictureSizes(), true)))
        list.add(InfoItem(prefix + getString(R.string.camera_supported_thumbnail_sizes), formatSizes(parameters.getSupportedJpegThumbnailSizes(), false)))
        list.add(InfoItem(prefix + getString(R.string.camera_supported_preview_formats), formatImageFormats(parameters.getSupportedPreviewFormats())))
        list.add(InfoItem(prefix + getString(R.string.camera_supported_preview_fps), formatFpsRangeList(parameters.getSupportedPreviewFpsRange())))
        list.add(InfoItem(prefix + getString(R.string.camera_supported_preview_sizes), formatSizes(parameters.getSupportedPreviewSizes(), false)))
        list.add(InfoItem(prefix + getString(R.string.camera_supported_video_sizes), formatSizes(parameters.getSupportedVideoSizes(), true)))
        getCameraFeatureItem(parameters, prefix)?.also { list.add(it) }
        if (parameters.isZoomSupported) {
            list.add(InfoItem(prefix + getString(R.string.camera_zoom_ratios), formatZoomRatios(parameters.zoomRatios)))
        }
        return list
    }

    private fun getCameraItems(idx: Int): List<InfoItem> {
        val name = getString(R.string.camera_name, idx)
        val camera = try {
            Camera.open(idx)
        } catch (e: RuntimeException) {
            FirebaseHelper.onThrowableCaught(e)
            e.printStackTrace()
            return listOf(InfoItem(name, R.string.unsupported))
        }
        val list = mutableListOf<InfoItem>()
        val prefix = "$name: "
        val cameraInfo = Camera.CameraInfo()
        cameraInfo.facing = MAGIC_INT
        Camera.getCameraInfo(idx, cameraInfo)
        if (MAGIC_INT != cameraInfo.facing) {
            list.addAll(getCameraInfoItem(cameraInfo, prefix))
        }
        list.addAll(getCameraParameterItems(camera.getParameters(), prefix))
        camera.release()
        return list
    }

    private fun getCameraItemsLegacy(): List<InfoItem> {
        val list = mutableListOf<InfoItem>()
        val count = Camera.getNumberOfCameras()
        if (0 == count) {
            list.add(InfoItem(R.string.item_camera, R.string.camera_none))
        } else {
            for (idx in 0 until count) {
                list.addAll(getCameraItems(idx))
            }
        }
        return list
    }

    private val legacyContent = mutableListOf<InfoItem>()
    override suspend fun getItems(): List<InfoItem> {
        if (legacyContent.isEmpty()) {
            legacyContent.addAll(getCameraItemsLegacy())
        }
        return legacyContent
    }

    suspend fun getLegacyItems(): List<InfoItem> {
        return getItems()
    }

    override fun getItem(infoId: Int, vararg args: Any?): InfoItem? = null
}

class CameraInfoProvider: InfoProvider() {
    companion object {
        private val TAG = CameraInfoProvider::class.java.simpleName
    }

    override val requiredPermissions: Array<String> = arrayOf(
        Manifest.permission.CAMERA
    )

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun formatCharacteristics(characteristics: CameraCharacteristics, outBuilder: StringBuilder? = null): String {
        val builder = outBuilder ?: StringBuilder()
        val startIdx = builder.length
        for (key in characteristics.keys.sortedBy { it.name }) {
            val value = characteristics[key]
            builder.append(key.name).append(": ").append(value.className()).append('\n')
        }
        return builder.trimEnd().substring(startIdx)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun getTestItem(): InfoItem {
        val builder = StringBuilder()
        val manager = app.getSystemService(Context.CAMERA_SERVICE) as CameraManager
        builder.append("-- Camera ID\n")
        for (id in manager.cameraIdList) {
            builder.append(id).append('\n')
        }
        for (id in manager.cameraIdList) {
            builder.append("-- Characteristics of: $id\n")
            try {
                formatCharacteristics(manager.getCameraCharacteristics(id), builder)
                builder.append("\n\n")
            } catch (e: Exception) {
                FirebaseHelper.onThrowableCaught(e)
                e.printStackTrace()
            }
        }
        return InfoItem("Test", builder.toString())
    }

    private fun formatConcurrentIds(sets: Set<Set<String>>): String {
        if (sets.isEmpty()) {
            return getString(R.string.not_available)
        }
        val builder = StringBuilder()
        for (set in sets) {
            builder.append("{ ")
            for (id in set) {
                builder.append(id).append(' ')
            }
            builder.append("}\n")
        }
        builder.deleteCharAt(builder.lastIndex)
        return builder.toString()
    }

    private val generalSpecs = listOf(
        InfoSpec(R.string.camera_count, 21),
        InfoSpec(R.string.camera_concurrent_ids, 30),
    )

    @SuppressLint("newApi")
    override fun getItem(infoId: Int, vararg args: Any?): InfoItem? {
        return (args[0] as? CameraManager)?.let { manager ->
            val value = when (infoId) {
                R.string.camera_count -> manager.cameraIdList.size.toString()
                R.string.camera_concurrent_ids -> formatConcurrentIds(manager.concurrentCameraIds)
                else -> getString(R.string.invalid_item)
            }
            InfoItem(infoId, value)
        }
    }

    private fun formatBooleanArray(values: BooleanArray): String {
        val builder = java.lang.StringBuilder()
        if (values.isEmpty()) {
            builder.append(getString(R.string.none))
        } else for (value in values) {
            builder.append(value.toString()).append('\n')
        }
        return builder.trimEnd().toString()
    }

    private fun formatInt(value: Int, map: Map<Int, String>): String {
        return (map[value] ?: getString(R.string.unknown)) + " ($value)"
    }

    private fun formatIntArray(values: IntArray, map: Map<Int, String>, outBuilder: StringBuilder? = null): String {
        val builder = outBuilder ?: StringBuilder()
        val startIdx = builder.length
        if (values.isEmpty()) {
            builder.append(getString(R.string.none))
        } else for (value in values) {
            builder.append(formatInt(value, map)).append('\n')
        }
        return builder.trimEnd().substring(startIdx)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun formatRange(value: Range<*>): String {
        return "${value.lower} ~ ${value.upper}"
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun formatRangeArray(ranges: Array<out Range<*>>, outBuilder: StringBuilder? = null): String {
        val builder = outBuilder ?: StringBuilder()
        val startIdx = builder.length
        if (ranges.isEmpty()) {
            builder.append(getString(R.string.none))
        } else for (range in ranges) {
            builder.append(formatRange(range)).append('\n')
        }
        return builder.trimEnd().substring(startIdx)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun formatSize(size: Size): String {
        return String.format("%,d x %,d", size.width, size.height)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun formatSizeArray(sizes: Array<out Size>, outBuilder: StringBuilder? = null): String {
        val builder = outBuilder ?: StringBuilder()
        val startIdx = builder.length
        if (sizes.isEmpty()) {
            builder.append(getString(R.string.none))
        } else for (size in sizes) {
            builder.append(formatSize(size)).append('\n')
        }
        return builder.trimEnd().substring(startIdx)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun formatSizeF(size: SizeF): String {
        return String.format("%f x %f", size.width, size.height)
    }

    private fun formatRect(rect: Rect, outBuilder: StringBuilder? = null): String {
        val builder = outBuilder ?: StringBuilder()
        val startIdx = builder.length
        builder.append("(${rect.left}, ${rect.top})~(${rect.right}, ${rect.bottom}) ${rect.width()} x ${rect.height()}")
        return builder.substring(startIdx)
    }

    private fun formatRectArray(rects: Array<out Rect>, outBuilder: StringBuilder? = null): String {
        val builder = outBuilder ?: StringBuilder()
        val startIdx = builder.length
        if (rects.isEmpty()) {
            builder.append(getString(R.string.none))
        } else for (rect in rects) {
            builder.append(formatRect(rect)).append('\n')
        }
        return builder.trimEnd().substring(startIdx)
    }

    private val colorCorrectionAberrationModes = mapOf(
        Pair(CameraMetadata.COLOR_CORRECTION_ABERRATION_MODE_OFF, "Off"),
        Pair(CameraMetadata.COLOR_CORRECTION_ABERRATION_MODE_FAST, "Fast"),
        Pair(CameraMetadata.COLOR_CORRECTION_ABERRATION_MODE_HIGH_QUALITY, "High Quality"),
    )
    private fun formatColorCorrectionAvailableAberrationModes(values: IntArray): String {
        return formatIntArray(values, colorCorrectionAberrationModes)
    }

    private val controlAeAntibandingModes = mapOf(
        Pair(CameraMetadata.CONTROL_AE_ANTIBANDING_MODE_OFF, "Off"),
        Pair(CameraMetadata.CONTROL_AE_ANTIBANDING_MODE_50HZ, "50 Hz"),
        Pair(CameraMetadata.CONTROL_AE_ANTIBANDING_MODE_60HZ, "60 Hz"),
        Pair(CameraMetadata.CONTROL_AE_ANTIBANDING_MODE_AUTO, "Auto"),
    )
    private fun formatControlAeAvailableAntibandingModes(values: IntArray): String {
        return formatIntArray(values, controlAeAntibandingModes)
    }

    private val controlAeModes = mapOf(
        Pair(CameraMetadata.CONTROL_AE_MODE_OFF, "Off"),
        Pair(CameraMetadata.CONTROL_AE_MODE_ON, "On"),
        Pair(CameraMetadata.CONTROL_AE_MODE_ON_AUTO_FLASH, "On, auto flash"),
        Pair(CameraMetadata.CONTROL_AE_MODE_ON_ALWAYS_FLASH, "On, always flash"),
        Pair(CameraMetadata.CONTROL_AE_MODE_ON_AUTO_FLASH_REDEYE, "On, auto flash, red eye"),
        Pair(CameraMetadata.CONTROL_AE_MODE_ON_EXTERNAL_FLASH, "On, external flash"),
    )
    private fun formatControlAeModes(values: IntArray): String {
        return formatIntArray(values, controlAeModes)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun formatControlAeCompensationRange(value: Range<Int>): String {
        return formatRange(value)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun formatControlAeCompensationStep(value: Rational): String {
        return "${value.numerator}/${value.denominator}"
    }

    private val controlAfModes = mapOf(
        Pair(CameraMetadata.CONTROL_AF_MODE_OFF, "Off"),
        Pair(CameraMetadata.CONTROL_AF_MODE_AUTO, "Auto"),
        Pair(CameraMetadata.CONTROL_AF_MODE_MACRO, "Macro"),
        Pair(CameraMetadata.CONTROL_AF_MODE_CONTINUOUS_VIDEO, "Continuous video"),
        Pair(CameraMetadata.CONTROL_AF_MODE_CONTINUOUS_PICTURE, "Continuous picture"),
        Pair(CameraMetadata.CONTROL_AF_MODE_EDOF, "EDOF (Extended depth of field)"),
    )
    private fun formatControlAfAvailableModes(values: IntArray): String {
        return formatIntArray(values, controlAfModes)
    }

    private val controlEffectModes = mapOf(
        Pair(CameraMetadata.CONTROL_EFFECT_MODE_OFF, "Off"),
        Pair(CameraMetadata.CONTROL_EFFECT_MODE_MONO, "Mono"),
        Pair(CameraMetadata.CONTROL_EFFECT_MODE_NEGATIVE, "Negative"),
        Pair(CameraMetadata.CONTROL_EFFECT_MODE_SOLARIZE, "Solarize"),
        Pair(CameraMetadata.CONTROL_EFFECT_MODE_SEPIA, "Sepia"),
        Pair(CameraMetadata.CONTROL_EFFECT_MODE_POSTERIZE, "Posterize"),
        Pair(CameraMetadata.CONTROL_EFFECT_MODE_WHITEBOARD, "Whiteboard"),
        Pair(CameraMetadata.CONTROL_EFFECT_MODE_BLACKBOARD, "Blackboard"),
        Pair(CameraMetadata.CONTROL_EFFECT_MODE_AQUA, "Aqua"),
    )
    private fun formatControlAvailableEffects(values: IntArray): String {
        return formatIntArray(values, controlEffectModes)
    }

    private val controlExtendedSceneModes = mapOf(
        Pair(CameraMetadata.CONTROL_EXTENDED_SCENE_MODE_DISABLED, "Disabled"),
        Pair(CameraMetadata.CONTROL_EXTENDED_SCENE_MODE_BOKEH_STILL_CAPTURE, "Bokeh, still capture"),
        Pair(CameraMetadata.CONTROL_EXTENDED_SCENE_MODE_BOKEH_CONTINUOUS, "Bokeh, continuous"),
    )
    private fun formatExtendedSceneMode(mode: Int): String {
        return formatInt(mode, controlExtendedSceneModes)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun formatAvailableExtendedSceneModeCapabilities(values: Array<Capability>): String {
        if (values.isEmpty()) {
            return getString(R.string.none)
        }
        val builder = StringBuilder()
        for (value in values) {
            builder.append("Mode: ").append(formatExtendedSceneMode(value.mode)).append('\n')
            builder.append("Zoom ratio range: ").append(formatRange(value.zoomRatioRange)).append('\n')
            builder.append("Max streaming size: ").append(formatSize(value.maxStreamingSize)).append("\n\n")
        }
        return builder.trimEnd().toString()
    }

    private val controlModes = mapOf(
        Pair(CameraMetadata.CONTROL_MODE_OFF, "Off"),
        Pair(CameraMetadata.CONTROL_MODE_AUTO, "Auto"),
        Pair(CameraMetadata.CONTROL_MODE_USE_SCENE_MODE, "Use scene mode"),
        Pair(CameraMetadata.CONTROL_MODE_OFF_KEEP_STATE, "Off, keep state"),
        Pair(CameraMetadata.CONTROL_MODE_USE_EXTENDED_SCENE_MODE, "Use extended scene mode"),
    )
    private fun formatControlAvailableModes(values: IntArray): String {
        return formatIntArray(values, controlModes)
    }

    private val controlSceneModes = mapOf(
        Pair(CameraMetadata.CONTROL_SCENE_MODE_DISABLED, "Disabled"),
        Pair(CameraMetadata.CONTROL_SCENE_MODE_FACE_PRIORITY, "Face priority"),
        Pair(CameraMetadata.CONTROL_SCENE_MODE_ACTION, "Action"),
        Pair(CameraMetadata.CONTROL_SCENE_MODE_PORTRAIT, "Portrait"),
        Pair(CameraMetadata.CONTROL_SCENE_MODE_LANDSCAPE, "Landscape"),
        Pair(CameraMetadata.CONTROL_SCENE_MODE_NIGHT, "Night"),
        Pair(CameraMetadata.CONTROL_SCENE_MODE_NIGHT_PORTRAIT, "Night portrait"),
        Pair(CameraMetadata.CONTROL_SCENE_MODE_THEATRE, "Theatre"),
        Pair(CameraMetadata.CONTROL_SCENE_MODE_BEACH, "Beach"),
        Pair(CameraMetadata.CONTROL_SCENE_MODE_SNOW, "Snow"),
        Pair(CameraMetadata.CONTROL_SCENE_MODE_SUNSET, "Sunset"),
        Pair(CameraMetadata.CONTROL_SCENE_MODE_STEADYPHOTO, "Steady photo"),
        Pair(CameraMetadata.CONTROL_SCENE_MODE_FIREWORKS, "Fireworks"),
        Pair(CameraMetadata.CONTROL_SCENE_MODE_SPORTS, "Sports"),
        Pair(CameraMetadata.CONTROL_SCENE_MODE_PARTY, "Party"),
        Pair(CameraMetadata.CONTROL_SCENE_MODE_CANDLELIGHT, "Candle light"),
        Pair(CameraMetadata.CONTROL_SCENE_MODE_BARCODE, "Barcode"),
        Pair(CameraMetadata.CONTROL_SCENE_MODE_HIGH_SPEED_VIDEO, "High speed video"),
        Pair(CameraMetadata.CONTROL_SCENE_MODE_HDR, "HDR (High dynamic range)"),
    )
    private fun formatControlAvailableSceneModes(values: IntArray): String {
        return formatIntArray(values, controlSceneModes)
    }

    private val controlVideoStabilizationModes = mapOf(
        Pair(CameraMetadata.CONTROL_VIDEO_STABILIZATION_MODE_OFF, "Off"),
        Pair(CameraMetadata.CONTROL_VIDEO_STABILIZATION_MODE_ON, "On"),
    )
    private fun formatControlAvailableVideoStabilizationModes(values: IntArray): String {
        return formatIntArray(values, controlVideoStabilizationModes)
    }

    private val controlAwbModes = mapOf(
        Pair(CameraMetadata.CONTROL_AWB_MODE_OFF, "Off"),
        Pair(CameraMetadata.CONTROL_AWB_MODE_AUTO, "Auto"),
        Pair(CameraMetadata.CONTROL_AWB_MODE_INCANDESCENT, "Incandescent"),
        Pair(CameraMetadata.CONTROL_AWB_MODE_FLUORESCENT, "Fluorescent"),
        Pair(CameraMetadata.CONTROL_AWB_MODE_WARM_FLUORESCENT, "Warm fluorescent"),
        Pair(CameraMetadata.CONTROL_AWB_MODE_DAYLIGHT, "Daylight"),
        Pair(CameraMetadata.CONTROL_AWB_MODE_CLOUDY_DAYLIGHT, "Cloudy daylight"),
        Pair(CameraMetadata.CONTROL_AWB_MODE_TWILIGHT, "Twilight"),
        Pair(CameraMetadata.CONTROL_AWB_MODE_SHADE, "Shade"),
    )
    private fun formatControlAwbAvailableModes(values: IntArray): String {
        return formatIntArray(values, controlAwbModes)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun formatControlPostRawSensitivityBoostRange(value: Range<Int>): String {
        return formatRange(value)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun formatControlZoomRatioRange(value: Range<Float>): String {
        return formatRange(value)
    }

    private val distortionCorrectionModes = mapOf(
        Pair(CameraMetadata.DISTORTION_CORRECTION_MODE_OFF, "Off"),
        Pair(CameraMetadata.DISTORTION_CORRECTION_MODE_FAST, "Fast"),
        Pair(CameraMetadata.DISTORTION_CORRECTION_MODE_HIGH_QUALITY, "High quality"),
    )
    private fun formatDistortionCorrectionAvailableModes(values: IntArray): String {
        return formatIntArray(values, distortionCorrectionModes)
    }

    private val edgeModes = mapOf(
        Pair(CameraMetadata.EDGE_MODE_OFF, "Off"),
        Pair(CameraMetadata.EDGE_MODE_FAST, "Fast"),
        Pair(CameraMetadata.EDGE_MODE_HIGH_QUALITY, "High quality"),
        Pair(CameraMetadata.EDGE_MODE_ZERO_SHUTTER_LAG, "Zero shutter lag"),
    )
    private fun formatEdgeAvailableEdgeModes(values: IntArray): String {
        return formatIntArray(values, edgeModes)
    }

    private val hotPixelModes = mapOf(
        Pair(CameraMetadata.HOT_PIXEL_MODE_OFF, "Off"),
        Pair(CameraMetadata.HOT_PIXEL_MODE_FAST, "Fast"),
        Pair(CameraMetadata.HOT_PIXEL_MODE_HIGH_QUALITY, "High quality"),
    )
    private fun formatHotPixelAvailableHotPixelModes(values: IntArray): String {
        return formatIntArray(values, hotPixelModes)
    }

    private val supportedHardwareLevels = mapOf(
        Pair(CameraMetadata.INFO_SUPPORTED_HARDWARE_LEVEL_LEGACY, "Legacy"),
        Pair(CameraMetadata.INFO_SUPPORTED_HARDWARE_LEVEL_LIMITED, "Limited"),
        Pair(CameraMetadata.INFO_SUPPORTED_HARDWARE_LEVEL_FULL, "Full"),
        Pair(CameraMetadata.INFO_SUPPORTED_HARDWARE_LEVEL_3, "Level 3"),
        Pair(CameraMetadata.INFO_SUPPORTED_HARDWARE_LEVEL_EXTERNAL, "External"),
    )
    private fun formatSupportedHardwareLevel(value: Int): String {
        return formatInt(value, supportedHardwareLevels)
    }

    private val lensFacings = mapOf(
        Pair(CameraMetadata.LENS_FACING_FRONT, "Front"),
        Pair(CameraMetadata.LENS_FACING_BACK, "Back"),
        Pair(CameraMetadata.LENS_FACING_EXTERNAL, "External"),
    )
    private fun formatLensFacing(value: Int): String {
        return formatInt(value, lensFacings)
    }

    private val lensOpticalStabilizations = mapOf(
        Pair(CameraMetadata.LENS_OPTICAL_STABILIZATION_MODE_OFF, "Off"),
        Pair(CameraMetadata.LENS_OPTICAL_STABILIZATION_MODE_ON, "On"),
    )
    private fun formatLensInfoAvailableOpticalStabilization(values: IntArray): String {
        return formatIntArray(values, lensOpticalStabilizations)
    }

    private val focusDistanceCalibrations = mapOf(
        Pair(CameraMetadata.LENS_INFO_FOCUS_DISTANCE_CALIBRATION_UNCALIBRATED, "Uncalibrated"),
        Pair(CameraMetadata.LENS_INFO_FOCUS_DISTANCE_CALIBRATION_APPROXIMATE, "Approximate"),
        Pair(CameraMetadata.LENS_INFO_FOCUS_DISTANCE_CALIBRATION_CALIBRATED, "Calibrated"),
    )

    private val lensPoseReferences = mapOf(
        Pair(CameraMetadata.LENS_POSE_REFERENCE_PRIMARY_CAMERA, "Primary camera"),
        Pair(CameraMetadata.LENS_POSE_REFERENCE_GYROSCOPE, "Gyroscope"),
        Pair(CameraMetadata.LENS_POSE_REFERENCE_UNDEFINED, "Undefined"),
    )

    private val logicalMultiCameraSensorSyncTypes = mapOf(
        Pair(CameraMetadata.LOGICAL_MULTI_CAMERA_SENSOR_SYNC_TYPE_APPROXIMATE, "Approximate"),
        Pair(CameraMetadata.LOGICAL_MULTI_CAMERA_SENSOR_SYNC_TYPE_CALIBRATED, "Calibrated"),
    )

    private val noiseReductionModes = mapOf(
        Pair(CameraMetadata.NOISE_REDUCTION_MODE_OFF, "Off"),
        Pair(CameraMetadata.NOISE_REDUCTION_MODE_FAST, "Fast"),
        Pair(CameraMetadata.NOISE_REDUCTION_MODE_HIGH_QUALITY, "High quality"),
        Pair(CameraMetadata.NOISE_REDUCTION_MODE_MINIMAL, "Minimal"),
        Pair(CameraMetadata.NOISE_REDUCTION_MODE_ZERO_SHUTTER_LAG, "Zero shutter lag"),
    )

    private val requestAvailableCapabilities = mapOf(
        Pair(CameraMetadata.REQUEST_AVAILABLE_CAPABILITIES_BACKWARD_COMPATIBLE, "Backward compatible"),
        Pair(CameraMetadata.REQUEST_AVAILABLE_CAPABILITIES_BURST_CAPTURE, "Burst capture"),
        Pair(CameraMetadata.REQUEST_AVAILABLE_CAPABILITIES_CONSTRAINED_HIGH_SPEED_VIDEO, "Constrained high speed video"),
        Pair(CameraMetadata.REQUEST_AVAILABLE_CAPABILITIES_DEPTH_OUTPUT, "Depth output"),
        Pair(CameraMetadata.REQUEST_AVAILABLE_CAPABILITIES_LOGICAL_MULTI_CAMERA, "Logical multi-camera"),
        Pair(CameraMetadata.REQUEST_AVAILABLE_CAPABILITIES_MANUAL_POST_PROCESSING, "Manual post processing"),
        Pair(CameraMetadata.REQUEST_AVAILABLE_CAPABILITIES_MANUAL_SENSOR, "Manual sensor"),
        Pair(CameraMetadata.REQUEST_AVAILABLE_CAPABILITIES_MONOCHROME, "Monochrome"),
        Pair(CameraMetadata.REQUEST_AVAILABLE_CAPABILITIES_MOTION_TRACKING, "Motion tracking"),
        Pair(CameraMetadata.REQUEST_AVAILABLE_CAPABILITIES_OFFLINE_PROCESSING, "Offline processing"),
        Pair(CameraMetadata.REQUEST_AVAILABLE_CAPABILITIES_PRIVATE_REPROCESSING, "private reprocessing"),
        Pair(CameraMetadata.REQUEST_AVAILABLE_CAPABILITIES_RAW, "Raw"),
        Pair(CameraMetadata.REQUEST_AVAILABLE_CAPABILITIES_READ_SENSOR_SETTINGS, "Read sensor settings"),
        Pair(CameraMetadata.REQUEST_AVAILABLE_CAPABILITIES_SECURE_IMAGE_DATA, "Secure image data"),
        Pair(CameraMetadata.REQUEST_AVAILABLE_CAPABILITIES_SYSTEM_CAMERA, "System camera"),
        Pair(CameraMetadata.REQUEST_AVAILABLE_CAPABILITIES_YUV_REPROCESSING, "YUV reprocessing"),
    )

    private val scalerCroppingTypes = mapOf(
        Pair(CameraMetadata.SCALER_CROPPING_TYPE_CENTER_ONLY, "Center only"),
        Pair(CameraMetadata.SCALER_CROPPING_TYPE_FREEFORM, "Freedom"),
    )

    private val imageFormats = mapOf(
        Pair(ImageFormat.DEPTH16, "Depth: 16 bit"),
        Pair(ImageFormat.DEPTH_JPEG, "Depth: JPEG"),
        Pair(ImageFormat.DEPTH_POINT_CLOUD, "Depth: point cloud"),
        Pair(ImageFormat.FLEX_RGBA_8888, "Multi-plane RGBA 8888"),
        Pair(ImageFormat.FLEX_RGB_888, "Multi-plane RGB 888"),
        Pair(ImageFormat.HEIC, "HEIC"),
        Pair(ImageFormat.JPEG, "JPEG"),
        Pair(ImageFormat.NV16, "NV16: YCbCr video"),
        Pair(ImageFormat.NV21, "NV21: YCbCr image"),
        Pair(ImageFormat.PRIVATE, "Private opaque image"),
        Pair(ImageFormat.RAW10, "Raw: 10 bit"),
        Pair(ImageFormat.RAW12, "Raw: 12 bit"),
        Pair(ImageFormat.RAW_PRIVATE, "Raw: private"),
        Pair(ImageFormat.RAW_SENSOR, "Raw: sensor"),
        Pair(ImageFormat.RGB_565, "RGB 565"),
        Pair(ImageFormat.UNKNOWN, "Unknown"),
        Pair(ImageFormat.Y8, "Y8: YUV planar"),
        Pair(ImageFormat.YUV_420_888, "Multi-plane YUV 420"),
        Pair(ImageFormat.YUV_422_888, "Multi-plane YUV 422"),
        Pair(ImageFormat.YUV_444_888, "Multi-plane YUV 444"),
        Pair(ImageFormat.YUY2, "YUY2 (YUYV): YCbCr image"),
        Pair(ImageFormat.YV12, "YV12: YUV 420"),
    )

    private val pixelFormats = mapOf(
        PixelFormat.A_8 to "A_8",
        PixelFormat.JPEG to "JPEG",
        PixelFormat.LA_88 to "LA_88",
        PixelFormat.L_8 to "L_8",
        PixelFormat.OPAQUE to "Opaque",
        PixelFormat.RGBA_1010102 to "RGBA 10 10 10 2",
        PixelFormat.RGBA_4444 to "RGBA 4444",
        PixelFormat.RGBA_5551 to "RGBA 5551",
        PixelFormat.RGBA_8888 to "RGBA 8888",
        PixelFormat.RGBA_F16 to "RGBA F16",
        PixelFormat.RGBX_8888 to "RGBX 8888",
        PixelFormat.RGB_332 to "RGB 332",
        PixelFormat.RGB_565 to "RGB 565",
        PixelFormat.RGB_888 to "RGB 888",
        PixelFormat.TRANSLUCENT to "Translucent",
        PixelFormat.TRANSPARENT to "Transparent",
        PixelFormat.UNKNOWN to "Unknown",
        PixelFormat.YCbCr_420_SP to "YCbCr 420 SP",
        PixelFormat.YCbCr_422_I to "YCbCr 422 I",
        PixelFormat.YCbCr_422_SP to "YCbCr 422 SP",
    )

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun formatMandatoryStreamInformation(information: MandatoryStreamCombination.MandatoryStreamInformation, outBuilder: StringBuilder? = null): String {
        val builder = outBuilder ?: StringBuilder()
        val startIdx = builder.length
        builder.append("Format: ").append(formatInt(information.format, imageFormats)).append('\n')
        for (size in information.availableSizes) {
            builder.append('\t').append(formatSize(size)).append('\n')
        }
        return builder.trimEnd().substring(startIdx)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun formatMandatoryStreamCombination(combination: MandatoryStreamCombination, outBuilder: StringBuilder? = null): String {
        val builder = outBuilder ?: StringBuilder()
        val startIdx = builder.length
        builder.append(combination.description)
        if (combination.isReprocessable) {
            builder.append("(Reprocessable)")
        }
        builder.append('\n')

        for (information in combination.streamsInformation) {
            formatMandatoryStreamInformation(information, builder)
            builder.append("\n\n")
        }
        return builder.trimEnd().substring(startIdx)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun formatMandatoryStreamCombinationArray(combinations: Array<MandatoryStreamCombination>): String {
        if (combinations.isEmpty()) {
            return getString(R.string.none)
        }
        val builder = StringBuilder()
        for (combination in combinations) {
            formatMandatoryStreamCombination(combination, builder)
            builder.append("\n\n")
        }
        return builder.trimEnd().toString()
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun formatScalerStreamConfigurationMap(configurationMap: StreamConfigurationMap): String {
        val builder = StringBuilder()
        builder.append("-- High speed video FPS ranges\n")
        formatRangeArray(configurationMap.highSpeedVideoFpsRanges, builder)
        builder.append("\n\n-- High speed video sizes\n")
        formatSizeArray(configurationMap.highSpeedVideoSizes, builder)
        builder.append("\n\n-- Output formats\n")
        formatIntArray(configurationMap.outputFormats, pixelFormats + imageFormats, builder)
        if (Build.VERSION_CODES.M <= Build.VERSION.SDK_INT) {
            builder.append("\n\n-- Input formats\n")
            formatIntArray(configurationMap.inputFormats, pixelFormats + imageFormats, builder)
            builder.append("\n\n-- High resolution output sizes\n")
            formatSizeArray(configurationMap.highSpeedVideoSizes, builder)
        }
        builder.append("\n\n-- Raw\n").append(configurationMap.toString())
        return builder.trimEnd().toString()
    }

    private val sensorTestPatternModes = mapOf(
        CameraMetadata.SENSOR_TEST_PATTERN_MODE_COLOR_BARS to "Color bars",
        CameraMetadata.SENSOR_TEST_PATTERN_MODE_COLOR_BARS_FADE_TO_GRAY to "Color bars fade to gray",
        CameraMetadata.SENSOR_TEST_PATTERN_MODE_CUSTOM1 to "Custom 1",
        CameraMetadata.SENSOR_TEST_PATTERN_MODE_OFF to "Off",
        CameraMetadata.SENSOR_TEST_PATTERN_MODE_PN9 to "PN9 (Pseudo random)",
        CameraMetadata.SENSOR_TEST_PATTERN_MODE_SOLID_COLOR to "Solid color",
    )

    private val sensorInfoColorFilterArrangements = mapOf(
        CameraMetadata.SENSOR_INFO_COLOR_FILTER_ARRANGEMENT_BGGR to "BGGR",
        CameraMetadata.SENSOR_INFO_COLOR_FILTER_ARRANGEMENT_GBRG to "GBRG",
        CameraMetadata.SENSOR_INFO_COLOR_FILTER_ARRANGEMENT_GRBG to "GRBG",
        CameraMetadata.SENSOR_INFO_COLOR_FILTER_ARRANGEMENT_MONO to "Monochrome",
        CameraMetadata.SENSOR_INFO_COLOR_FILTER_ARRANGEMENT_NIR to "NIR (Near infrared)",
        CameraMetadata.SENSOR_INFO_COLOR_FILTER_ARRANGEMENT_RGB to "RGB",
        CameraMetadata.SENSOR_INFO_COLOR_FILTER_ARRANGEMENT_RGGB to "RGGB",
    )

    private val sensorInfoTimestampSources = mapOf(
        CameraMetadata.SENSOR_INFO_TIMESTAMP_SOURCE_REALTIME to "Realtime",
        CameraMetadata.SENSOR_INFO_TIMESTAMP_SOURCE_UNKNOWN to "Unknown",
    )

    private val sensorReferenceIlluminant1s = mapOf(
        CameraMetadata.SENSOR_REFERENCE_ILLUMINANT1_CLOUDY_WEATHER to "Cloudy weather",
        CameraMetadata.SENSOR_REFERENCE_ILLUMINANT1_COOL_WHITE_FLUORESCENT to "Cool white fluorescent (W 3900 ~ 4500K)",
        CameraMetadata.SENSOR_REFERENCE_ILLUMINANT1_D50 to "D50",
        CameraMetadata.SENSOR_REFERENCE_ILLUMINANT1_D55 to "D55",
        CameraMetadata.SENSOR_REFERENCE_ILLUMINANT1_D65 to "D65",
        CameraMetadata.SENSOR_REFERENCE_ILLUMINANT1_D75 to "D75",
        CameraMetadata.SENSOR_REFERENCE_ILLUMINANT1_DAYLIGHT to "Daylight",
        CameraMetadata.SENSOR_REFERENCE_ILLUMINANT1_DAYLIGHT_FLUORESCENT to "Daylight fluorescent (D 5700 ~ 7100K)",
        CameraMetadata.SENSOR_REFERENCE_ILLUMINANT1_DAY_WHITE_FLUORESCENT to "Day white fluorescent (N 4600 ~ 5400K)",
        CameraMetadata.SENSOR_REFERENCE_ILLUMINANT1_FINE_WEATHER to "Fine waather",
        CameraMetadata.SENSOR_REFERENCE_ILLUMINANT1_FLASH to "Flash",
        CameraMetadata.SENSOR_REFERENCE_ILLUMINANT1_FLUORESCENT to "Fluorescent",
        CameraMetadata.SENSOR_REFERENCE_ILLUMINANT1_ISO_STUDIO_TUNGSTEN to "ISO studio tungsten",
        CameraMetadata.SENSOR_REFERENCE_ILLUMINANT1_SHADE to "Shade",
        CameraMetadata.SENSOR_REFERENCE_ILLUMINANT1_STANDARD_A to "Standard A",
        CameraMetadata.SENSOR_REFERENCE_ILLUMINANT1_STANDARD_B to "Standard B",
        CameraMetadata.SENSOR_REFERENCE_ILLUMINANT1_STANDARD_C to "Standard C",
        CameraMetadata.SENSOR_REFERENCE_ILLUMINANT1_TUNGSTEN to "Tungsten",
        CameraMetadata.SENSOR_REFERENCE_ILLUMINANT1_WHITE_FLUORESCENT to "White fluorescent (WW 3200 ~ 3700K)",
    )

    private val shadingModes = mapOf(
        CameraMetadata.SHADING_MODE_OFF to "Off",
        CameraMetadata.SHADING_MODE_FAST to "Fast",
        CameraMetadata.SHADING_MODE_HIGH_QUALITY to "High quality"
    )

    private val statisticsFaceDetectModes = mapOf(
        CameraMetadata.STATISTICS_FACE_DETECT_MODE_OFF to "Off",
        CameraMetadata.STATISTICS_FACE_DETECT_MODE_SIMPLE to "Simple",
        CameraMetadata.STATISTICS_FACE_DETECT_MODE_FULL to "Full",
    )

    private val statisticsLensShadingMapModes = mapOf(
        CameraMetadata.STATISTICS_LENS_SHADING_MAP_MODE_OFF to "Off",
        CameraMetadata.STATISTICS_LENS_SHADING_MAP_MODE_ON to "On",
    )

    private val statisticsOisDataModes = mapOf(
        CameraMetadata.STATISTICS_OIS_DATA_MODE_OFF to "Off",
        CameraMetadata.STATISTICS_OIS_DATA_MODE_ON to "On",
    )

    private val tonemapModes = mapOf(
        CameraMetadata.TONEMAP_MODE_CONTRAST_CURVE to "Contrast curve",
        CameraMetadata.TONEMAP_MODE_FAST to "Fast",
        CameraMetadata.TONEMAP_MODE_GAMMA_VALUE to "Gamma value",
        CameraMetadata.TONEMAP_MODE_HIGH_QUALITY to "High quality",
        CameraMetadata.TONEMAP_MODE_PRESET_CURVE to "Preset curve",
    )

    private val characteristicFormatters = mutableMapOf<String, (Any)->String>()

    @SuppressLint("newApi")
    @Suppress("UNCHECKED_CAST")
    private val characteristicsFormatterList = listOf<Pair<Lazy<String>, (Any)->String>> (
        Pair(lazy { CameraCharacteristics.COLOR_CORRECTION_AVAILABLE_ABERRATION_MODES.name }, { formatColorCorrectionAvailableAberrationModes(it as IntArray) }),
        Pair(lazy { CameraCharacteristics.CONTROL_AE_AVAILABLE_ANTIBANDING_MODES.name }, { formatControlAeAvailableAntibandingModes(it as IntArray) }),
        Pair(lazy { CameraCharacteristics.CONTROL_AE_AVAILABLE_MODES.name }, { formatControlAeModes(it as IntArray) }),
        Pair(lazy { CameraCharacteristics.CONTROL_AE_AVAILABLE_TARGET_FPS_RANGES.name }, { formatRangeArray(it as Array<Range<*>>) }),
        Pair(lazy { CameraCharacteristics.CONTROL_AE_COMPENSATION_RANGE.name }, { formatControlAeCompensationRange(it as Range<Int>) }),
        Pair(lazy { CameraCharacteristics.CONTROL_AE_COMPENSATION_STEP.name }, { formatControlAeCompensationStep(it as Rational) }),
        Pair(lazy { CameraCharacteristics.CONTROL_AE_LOCK_AVAILABLE.name }, { it.toString() }), //  Boolean
        Pair(lazy { CameraCharacteristics.CONTROL_AF_AVAILABLE_MODES.name }, { formatControlAfAvailableModes(it as IntArray) }),
        Pair(lazy { CameraCharacteristics.CONTROL_AVAILABLE_EFFECTS.name }, { formatControlAvailableEffects(it as IntArray) }),
        Pair(lazy { CameraCharacteristics.CONTROL_AVAILABLE_EXTENDED_SCENE_MODE_CAPABILITIES.name }, { formatAvailableExtendedSceneModeCapabilities(it as Array<Capability>) }),
        Pair(lazy { CameraCharacteristics.CONTROL_AVAILABLE_MODES.name }, { formatControlAvailableModes(it as IntArray) }),
        Pair(lazy { CameraCharacteristics.CONTROL_AVAILABLE_SCENE_MODES.name }, { formatControlAvailableSceneModes(it as IntArray) }),
        Pair(lazy { CameraCharacteristics.CONTROL_AVAILABLE_VIDEO_STABILIZATION_MODES.name }, { formatControlAvailableVideoStabilizationModes(it as IntArray) }),
        Pair(lazy { CameraCharacteristics.CONTROL_AWB_AVAILABLE_MODES.name }, { formatControlAwbAvailableModes(it as IntArray) }),
        Pair(lazy { CameraCharacteristics.CONTROL_AWB_LOCK_AVAILABLE.name }, { it.toString() }), //  Boolean
        Pair(lazy { CameraCharacteristics.CONTROL_MAX_REGIONS_AE.name }, { it.toString() }), //  Int
        Pair(lazy { CameraCharacteristics.CONTROL_MAX_REGIONS_AF.name }, { it.toString() }), //  Int
        Pair(lazy { CameraCharacteristics.CONTROL_MAX_REGIONS_AWB.name }, { it.toString() }), //  Int
        Pair(lazy { CameraCharacteristics.CONTROL_POST_RAW_SENSITIVITY_BOOST_RANGE.name }, { formatControlPostRawSensitivityBoostRange(it as Range<Int>) }),
        Pair(lazy { CameraCharacteristics.CONTROL_ZOOM_RATIO_RANGE.name }, { formatControlZoomRatioRange(it as Range<Float>) }),
        Pair(lazy { CameraCharacteristics.DEPTH_DEPTH_IS_EXCLUSIVE.name }, { it.toString() }), //  Boolean
        Pair(lazy { CameraCharacteristics.DISTORTION_CORRECTION_AVAILABLE_MODES.name }, { formatDistortionCorrectionAvailableModes(it as IntArray) }),
        Pair(lazy { CameraCharacteristics.EDGE_AVAILABLE_EDGE_MODES.name }, { formatEdgeAvailableEdgeModes(it as IntArray) }),
        Pair(lazy { CameraCharacteristics.FLASH_INFO_AVAILABLE.name }, { it.toString() }), //  Boolean
        Pair(lazy { CameraCharacteristics.HOT_PIXEL_AVAILABLE_HOT_PIXEL_MODES.name }, { formatHotPixelAvailableHotPixelModes(it as IntArray) }),
        Pair(lazy { CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL.name }, { formatSupportedHardwareLevel(it as Int) }),
        Pair(lazy { CameraCharacteristics.INFO_VERSION.name }, { it as String }),
        Pair(lazy { CameraCharacteristics.JPEG_AVAILABLE_THUMBNAIL_SIZES.name }, { formatSizeArray(it as Array<Size>) }),
        Pair(lazy { CameraCharacteristics.LENS_DISTORTION.name }, { formatFloatArray(it as FloatArray) }),
        Pair(lazy { CameraCharacteristics.LENS_FACING.name }, { formatLensFacing(it as Int) }),
        Pair(lazy { CameraCharacteristics.LENS_INFO_AVAILABLE_APERTURES.name }, { formatFloatArray(it as FloatArray) }),
        Pair(lazy { CameraCharacteristics.LENS_INFO_AVAILABLE_FILTER_DENSITIES.name }, { formatFloatArray(it as FloatArray) }),
        Pair(lazy { CameraCharacteristics.LENS_INFO_AVAILABLE_FOCAL_LENGTHS.name }, { formatFloatArray(it as FloatArray) }),
        Pair(lazy { CameraCharacteristics.LENS_INFO_AVAILABLE_OPTICAL_STABILIZATION.name }, { formatLensInfoAvailableOpticalStabilization(it as IntArray) }),
        Pair(lazy { CameraCharacteristics.LENS_INFO_FOCUS_DISTANCE_CALIBRATION.name }, { formatInt(it as Int, focusDistanceCalibrations) }),
        Pair(lazy { CameraCharacteristics.LENS_INFO_HYPERFOCAL_DISTANCE.name }, { it.toString() }), //  Float
        Pair(lazy { CameraCharacteristics.LENS_INFO_MINIMUM_FOCUS_DISTANCE.name }, { it.toString() }), //  Float
        Pair(lazy { CameraCharacteristics.LENS_INTRINSIC_CALIBRATION.name }, { formatFloatArray(it as FloatArray) }),
        Pair(lazy { CameraCharacteristics.LENS_POSE_REFERENCE.name }, { formatInt(it as Int, lensPoseReferences) }),
        Pair(lazy { CameraCharacteristics.LENS_POSE_ROTATION.name }, { formatFloatArray(it as FloatArray) }),
        Pair(lazy { CameraCharacteristics.LENS_POSE_TRANSLATION.name }, { formatFloatArray(it as FloatArray) }),
        Pair(lazy { CameraCharacteristics.LENS_RADIAL_DISTORTION.name }, { formatFloatArray(it as FloatArray) }),
        Pair(lazy { CameraCharacteristics.LOGICAL_MULTI_CAMERA_SENSOR_SYNC_TYPE.name }, { formatInt(it as Int, logicalMultiCameraSensorSyncTypes) }),
        Pair(lazy { CameraCharacteristics.NOISE_REDUCTION_AVAILABLE_NOISE_REDUCTION_MODES.name }, { formatIntArray(it as IntArray, noiseReductionModes) }),
        Pair(lazy { CameraCharacteristics.REPROCESS_MAX_CAPTURE_STALL.name }, { it.toString() }), //  Int
        Pair(lazy { CameraCharacteristics.REQUEST_AVAILABLE_CAPABILITIES.name }, { formatIntArray(it as IntArray, requestAvailableCapabilities) }),
        Pair(lazy { CameraCharacteristics.REQUEST_MAX_NUM_INPUT_STREAMS.name }, { it.toString() }), //  Int
        Pair(lazy { CameraCharacteristics.REQUEST_MAX_NUM_OUTPUT_PROC.name }, { it.toString() }), //  Int
        Pair(lazy { CameraCharacteristics.REQUEST_MAX_NUM_OUTPUT_PROC_STALLING.name }, { it.toString() }), //  Int
        Pair(lazy { CameraCharacteristics.REQUEST_MAX_NUM_OUTPUT_RAW.name }, { it.toString() }), //  Int
        Pair(lazy { CameraCharacteristics.REQUEST_PARTIAL_RESULT_COUNT.name }, { it.toString() }), //  Int
        Pair(lazy { CameraCharacteristics.REQUEST_PIPELINE_MAX_DEPTH.name }, { it.toString() }), //  Byte
        Pair(lazy { CameraCharacteristics.SCALER_AVAILABLE_MAX_DIGITAL_ZOOM.name }, { it.toString() }), //  Float
        Pair(lazy { CameraCharacteristics.SCALER_CROPPING_TYPE.name }, { formatInt(it as Int, scalerCroppingTypes) }),
        Pair(lazy { CameraCharacteristics.SCALER_MANDATORY_CONCURRENT_STREAM_COMBINATIONS.name }, { formatMandatoryStreamCombinationArray(it as Array<MandatoryStreamCombination>) }),
        Pair(lazy { CameraCharacteristics.SCALER_MANDATORY_STREAM_COMBINATIONS.name }, { formatMandatoryStreamCombinationArray(it as Array<MandatoryStreamCombination>) }),
        Pair(lazy { CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP.name }, { formatScalerStreamConfigurationMap(it as StreamConfigurationMap) }),
        Pair(lazy { CameraCharacteristics.SENSOR_AVAILABLE_TEST_PATTERN_MODES.name }, { formatIntArray(it as IntArray, sensorTestPatternModes) }),
        Pair(lazy { CameraCharacteristics.SENSOR_BLACK_LEVEL_PATTERN.name }, { it.toString() }), //  BlackLevelPattern
        Pair(lazy { CameraCharacteristics.SENSOR_CALIBRATION_TRANSFORM1.name }, { it.toString() }), //  ColorSpaceTransform
        Pair(lazy { CameraCharacteristics.SENSOR_CALIBRATION_TRANSFORM2.name }, { it.toString() }), //  ColorSpaceTransform
        Pair(lazy { CameraCharacteristics.SENSOR_COLOR_TRANSFORM1.name }, { it.toString() }), //  ColorSpaceTransform
        Pair(lazy { CameraCharacteristics.SENSOR_COLOR_TRANSFORM2.name }, { it.toString() }), //  ColorSpaceTransform
        Pair(lazy { CameraCharacteristics.SENSOR_FORWARD_MATRIX1.name }, { it.toString() }), //  ColorSpaceTransform
        Pair(lazy { CameraCharacteristics.SENSOR_FORWARD_MATRIX2.name }, { it.toString() }), //  ColorSpaceTransform
        Pair(lazy { CameraCharacteristics.SENSOR_INFO_ACTIVE_ARRAY_SIZE.name }, { formatRect(it as Rect) }),
        Pair(lazy { CameraCharacteristics.SENSOR_INFO_COLOR_FILTER_ARRANGEMENT.name }, { formatInt(it as Int, sensorInfoColorFilterArrangements) }),
        Pair(lazy { CameraCharacteristics.SENSOR_INFO_EXPOSURE_TIME_RANGE.name }, { formatRange(it as Range<*>) }),
        Pair(lazy { CameraCharacteristics.SENSOR_INFO_LENS_SHADING_APPLIED.name }, { it.toString() }),  //  Boolean
        Pair(lazy { CameraCharacteristics.SENSOR_INFO_MAX_FRAME_DURATION.name }, { String.format("%,d ns", it) }), //  Long
        Pair(lazy { CameraCharacteristics.SENSOR_INFO_PHYSICAL_SIZE.name }, { formatSizeF(it as SizeF) }),
        Pair(lazy { CameraCharacteristics.SENSOR_INFO_PIXEL_ARRAY_SIZE.name }, { formatSize(it as Size) }),
        Pair(lazy { CameraCharacteristics.SENSOR_INFO_PRE_CORRECTION_ACTIVE_ARRAY_SIZE.name }, { formatRect(it as Rect) }),
        Pair(lazy { CameraCharacteristics.SENSOR_INFO_SENSITIVITY_RANGE.name }, { formatRange(it as Range<*>) }),
        Pair(lazy { CameraCharacteristics.SENSOR_INFO_TIMESTAMP_SOURCE.name }, { formatInt(it as Int, sensorInfoTimestampSources) }),
        Pair(lazy { CameraCharacteristics.SENSOR_INFO_WHITE_LEVEL.name }, { it.toString() }), //  Int
        Pair(lazy { CameraCharacteristics.SENSOR_MAX_ANALOG_SENSITIVITY.name }, { it.toString() }), //  Int
        Pair(lazy { CameraCharacteristics.SENSOR_OPTICAL_BLACK_REGIONS.name }, { formatRectArray(it as Array<Rect>) }),
        Pair(lazy { CameraCharacteristics.SENSOR_ORIENTATION.name }, { it.toString() }), //  Int
        Pair(lazy { CameraCharacteristics.SENSOR_REFERENCE_ILLUMINANT1.name }, { formatInt(it as Int, sensorReferenceIlluminant1s) }),
        Pair(lazy { CameraCharacteristics.SENSOR_REFERENCE_ILLUMINANT2.name }, { formatInt((it as Byte).toInt(), sensorReferenceIlluminant1s) }),
        Pair(lazy { CameraCharacteristics.SHADING_AVAILABLE_MODES.name }, { formatIntArray(it as IntArray, shadingModes) }),
        Pair(lazy { CameraCharacteristics.STATISTICS_INFO_AVAILABLE_FACE_DETECT_MODES.name }, { formatIntArray(it as IntArray, statisticsFaceDetectModes) }),
        Pair(lazy { CameraCharacteristics.STATISTICS_INFO_AVAILABLE_HOT_PIXEL_MAP_MODES.name }, { formatBooleanArray(it as BooleanArray) }),
        Pair(lazy { CameraCharacteristics.STATISTICS_INFO_AVAILABLE_LENS_SHADING_MAP_MODES.name }, { formatIntArray(it as IntArray, statisticsLensShadingMapModes) }),
        Pair(lazy { CameraCharacteristics.STATISTICS_INFO_AVAILABLE_OIS_DATA_MODES.name }, { formatIntArray(it as IntArray, statisticsOisDataModes) }),
        Pair(lazy { CameraCharacteristics.STATISTICS_INFO_MAX_FACE_COUNT.name }, { it.toString() }), //  Int
        Pair(lazy { CameraCharacteristics.SYNC_MAX_LATENCY.name }, { it.toString() }), //  Int
        Pair(lazy { CameraCharacteristics.TONEMAP_AVAILABLE_TONE_MAP_MODES.name }, { formatIntArray(it as IntArray, tonemapModes) }),
        Pair(lazy { CameraCharacteristics.TONEMAP_MAX_CURVE_POINTS.name }, { it.toString() }), //  Int
    )

    @SuppressLint("newApi")
    private suspend fun prepareFormatters() {
        for (item in characteristicsFormatterList) {
            try {
                characteristicFormatters[item.first.value] = item.second
            } catch (e: NoSuchFieldError) {
                //  Using version dependent fields in older version throws this error
                //  So we have to pre-catch it like this
                //  We are expecting lots of this according to android version,
                //  So, no log printout, no firebase event here
            }
        }
    }

    @SuppressLint("newApi")
    private fun getCameraCharacteristicsItem(key: CameraCharacteristics.Key<*>, value: Any?, prefix: String): InfoItem {
        if (null == value) {
            return InfoItem(key.name, R.string.unknown)
        }
        val formatted: String = try {
            characteristicFormatters[key.name]?.invoke(value) ?: getString(R.string.invalid_item)
        } catch (e: Exception) {
            FirebaseHelper.onThrowableCaught(e)
            Log.e(TAG, "error formatting ${key.name}")
            e.printStackTrace()
            getString(R.string.unsupported)
        }
        return InfoItem(prefix + key.name, formatted)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun getCameraCharacteristicsItems(manager: CameraManager, id: String): List<InfoItem> {
        val list = mutableListOf<InfoItem>()
        val cc = manager.getCameraCharacteristics(id)
        val prefix = "$id: "
        for (key in cc.keys.sortedBy { it.name }) {
            val value = cc[key]
            list.add(getCameraCharacteristicsItem(key, value, prefix))
        }
        return list
    }

    private val cameraContent = mutableListOf<InfoItem>()
    override suspend fun getItems(): List<InfoItem> {
        if (cameraContent.isEmpty()) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                cameraContent.addAll(CameraInfoLegacy().getLegacyItems())
            } else {
                if (characteristicFormatters.isEmpty()) {
                    prepareFormatters()
                }
                val manager = app.getSystemService(Context.CAMERA_SERVICE) as CameraManager
                cameraContent.addItems(generalSpecs, manager)
                if (manager.cameraIdList.isNotEmpty()) {
                    for (id in manager.cameraIdList) {
                        cameraContent.addAll(getCameraCharacteristicsItems(manager, id))
                    }
                }
//                cameraContent.add(getTestItem())
            }
        }
        return cameraContent
    }
}
