package com.oeu.a.sysinfo.content

import android.annotation.SuppressLint
import android.os.Build
import android.os.Environment
import android.util.Log
import com.oeu.a.sysinfo.R
import com.oeu.a.sysinfo.utils.FirebaseHelper
import java.io.File
import com.oeu.a.sysinfo.utils.with

class StorageInfoProvider: InfoProvider() {
    companion object {
        private val TAG = StorageInfoProvider::class.java.simpleName
    }

    private fun formatStorageEnvironment(): String {
        val emulated = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            Environment.isExternalStorageEmulated()
        } else {
            false
        }
        val removable = Environment.isExternalStorageRemovable()
        val builder = StringBuilder()
        builder.append(Environment.getExternalStorageState()).append(", ")
        if (removable) {
            builder.append(getString(R.string.storage_removable))
        } else {
            builder.append(getString(R.string.storage_non_removable))
        }
        if (emulated) {
            builder.append(getString(R.string.storage_emulated))
        }
        return builder.toString()
    }

    private fun isExternalStorageAccessible(): Boolean {
        if (!Environment.isExternalStorageRemovable()) {
            return true
        }
        return when (Environment.getExternalStorageState()) {
            Environment.MEDIA_MOUNTED, Environment.MEDIA_MOUNTED_READ_ONLY -> true
            else -> false
        }
    }

    private val itemSpecs = listOf(
        InfoSpec(R.string.storage_absolute_path, 1),
        InfoSpec(R.string.storage_total_space, 9),
        InfoSpec(R.string.storage_free_space, 9),
        InfoSpec(R.string.storage_usable_space, 9),
        InfoSpec(R.string.storage_state, 19)
    )

    @SuppressLint("NewApi")
    override fun getItem(infoId: Int, vararg args: Any?): InfoItem? {
        return with(args[0] as? File, args[1] as? String) { file, prefix ->
            val name = getString(infoId)
            val value = try {
                when (infoId) {
                    R.string.storage_absolute_path -> file.absolutePath
                    R.string.storage_total_space -> formatStorageSize(file.totalSpace)
                    R.string.storage_free_space -> formatStorageSize(file.freeSpace)
                    R.string.storage_usable_space -> formatStorageSize(file.usableSpace)
                    R.string.storage_state -> if (Build.VERSION_CODES.LOLLIPOP <= Build.VERSION.SDK_INT) {
                        Environment.getExternalStorageState(file)
                    } else {
                        Environment.getStorageState(file)
                    }
                    else -> getString(R.string.invalid_item)
                }
            } catch (e: Error) {
                FirebaseHelper.onThrowableCaught(e)
                e.printStackTrace()
                getString(R.string.unsupported)
            }
            InfoItem("$prefix: $name", value)
        }
    }

    private val storageContent: MutableList<InfoItem> = mutableListOf()
    override suspend fun getItems(): List<InfoItem> {
        if (storageContent.isEmpty()) {
            storageContent.addItems(itemSpecs, Environment.getRootDirectory(), "System")
            storageContent.addItems(itemSpecs, Environment.getDataDirectory(), "Data")
            storageContent.addItems(itemSpecs, Environment.getDownloadCacheDirectory(), "Cache")

            storageContent.add(InfoItem(R.string.storage_external_description, formatStorageEnvironment()))
            if (isExternalStorageAccessible()) {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
                    storageContent.addItems(itemSpecs, Environment.getExternalStorageDirectory(), "External")
                } else {
                    storageContent.addItems(itemSpecs, app.getExternalFilesDir(null), "External")
                }
            }
        }
        return storageContent
    }
}
