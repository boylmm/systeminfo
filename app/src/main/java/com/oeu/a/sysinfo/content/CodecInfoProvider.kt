package com.oeu.a.sysinfo.content

import android.annotation.SuppressLint
import android.media.MediaCodecInfo
import android.media.MediaCodecInfo.*
import android.media.MediaCodecList
import android.os.Build
import androidx.annotation.RequiresApi
import com.oeu.a.sysinfo.R
import com.oeu.a.sysinfo.utils.FirebaseHelper
import java.lang.Exception

class CodecInfoProvider: InfoProvider() {
    companion object {
        private val TAG = CodecInfoProvider::class.java.simpleName
    }

    @SuppressLint("NewApi")
    private fun addCapability(sb: StringBuilder, mccap: CodecCapabilities) {
        val ap = mccap.audioCapabilities
        if (null == ap) {
//            sb.append("\nNo audio capability.")  //  No need to show this
        } else {
            sb.append("\nAudio")
                .append("\n\tBitrate range: ${ap.bitrateRange}")
                .append("\n\tMaximum input channel: ${ap.maxInputChannelCount}")
            val rateRanges = ap.supportedSampleRateRanges
            if (null != rateRanges && rateRanges.isNotEmpty()) {
                sb.append("\n\tSupported sample rate ranges")
                for (range in rateRanges) {
                    sb.append("\n\t\t$range")
                }
            }
            val sampleRates = ap.supportedSampleRates
            if (null != sampleRates && sampleRates.isNotEmpty()) {
                sb.append("\n\tSupported sample rates")
                for (rate in sampleRates) {
                    sb.append("\n\t\t$rate")
                }
            }
        }
        val vp = mccap.videoCapabilities
        if (null == vp) {
//            sb.append("\nNo video capability.")  //  No need to show this
        } else {
            sb.append("\nVideo")
                .append("\n\tBitrate range: ${vp.bitrateRange}")
                .append("\n\tFrame rate range: ${vp.supportedFrameRates}")
                .append("\n\tSupported widths: ${vp.supportedWidths}")
                .append("\n\tSupported heights: ${vp.supportedHeights}")
                .append("\n\tWidth alignment: ${vp.widthAlignment}")
                .append("\n\tHeight alignment: ${vp.heightAlignment}")
        }

        val ep = mccap.encoderCapabilities
        if (null == ep) {
            //  Nothing to do here
        } else {
            sb.append("\nEncoding").append("\n\tComplexity range: ${ep.complexityRange}")
            val cbr = ep.isBitrateModeSupported(EncoderCapabilities.BITRATE_MODE_CBR)
            val cq = ep.isBitrateModeSupported(EncoderCapabilities.BITRATE_MODE_CQ)
            val vbr = ep.isBitrateModeSupported(EncoderCapabilities.BITRATE_MODE_VBR)
            if (cbr || cq || vbr) {
                sb.append("\n\tSupported Bitrate mode")
                if (cbr) {
                    sb.append("\n\t\tConstant bitrate")
                }
                if (cq) {
                    sb.append("\n\t\tConstant quality")
                }
                if (vbr) {
                    sb.append("\n\t\tVariable bitrate")
                }
            }
        }
        sb.append('\n')
    }

    @SuppressLint("NewApi")
    private fun addTypeInfo(sb: StringBuilder, info: MediaCodecInfo, type: String) {
        sb.append(type).append(if (info.isEncoder) " encoder" else "")
        if (Build.VERSION_CODES.LOLLIPOP <= Build.VERSION.SDK_INT) {
            val mccap = info.getCapabilitiesForType(type)
            addCapability(sb, mccap)
        }
    }

    @SuppressLint("NewApi")
    private fun getCodecInfoItem(info: MediaCodecInfo): InfoItem {
        val types = info.supportedTypes
        val sb = StringBuilder()
        try {
            if (Build.VERSION_CODES.Q <= Build.VERSION.SDK_INT) {
                if (info.isAlias) {
                    sb.append("Alias of ${info.canonicalName}\n")
                } else if (info.name != info.canonicalName) {
                    sb.append(info.canonicalName).append('\n')
                }
                if (info.isHardwareAccelerated) {
                    sb.append(getString(R.string.codec_hardware_accelerated)).append('\n')
                }
                if (info.isSoftwareOnly) {
                    sb.append(getString(R.string.codec_software_only)).append('\n')
                }
                if (info.isVendor) {
                    sb.append(getString(R.string.codec_device_specific)).append('\n')
                }
            }
            if (types.isEmpty()) {
                sb.append(getString(R.string.unsupported))
            } else {
                addTypeInfo(sb, info, types[0])
                for (idx in 1 until types.size) {
                    addTypeInfo(sb, info, types[idx])
                }
                sb.deleteCharAt(sb.lastIndex)
            }
        } catch (e: Exception) {
            FirebaseHelper.onThrowableCaught(e)
            e.printStackTrace()
            sb.append('\n').append(getString(R.string.error))
        }
        return InfoItem(info.name, sb.toString())
    }

    private val MediaCodecInfo.mimeType: String @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    get() = if (supportedTypes.isEmpty()) "" else supportedTypes[0]

    private val codecContent: MutableList<InfoItem> = mutableListOf()
    override suspend fun getItems(): List<InfoItem> {
        if (codecContent.isEmpty()) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                codecContent.add(InfoItem(R.string.codec_info, getString(R.string.sdk_version_required, Build.VERSION_CODES.JELLY_BEAN)))
            } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                val codecInfos: MutableList<MediaCodecInfo> = mutableListOf()
                for (idx in 0 until MediaCodecList.getCodecCount()) {
                    codecInfos.add(MediaCodecList.getCodecInfoAt(idx))
                }
                codecInfos.sortBy { it.mimeType }
                for (codecInfo in codecInfos) {
                    codecContent.add(getCodecInfoItem(codecInfo))
                }
            } else {
                val codecInfos = MediaCodecList(MediaCodecList.ALL_CODECS).codecInfos
                if (codecInfos.isEmpty()) {
                    codecContent.add(InfoItem(R.string.codec_info, R.string.codec_none))
                } else {
                    val sorted = mutableListOf(*codecInfos).sortedBy { it.mimeType }
                    for (codecInfo in sorted) {
                        codecContent.add(getCodecInfoItem(codecInfo))
                    }
                }
            }
        }
        return codecContent
    }
}
