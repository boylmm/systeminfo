package com.oeu.a.sysinfo.content

import android.app.Application
import android.content.pm.PackageManager
import android.os.Build
import androidx.core.app.ActivityCompat
import com.oeu.a.sysinfo.R
import kotlinx.coroutines.*
import java.text.DateFormat
import java.util.*

abstract class InfoProvider {
    companion object {
        private val TAG = InfoProvider::class.java.simpleName

        lateinit var app: Application  //  Init this from SystemInfoApplication
        private val backgroundScope = CoroutineScope(Dispatchers.IO)

        //  See https://www.npl.co.uk/si-units  for unit prefixes
        @JvmStatic protected val siPrefixes: Array<String> = arrayOf("", "k", "M", "G", "T", "P", "E", "Z", "Y")
        val shareInfoItems: List<InfoItem> by lazy {
            listOf(
                InfoItem(R.string.about_app_name, app.packageManager.getApplicationLabel(app.applicationInfo).toString() + " " + AboutInfoProvider.formatAppVersion()),
                InfoItem(R.string.android_device, "${Build.MANUFACTURER}: ${Build.MODEL}"),
                InfoItem(R.string.android_sdk, AndroidInfoProvider.formatSdk(Build.VERSION.SDK_INT)),
            )
        }
    }

    protected abstract suspend fun getItems() :List<InfoItem>
    protected open fun getItem(infoId: Int, vararg args: Any?): InfoItem? = null

    open val requiredPermissions: Array<String> = emptyArray()

    protected data class InfoSpec(val titleId: Int, val minSdk: Int = 1, val deprecateSdk: Int = Int.MAX_VALUE) {
        val isAvailable: Boolean get() = Build.VERSION.SDK_INT in minSdk until deprecateSdk
    }

    protected fun getString(id: Int, vararg args: Any?): String = app.getString(id, *args)

    fun formatTime(time: Long): String =
        DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, Locale.getDefault()).format(Date(time))

    fun formatStringArray(vararg array: String): String {
        if (array.isEmpty()) {
            return getString(R.string.none)
        }
        val builder = StringBuilder()
        builder.append(array[0])
        for (idx in 1 until array.size) {
            builder.append('\n').append(array[idx])
        }
        return builder.toString()
    }

    fun formatFloatArray(array: FloatArray): String {
        if (array.isEmpty()) {
            return getString(R.string.none)
        }
        val builder = StringBuilder()
        builder.append(array[0].toString())
        for (idx in 1 until array.size) {
            builder.append('\n').append(array[idx].toString())
        }
        return builder.toString()
    }

    fun formatStorageSize(size: Long): String {
        var unitIdx = 0
        var tmp = size
        var div = 1L
        while (1024 < tmp) {
            ++unitIdx
            tmp /= 1024
            div *= 1024
        }
        val value: Float = size.toFloat() / div.toFloat()
        if (siPrefixes.size <= unitIdx) {
            unitIdx = siPrefixes.size - 1
        }
        return String.format("%.1f %s (%,d)", value, siPrefixes[unitIdx], size)
    }

    protected fun getItem(spec: InfoSpec, vararg args: Any?): InfoItem? {
        return when {
            Build.VERSION.SDK_INT < spec.minSdk ->
                null  //  or use InfoItem(spec.titleId, app.getString(R.string.sdk_version_required, spec.minSdk))
            spec.deprecateSdk <= Build.VERSION.SDK_INT ->
                InfoItem(spec.titleId, app.getString(R.string.sdk_version_deprecated, spec.deprecateSdk))
            else -> getItem(spec.titleId, *args) ?: InfoItem(spec.titleId, R.string.unsupported)
        }
    }

    protected fun MutableList<InfoItem>.addItems(specs: List<InfoSpec>, vararg args: Any?) {
        for (spec in specs) {
            getItem(spec, *args)?.also {
                this.add(it)
            }
        }
    }

    private val permissionDeniedItems: List<InfoItem> get() =
        listOf(InfoItem(R.string.permissions_required, formatStringArray(*requiredPermissions)))

    fun getItemsAsync(onCompletion: (List<InfoItem>)-> Unit) {
        backgroundScope.launch {
            var granted = true
            for (permission in requiredPermissions) {
                if (ActivityCompat.checkSelfPermission(app, permission) != PackageManager.PERMISSION_GRANTED) {
                    granted = false
                    break
                }
            }

            val items = if (granted) getItems() else permissionDeniedItems
            withContext(Dispatchers.Main) {
                onCompletion(items)
            }
        }
    }
}
